//
//  ActionsProfile.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 07/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension ProfileViewController: retornoJsonDelegate{
    
    internal func recoveryAllData(){
        
        let ws = WebService(
            funcao: "pre_editar_motorista",
            campos:"",
            authUsuario: "",
            authSenha: "",
            tipo:"GET",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        if let status = json["status"] as? Int{
            
            
            switch status{
                
            case 1://the request was a success
                if let arrayCidades = json["cidades"] as? NSArray{
                    
                    self.cities = [GenericField]()
                    for i in arrayCidades{
                        
                        let eachInformation = i as! [String: AnyObject]
                        let id   = eachInformation["id"]?.integerValue
                        let name = eachInformation["nome"] as! String
                        
                        self.cities.append(GenericField(id: id!, name: name))
                        
                    }
                    
                }
                
                if let arrayGender = json["marcas"] as? NSArray{
                    
                    self.brands = [GenericField]()
                    for i in arrayGender{
                        
                        let eachInformation = i as! [String: AnyObject]
                        let id    = eachInformation["id"]?.integerValue
                        let name  = eachInformation["nome"] as! String
                        
                        self.brands.append(GenericField(id: id!, name: name))
                        
                    }
                    
                }
                
                if let driver = json["motorista"] as? NSDictionary{
                    
                    if let neighborhood = driver["bairro_usuario"] as? String{
                        self.personalData.setNeighborhood(neighborhood: neighborhood)
                    }
                    
                    if let cellPhone = driver["celular_usuario"] as? String{
                        self.personalData.setcellPhone(cellPhone: cellPhone)
                    }
                    
                    if let zipCode = driver["cep_usuario"] as? String{
                        self.personalData.setZipCode(zipCode: zipCode)
                    }
                    
                    if let city = driver["cidade_usuario"] as? String{
                        self.personalData.setCity(city: city)
                    }
                    
                    if let complement = driver["complemento_usuario"] as? String{
                        self.personalData.setComplement(complement: complement)
                    }
                    
                    if let cpf = driver["cpf_usuario"] as? String{
                        self.personalData.setCPF(cpf: cpf)
                    }
                    
                    if let email = driver["email_usuario"] as? String{
                        self.personalData.setEmail(email: email)
                    }
                    
                    if let gender =  (driver["fk_genero"] as AnyObject).integerValue {
                        self.personalData.setGender(idGender: gender)
                    }
                    
                    if let uf = (driver["fk_uf_usuario"] as AnyObject).integerValue{
                        self.personalData.setIdState(idState: uf)
                    }
                    
                    if let address = driver["logradouro_usuario"] as? String{
                        self.personalData.setAddress(address: address)
                    }
                    
                    if let name = driver["nome_usuario"] as? String{
                        self.personalData.setDriverName(name: name)
                    }
                    
                    if let number = driver["num_residencia_usuario"] as? String{
                        self.personalData.setResidenceNumber(residenceNumber: number)
                    }
                    
                    if let rg = driver["rg_usuario"] as? String{
                        self.personalData.setRG(rg: rg)
                    }
                    
                    if let phone = driver["telefone_usuario"] as? String{
                        self.personalData.setPhone(phone: phone)
                    }
                    
                }
                
                if let vehicle = json["veiculo"] as? NSDictionary{
                    
                    if let color = vehicle["cor_carro_motorista"] as? String{
                        self.vehicleData.setCarColor(carColor: color)
                    }
                    
                    if let city = (vehicle["fk_cidade_ativa_motorista"] as AnyObject).integerValue {
                        self.vehicleData.setIdActive(idActiveCity: city)
                    }
                    
                    if let model = (vehicle["fk_modelo_carro_motorista"] as AnyObject).integerValue{
                        self.vehicleData.setIdModelCar(idModelCar: model)
                    }
                    
                    if let brand = (vehicle["fk_montadora_carro_motorista"] as AnyObject).integerValue{
                        self.vehicleData.setIdBrandCar(idBrandCar: brand)
                    }
                    
                    if let licensePlate = vehicle["placa_carro_motorista"] as? String{
                        self.vehicleData.setCarLicensePlate(carLicensePlate: licensePlate)
                    }
                    
                }
                
            case 0://did not find nothing
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10: // internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
            }
            
        }
        
    }
    
}
