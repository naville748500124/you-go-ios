//
//  ActionsBankData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 07/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension BankDataViewController: retornoJsonDelegate{
    
    internal func findBanks(){
        
        self.functionController = 1
        let ws = WebService(
            funcao: "listar_bancos",
            campos:"",
            authUsuario: "",
            authSenha: "",
            tipo:"GET",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    internal func registerBank(tipoConta: Int){
        
        self.functionController = 2
        let ws = WebService(
            funcao: "motorista_dados_bancarios",
            campos:"fk_banco_motorista=\(self.idBank)&agencia_motorista=\(self.textFieldAgency.text!)&conta_motorista=\(self.textFieldAccount.text!)&digito_motorista=\(self.textFieldDigit.text!)&tipo_conta=\(tipoConta)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        
        if let status = json["status"] as? Int{
            switch status {
            case 1://request was a success
                
                switch self.functionController{
                    
                case 1:
                    if let banks = json["bancos"] as? NSArray{
                        
                        self.bankArray = [GenericField]()
                        for i in banks {
                            
                            let eachInformation = i as! [String: AnyObject]
                            let id    = eachInformation["id_banco"]?.integerValue
                            let name  = eachInformation["banco"] as! String
                            
                            self.bankArray.append(GenericField(id: id!, name: name))
                            
                        }
                        
                        if self.shared.bankID.isEmpty{
                            
                            self.textFieldBank.text = self.bankArray.first?.name
                            self.idBank             = (self.bankArray.first?.id)!
                            
                        }else{
                            
                            if self.bankArray.contains(where: {$0.id == Int(self.shared.bankID)!}){
                                for i in bankArray{
                                    if i.id == Int(self.shared.bankID)!{
                                        self.textFieldBank.text = i.name
                                        self.idBank = Int(self.shared.bankID)!
                                    }
                                }
                            }
                            
                        }
                        
                    }

                case 2:
                    if bankArray.contains(where: {$0.id == self.idBank}){
                        for i in bankArray{
                            if i.id == self.idBank{
                                self.textFieldBank.text = i.name
                            }
                        }
                    }
                    
                    self.shared.bankID        = String(self.idBank)
                    self.shared.agencyDriver  = self.textFieldAgency.text!
                    self.shared.accountDriver = self.textFieldAccount.text!
                    self.shared.digitAccount  = self.textFieldDigit.text!
                    self.shared.tipoConta     = self.tipoConta
                    
                    self.registarButton.setTitle("Atualizar", for: .normal)
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 1)
                default:
                    break
                    
                }
                
            case 0://did not find nothing 
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10://internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
            }
        }
        
    }
    
}
