//
//  ActionsResetPassword.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 05/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension ResetPasswordViewController: retornoJsonDelegate{
    
    internal func resetPassword(){
        
        let ws = WebService(
            funcao:"esqueci_senha?email_usuario=\(self.textFieldEmail.text!)",
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        
        if let status = json["status"] as? Int{
            
            switch status {
            case 1:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 1)
            case -9:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case 0:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            default:
                avisoToast("Ocorreu um erro.", posicao: 3, altura: 45, tipo: 3)
                break
            }
            
        }
        
    }
    
}
