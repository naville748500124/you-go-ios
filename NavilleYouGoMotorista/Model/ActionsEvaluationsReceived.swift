//
//  ActionsEvaluationsReceived.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 15/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

extension EvaluationsReceivedViewController: retornoJsonDelegate {
    
    
    internal func requestEvaluations(){
        
        let ws = WebService(
            funcao:"media_avaliacoes",
            campos:"",
            authUsuario: "",
            authSenha: "",
            tipo:"GET",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
       
        if let status = json["status"] as? Int{
            switch status {
            case 1://return webservice
                if let data = json["avaliacoes"] as? NSDictionary{
                    print("entou no dictionay")
                    self.evaluations = Evaluations()
                    
                    if let punctuality = (data["pontualidade_motorista"] as AnyObject).doubleValue{
                        self.evaluations.setPunctyality(punctuality: punctuality)
                    }
                    
                    if let sympathy = (data["simpatia_motorista"] as AnyObject).doubleValue{
                        self.evaluations.setSympathy(sympathy: sympathy)
                    }
                    
                    if let car = (data["carro_motorista"] as AnyObject).doubleValue{
                        self.evaluations.setCar(car: car)
                    }
                    
                    if let service = (data["servico_motorista"] as AnyObject).doubleValue{
                        self.evaluations.setService(service: service)
                    }
                    self.updateEvaluations()
                }else{
                    
                    let alert = UIAlertController(title: "Aviso", message: "Você ainda não tem avaliação, por isso recebe a avaliação mínima inicial.", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            case 0://did not find nothing
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10://internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
            }
        
        }
        
    }
    
}
