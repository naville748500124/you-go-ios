//
//  ActionsPayment.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 25/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension PaymentViewController: retornoJsonDelegate{
    
    internal func requestHistory(){
        
        let ws = WebService(
            funcao:"hist_pagamentos\(self.newSearch.getURL())",
            campos: "",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        if let status = json["status"] as? Int{
            
            switch status {
            case 1:
                
                if let response = json["ganhos"] as? NSArray{
                    
                    if response.count < 1 {
                        self.booleanRequestData = true
                    }else{
                        self.booleanRequestData = false
                        
                        for i in response{
                            
                            if let eachInformation = i as? NSDictionary{
                                
                                let unitRace = PaymentHistory()
                                
                                if let beginDate = eachInformation["data_corrida_inicio"] as? String{
                                    unitRace.setBeginDate(date: beginDate)
                                }
                                
                                if let endDate = eachInformation["data_corrida_fim"] as? String{
                                    unitRace.setEndDate(date: endDate)
                                }
                                
                                if let id = (eachInformation["id_corrida"] as AnyObject).integerValue{
                                    unitRace.setID(id: id)
                                }
                                
                                if let name = eachInformation["nome_usuario"] as? String{
                                    unitRace.setName(name: name)
                                }
                                
                                if let value = eachInformation["valor_corrida"] as? String{
                                    unitRace.setValue(value: value)
                                }
                                
                                if let payment = (eachInformation["pagamento_motorista"] as AnyObject).integerValue{
                                    unitRace.setStatus(status: payment)
                                }
                                
                                if self.arraySearch.contains(where: {$0.getID() == unitRace.getID()}){
                                    print("This object already exist in array")
                                }else{
                                    self.arraySearch.append(unitRace)
                                }
                                self.newSearch.setSelfQuantity(quantity: arraySearch.count)
                            }
                            
                        }
                        
                        
                    }
                    self.paymentTable.reloadData()
                }
                
            default:
                break
            }
            
        }
        
        
    }
    
}
