//
//  Evaluations.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 15/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class Evaluations {
    
    
    private var punctualityRating: Double
    private var sympathyRating:    Double
    private var carRating:         Double
    private var serviceRating:     Double
    
    
    init(ponctuality: Double, sympathy: Double, car: Double, service: Double) {
        
        self.punctualityRating = ponctuality
        self.sympathyRating    = sympathy
        self.carRating         = car
        self.serviceRating     = service
        
    }
    
    init() {
        
        self.punctualityRating = 2.5
        self.sympathyRating    = 2.5
        self.carRating         = 2.5
        self.serviceRating     = 2.5
        
    }
    
//    punctualityRating
    public func getPunctuality() -> Double{
        return self.punctualityRating
    }
    
    public func setPunctyality(punctuality: Double){
        self.punctualityRating = punctuality
    }
    
//    sympathyRating
    public func getSympathy() -> Double{
        return self.sympathyRating
    }
    
    public func setSympathy(sympathy: Double){
        self.sympathyRating = sympathy
    }
    
//    carRating
    public func getCar() -> Double{
        return self.carRating
    }
    
    public func setCar(car: Double){
        self.carRating = car
    }
    
//    serviceRating
    public func getService() -> Double{
        return self.serviceRating
    }
    
    public func setService(service: Double){
        self.serviceRating = service
    }
}
