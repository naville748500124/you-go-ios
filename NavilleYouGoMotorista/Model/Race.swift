//
//  Race.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 09/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class Race{
    
    private var customerOriginLatitude          : Double
    private var customerOriginLongitude         : Double
    private var customerOriginAddress           : String
    private var customerDestinationLatitude     : Double
    private var customerDestinationLongitude    : Double
    private var customerDestinationAddress      : String
    private var customerBags                    : Int
    private var customerAmount                  : Int
    private var customerIDRace                  : Int
    private var customerPhone                   : String
    private var customerName                    : String
    private var customerIDSolicitation          : Int
    private var customerIDCustomer              : Int
    private var customerRaceCost                : Double
    private var statusRace: [GenericField]
    
    
   
   

    
    init(latitudeOrigin: Double, longitudeOrigin: Double, addressOrigin: String, latitudeDestination: Double, longitudeDestination: Double, addressDestination: String, bags: Int, amount: Int, idRace: Int, phone: String, name: String, idSolicitation: Int, idCustomer: Int,customerRaceCost: Double, statusRace: [GenericField]) {
        
        self.customerOriginLatitude         = latitudeOrigin
        self.customerOriginLongitude        = longitudeOrigin
        self.customerOriginAddress          = addressOrigin
        self.customerDestinationLatitude    = latitudeDestination
        self.customerDestinationLongitude   = longitudeDestination
        self.customerDestinationAddress     = addressDestination
        self.customerBags                   = bags
        self.customerAmount                 = amount
        self.customerName                   = name
        self.customerPhone                  = phone
        self.customerIDRace                 = idRace
        self.customerIDCustomer             = idCustomer
        self.customerIDSolicitation         = idSolicitation
        self.statusRace                     = statusRace
        self.customerRaceCost               = customerRaceCost
    }
    
    init() {
        self.customerOriginLatitude         = 0.0
        self.customerOriginLongitude        = 0.0
        self.customerOriginAddress          = ""
        self.customerDestinationLatitude    = 0.0
        self.customerDestinationLongitude   = 0.0
        self.customerDestinationAddress     = ""
        self.customerBags                   = 0
        self.customerAmount                 = 0
        self.customerName                   = ""
        self.customerPhone                  = ""
        self.customerIDRace                 = 0
        self.customerIDCustomer             = 0
        self.customerIDSolicitation         = 0
        self.statusRace                     = [GenericField]()
        customerRaceCost                    = 0.0
    }
    
    
    //customerOriginLatitude
    public func getOriginLatitude() -> Double{
        return self.customerOriginLatitude
    }
    
    public func setOriginLatitude(latitude: Double){
        self.customerOriginLatitude = latitude
    }
    
    //customerOriginLongitude
    public func getOriginLongitude() -> Double{
        return self.customerOriginLongitude
    }
    
    public func setOriginLongitude(longitude: Double){
        self.customerOriginLongitude = longitude
    }
    
    //customerOriginAddress
    public func getOriginAddress() -> String{
        return self.customerOriginAddress
    }
    
    public func setOriginAddress(address: String){
        self.customerOriginAddress = address
    }
    
    //customerDestinationLatitude
    public func getDestinationLatitude() -> Double{
        return self.customerDestinationLatitude
    }
    
    public func setDestinationLatitude(latitude: Double){
        self.customerDestinationLatitude = latitude
    }
    
    //customerDestinationLongitude
    public func getDestinationLongitude() -> Double{
        return self.customerDestinationLongitude
    }
    
    public func setDestinationLongitude(longitude: Double){
        self.customerDestinationLongitude = longitude
    }
    
    //customerDestinationAddress
    public func getDestinationAddress() -> String{
        return self.customerDestinationAddress
    }
    
    public func setDestinationAddress(address: String){
        self.customerDestinationAddress = address
    }
    
    
    //customerBags
    public func getBags() -> Int{
        return self.customerBags
    }
    
    public func setBags(bags: Int){
        self.customerBags = bags
    }
    
    //customerAmount
    public func getAmount() -> Int{
        return self.customerAmount
    }
    
    public func setAmount(amount: Int){
        self.customerAmount = amount
    }
    
    //customerName
    public func getName() -> String{
        return self.customerName
    }
    
    public func setName(name: String){
        self.customerName = name
    }
    
    //customerPhone
    public func getPhone() -> String{
        return self.customerPhone
    }
    
    public func setPhone(phone: String){
        self.customerPhone = phone
    }
    
    //customerIDRace
    public func getIDRace() -> Int{
        return self.customerIDRace
    }
    
    public func setIDRace(idRace: Int){
        self.customerIDRace = idRace
    }
    
    //customerIDSolicitation
    public func getIDSolicitation() -> Int{
        return self.customerIDSolicitation
    }
    
    public func setIDSolicitation(idSolicitation: Int){
        self.customerIDSolicitation = idSolicitation
    }
    
    //customerIDCustomer
    public func getIDCustomer() -> Int{
        return self.customerIDCustomer
    }

    public func setIDCustomer(idCustomer: Int){
        self.customerIDCustomer = idCustomer
    }
    
    //customerRaceCost
    public func getRaceCost() -> Double{
        return self.customerRaceCost
    }
    
    public func setRaceCost(customerRaceCost: Double){
        self.customerRaceCost = customerRaceCost
    }
    
    //statusRacce
    public func getStatusRace() -> [GenericField]{
        return self.statusRace
    }
    
    public func setStatusRace(statusRace: [GenericField]){
        self.statusRace = statusRace
    }
    
}
