//
//  Protocols.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 07/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation


protocol ActivateSMSDelegate {
    func activeSMS(email: String, password: String)
}
