//
//  ActionsPersonalData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 08/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

extension PersonalDataViewController: retornoJsonDelegate{
    
    internal func findZipCode(){
        
        if (self.textFieldZipCode.text?.count)! < 9 {
            avisoToast("O cep precisa ter 8 números.", posicao: 3, altura: 45, tipo: 3)
            
        }else{
            
            let zipCode = self.textFieldZipCode.text!.replacingOccurrences(of: "-", with: "")
            self.functionController = 1
            
            let ws = WebService(
                funcao: "buscar_cep?cep=\(zipCode)",
                campos:"",
                authUsuario:"",
                authSenha:"",
                tipo:"GET",
                ativarIndicator: true,
                token: ""
            );
            
            ws.setUrlServidor(urlServidor: self.shared.serverShared)
            ws.conectar()
            ws.retornoDelegate = self
        }
        
    }
    
    internal func updateDriver(){
        
        self.functionController = 2
        
        let ws = WebService(
            funcao: "editar_cadastro_motorista",
            campos: self.postFields(),
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        
        if let status = json["status"] as? Int{
            
            switch status{
                
            case 1://request was a success
                
                switch self.functionController{
                case 1:
                    if let eachInformation: NSDictionary = json["endereco"] as? NSDictionary{
                        
                        
                        if let neighborhood: String = eachInformation["bairro"] as? String{
                            self.textFieldNeighborhood.text = neighborhood
                        }
                        
                        if let zipCode: String = eachInformation["cep"] as? String{
                            self.textFieldZipCode.text = zipCode
                        }
                        
                        if let city: String = eachInformation["localidade"] as? String{
                            self.textFieldCity.text = city
                        }
                        
                        if let address: String = eachInformation["logradouro"] as? String{
                            self.textFieldAddress.text = address
                        }
                        
                        if let state: String = eachInformation["uf"] as? String{
                            self.textFieldState.text = state
                            
                            if STATES.contains(where: {$0.name == state}){
                                
                                for i in STATES{
                                    
                                    if i.name == state {
                                        self.personalData.setIdState(idState: i.id)
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                        
                        if (textFieldNeighborhood.text?.isEmpty)! && (textFieldCity.text?.isEmpty)! && (textFieldAddress.text?.isEmpty)! {
                            let alert = UIAlertController(title: "Aviso", message: "Não foi possível encontrar endereço para este CEP.", preferredStyle: .alert)
                            let ok    = UIAlertAction(title: "Ok", style: .default , handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                case 2:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 1)
                    self.shared.userEmail = self.textFieldEmail.text!
                    self.shared.userName  = self.textFieldName.text!
                default:
                    break
                }
                
            case 0://did not find nothing
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
                
            }
            
        }
        
    }
    
}
