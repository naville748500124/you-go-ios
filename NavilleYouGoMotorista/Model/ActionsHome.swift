//
//  ActionsHome.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 08/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension HomeViewController: retornoJsonDelegate{
    
    //turn "ON" of turn "OFF", driver status
    internal func onlineOffline(){
        
        if CLLocationManager.authorizationStatus().rawValue == 2 {
            
            let alert = UIAlertController(title: "Aviso", message: "Você precisa autorizar o uso da \'Localização\' durante o uso do aplicativo.", preferredStyle: .alert)
            let close = UIAlertAction(title: "Fechar", style: .destructive, handler: nil)
            let active = UIAlertAction(title: "Ativar", style: .default , handler: { (action) in
                
                guard let appSettings = URL(string: UIApplicationOpenSettingsURLString) else { return }
                UIApplication.shared.open(appSettings, completionHandler: nil)
                
            })
            alert.addAction(close)
            alert.addAction(active)
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            var status: Int?
            if booleanOnOffline{
                status = 1
            }else{
                status = 0
            }
            self.stopTimer()
            
            self.functionController = 1
            let ws = WebService(
                funcao:"motorista_disponivel",
                campos:"online=\(status!)",
                authUsuario: "",
                authSenha: "",
                tipo:"POST",
                ativarIndicator: true,
                token: self.shared.userToken
            );
            
            ws.setUrlServidor(urlServidor: self.shared.serverShared)
            ws.conectar()
            ws.retornoDelegate = self
            
        }
        
    }
    
    //continues one race, if the application was crash
    internal func continuesRacing(idRace: Int){
        
        self.functionController = 2
        let ws = WebService(
            funcao:"deslocamento_motorista",
            campos:"latitude_atual=\(self.userLatitude)&longitude_atual=\(self.userLongitude)&id_corrida=\(idRace)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: false,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    //sending driver location to webservice, each 5 seconds
    internal func sendMyLocation(){
        
       
        self.functionController = 2
        let ws = WebService(
            funcao:"deslocamento_motorista",
            campos:"latitude_atual=\(self.userLatitude)&longitude_atual=\(self.userLongitude)&id_corrida=\(self.newRace.getIDRace())",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: false,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    //this function accept or refuse one race
    internal func acceptOneRace(){
        
        self.stopTimer()
        self.functionController = 3
        let ws = WebService(
            funcao:"responder_corrida",
            campos:"id_corrida=\(self.newRace.getIDRace())&id_solicitacao_corrida=\(self.newRace.getIDSolicitation())&resposta=\(self.acceptOrRefuseRace)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    //this function cancel one race, already accept
    internal func cancelOneRace(justifield: String){
        
        self.functionController = 4
        let ws = WebService(
            funcao:"cancelar_corrida",
            campos:"id_corrida=\(self.newRace.getIDRace())&justificativa=\(justifield)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    
    //this function start new race
    internal func startNewRace(){
        
        self.functionController = 5
        let ws = WebService(
            funcao:"iniciar_corrida",
            campos:"id_corrida=\(self.newRace.getIDRace())&latitude_atual=\(self.userLatitude)&longitude_atual=\(self.userLongitude)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    //this function finalize one race
    internal func finishRace() {
        
        self.functionController = 6
        let ws = WebService(
            funcao:"finalizar_corrida",
            campos:"id_corrida=\(self.newRace.getIDRace())&latitude_atual=\(self.userLatitude)&longitude_atual=\(self.userLongitude)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
 
    
    //this function the driver will evaluation the customer
    @objc internal func evaluationCustomer(sender: CustomButton){
        
        self.buttonOnlineOffiline.isHidden = false
        self.functionController = 7
        let ws = WebService(
            funcao:"avaliar_corrida",
            campos:"id_corrida=\(sender.idRace!)&pontualidade_passageiro=\(sender.punctuality!)&simpatia_passageiro=\(sender.sympathy!)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    
    //this function will find a race if the app crashed
    internal func followRace(idRace: Int){
        self.functionController = 8
        let ws = WebService(
            funcao:"acompanhar_corrida",
            campos:"id_corrida=\(idRace)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        
        if let status = json["status"] as? Int{
            switch status{
                
            case 1://request was a success
               
                switch self.functionController{
                    
                case 1://onlineOffline
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    if self.booleanOnOffline{
                        
                        //self.locationManager.startUpdatingLocation()
                        self.buttonOnlineOffiline.setTitle("Status: Online", for: .normal)
                        self.buttonOnlineOffiline.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
                        startTimer()
                        
                    }else{
                        
                        self.buttonOnlineOffiline.setTitle("Status: Offline", for: .normal)
                        self.buttonOnlineOffiline.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                        stopTimer()
                        
                    }
                    self.booleanOnOffline = !booleanOnOffline
                    
                case 2://sendMyLocation
                    
                    
                    if let raceData = json["dados_corrida"] as? NSDictionary{
                        
                        if let addressOrigin = raceData["endereco_origem"] as? String{
                            self.newRace.setOriginAddress(address: addressOrigin)
                        }
                        
                        if let latitudeOrigin = (raceData["latitude_origem"] as AnyObject).doubleValue {
                            self.newRace.setOriginLatitude(latitude: latitudeOrigin)
                        }
                        
                        if let longitudeOrigin = (raceData["longitude_origem"] as AnyObject).doubleValue {
                            self.newRace.setOriginLongitude(longitude: longitudeOrigin)
                        }
                        
                        if let addressDestination = raceData["endereco_destino"] as? String{
                            self.newRace.setDestinationAddress(address: addressDestination)
                        }
                        
                        if let latitudeDestination = (raceData["latitude_destino"] as AnyObject).doubleValue {
                            self.newRace.setDestinationLatitude(latitude: latitudeDestination)
                        }
                        
                        if let longitudeDestination = (raceData["longitude_destino"] as AnyObject).doubleValue {
                            self.newRace.setDestinationLongitude(longitude: longitudeDestination)
                        }
                        
                        if let amount = (raceData["qtd_passageiros"] as AnyObject).integerValue {
                            self.newRace.setAmount(amount: amount)
                        }
                        
                        if let bags = (raceData["qtd_malas"] as AnyObject).integerValue {
                            self.newRace.setBags(bags: bags)
                        }
                        
                        if let name = raceData["nome_usuario"] as? String{
                            self.newRace.setName(name: name)
                        }
                        
                        if let phone = raceData["celular_usuario"] as? String{
                            self.newRace.setPhone(phone: phone)
                        }
                        
                        if let idSolicitation = (raceData["id_solicitacao"] as AnyObject).integerValue {
                            self.newRace.setIDSolicitation(idSolicitation: idSolicitation)
                        }
                        
                        if let idCustomer = (raceData["id_passageiro"] as AnyObject).integerValue {
                            self.newRace.setIDCustomer(idCustomer: idCustomer)
                        }
                        
                        if let costRace = (raceData["valor_corrida"] as AnyObject).doubleValue{
                            self.newRace.setRaceCost(customerRaceCost: costRace)
                        }
                        
                        if let idRace = (raceData["id_corrida"] as AnyObject).integerValue {
                            
                            
                            if(self.newRace.getIDRace() < idRace){
                               //self.stopTimer()
                                self.viewEvaluation.removeFromSuperview()
                                for subview in self.viewEvaluation.subviews {
                                    if subview.isDescendant(of: viewEvaluation){
                                        subview.removeFromSuperview()
                                    }
                                }
                                self.newRace.setIDRace(idRace: idRace)
                                self.routeCustomer(timer: (raceData["tempo_restante"] as AnyObject).integerValue)
                                self.createDetailsView()
                                self.viewDetailExist = true
                                //self.startTimer()
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    if let statusRace = json["status_corrida"] as? NSDictionary{
                        
                        if let id = (statusRace["cod"] as AnyObject).integerValue {
                            if let status = statusRace["status"] as? String{
                                
                                self.newRace.setStatusRace(statusRace: [GenericField(id: id, name: status)])
                                
                            }
                        }
                        
                    }
                    
                   
                    
                    
                    if let validate: [GenericField] = self.newRace.getStatusRace() as? [GenericField]{
                        
                       
                        
                        if validate.first?.id == 87 || validate.first?.id == 89 {//race cancelled
                            print("chegou aqui 1 ")
                            self.timerButton.invalidate()
                            self.map.overlays.forEach {
                                
                                if !($0 is MKUserLocation){
                                    self.map.remove($0)
                                    self.map.removeAnnotations(self.map.annotations)
                                    self.locationManager.startUpdatingLocation()
                                }
                            }
                            
                            openView(validate: booleanViewDetails, constraint: heightConstraintViewDetails!, number: 0, time: 0.7)
                            self.performSegue(withIdentifier: SEGUE_TO_MAKE_PAYMENT, sender: self.newRace.getIDRace())
                            self.newRace = Race()
                            UserDefaults.standard.set(self.newRace.getIDRace(), forKey: "UserIDRace")
                            viewRace.removeFromSuperview()
                            viewDetail.removeFromSuperview()
                            
                            for subview in self.viewRace.subviews{
                                if subview.isDescendant(of: viewRace){
                                    subview.removeFromSuperview()
                                }
                            }
                            for subview in self.viewDetail.subviews {
                                if subview.isDescendant(of: viewDetail){
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            self.buttonOnlineOffiline.isHidden = false
                            self.mapZoomUserLocation()
                            
                            let alert = UIAlertController(title: "Aviso", message: "Infelizmente o cliente cancelou a viagem.", preferredStyle: .alert)
                            let ok    = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if validate.first?.id == 86 {//race finished
                            
                            self.stopTimer()
                            
                            createViewEvaluations(raceID: self.newRace.getIDRace(), passengerID: self.newRace.getIDCustomer())
                            
                            self.newRace = Race()
                            UserDefaults.standard.set(self.newRace.getIDRace(), forKey: "UserIDRace")
                            self.map.overlays.forEach {
                                
                                if !($0 is MKUserLocation){
                                    self.map.remove($0)
                                    self.map.removeAnnotations(self.map.annotations)
                                    self.locationManager.startUpdatingLocation()
                                }
                            }
                            
                            openView(validate: booleanViewDetails, constraint: heightConstraintViewDetails!, number: 0, time: 0.7)
                            
                            viewDetail.removeFromSuperview()
                            for subview in self.viewDetail.subviews {
                                if subview.isDescendant(of: viewDetail){
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            self.viewDetailExist = false
                            self.booleanViewDetails = false
                            self.booleanProgressRace = false
                            //self.buttonOnlineOffiline.isHidden = false
                            self.mapZoomUserLocation()
                            
                        }else if validate.first?.id == 85 {//race in progress
                            
                            if self.booleanProgressRace == false{
                                self.booleanProgressRace = true
                                self.map.overlays.forEach {
                                    
                                    if !($0 is MKUserLocation){
                                        self.map.remove($0)
                                        self.map.removeAnnotations(self.map.annotations)
                                        self.locationManager.startUpdatingLocation()
                                    }
                                }
                                
                                self.routeDestination(validate: 0)
                                
                                self.cancelButton.backgroundColor = UIColor.darkGray
                                
                                
                                self.buttonGo.setTitle("Finalizar corrida", for: .normal)
                                self.buttonGo.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                                self.chooseNavigation(originOrDestination: false)
                                
                            }
                            
                        }else if validate.first?.id == 84 {//customer is waiting driver arrived
                            
                            if self.viewDetailExist == false{
                                //createDetailsView()
                            }
                            
                            self.timerButton.invalidate()
                            self.viewRace.removeFromSuperview()
                            for subview in self.viewRace.subviews {
                                if subview.isDescendant(of: viewRace){
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            UserDefaults.standard.set(self.newRace.getIDRace(), forKey: "UserIDRace")
                            
                            if let first = self.map.overlays.first {
                                let rect = self.map.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                                self.map.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 100.0, left: 40.0, bottom: 110.0, right: 40.0), animated: true)
                            }
                            
                        }else if validate.first?.id == 88 {//driver cancelled
                            
                            self.newRace = Race()
                            
                            UserDefaults.standard.set(self.newRace.getIDRace(), forKey: "UserIDRace")
                            self.map.overlays.forEach {
                                
                                if !($0 is MKUserLocation){
                                    self.map.remove($0)
                                    self.map.removeAnnotations(self.map.annotations)
                                    self.locationManager.startUpdatingLocation()
                                }
                            }
                            openView(validate: booleanViewDetails, constraint: heightConstraintViewDetails!, number: 0, time: 0.7)
                            
                            viewDetail.removeFromSuperview()
                            for subview in self.viewDetail.subviews {
                                if subview.isDescendant(of: viewDetail){
                                    subview.removeFromSuperview()
                                }
                            }
                            
                            self.viewDetailExist = false
                            self.booleanViewDetails = false
                            self.buttonOnlineOffiline.isHidden = false
                            self.mapZoomUserLocation()
                            self.startTimer()
                            
                        }
                        
                        if self.newRace.getIDRace() > 0 && self.viewDetailExist == false && validate.first?.id == 84{
                            print("deveria aqui criar a tela que nao foi criada em cima")
                            
                        }
                        
                    }
                    
                    
                    
                case 3://acceptOneRace
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    
                    if self.acceptOrRefuseRace == ID_ACCEPT_RACE {
                        print("accept")
                        self.startTimer()
                        visibleDetailView()
                    }else{
                        print("reffuse")
                        
                        self.timerButton.invalidate()
                        self.viewRace.removeFromSuperview()
                        for subview in self.viewRace.subviews {
                            if subview.isDescendant(of: viewRace){
                                subview.removeFromSuperview()
                            }
                        }
                        
                        self.newRace = Race()
                        UserDefaults.standard.set(self.newRace.getIDRace(), forKey: "UserIDRace")
                        viewDetail.removeFromSuperview()
                        for subview in self.viewDetail.subviews {
                            if subview.isDescendant(of: viewDetail){
                                subview.removeFromSuperview()
                            }
                        }
                        
                        self.map.overlays.forEach {
                            
                            if !($0 is MKUserLocation){
                                self.map.remove($0)
                                self.map.removeAnnotations(self.map.annotations)
                                self.locationManager.startUpdatingLocation()
                            }
                        }
                        self.startTimer()
                        self.buttonOnlineOffiline.isHidden = false
                        self.mapZoomUserLocation()
                    }
                    
                case 4://cancelOneRace
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    self.viewDetailExist = false
                    
                    
                case 5://startNewRace
                    self.visibleDetailView()
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    
                case 6://finishRace
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    self.visibleDetailView()
                    
                case 7://evaluationCustomer
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 2)
                    self.viewEvaluation.removeFromSuperview()
                    for subview in self.viewEvaluation.subviews {
                        if subview.isDescendant(of: viewEvaluation){
                            subview.removeFromSuperview()
                        }
                    }
                    self.startTimer()
                    
                case 8://followRace
                   
                    if let data = json["dados"] as? NSDictionary{
                        
                        self.newRace = Race()
                        if let cellPhone = data["celular_usuario"] as? String{
                            self.newRace.setPhone(phone: cellPhone)
                        }
                        
                        if let originAddress = data["endereco_origem"] as? String{
                            self.newRace.setOriginAddress(address: originAddress)
                        }
                        
                        if let destinationAddress = data["endereco_destino"] as? String{
                            self.newRace.setDestinationAddress(address: destinationAddress)
                        }
                        
                        if let idRace = (data["id_corrida"] as AnyObject).integerValue{
                            self.newRace.setIDRace(idRace: idRace)
                        }
                        
                        if let idCustomer = (data["id_passageiro"] as AnyObject).integerValue{
                            self.newRace.setIDCustomer(idCustomer: idCustomer)
                        }
                        
                        if let latitudeOrigin = (data["latitude_origem"] as AnyObject).doubleValue{
                            self.newRace.setOriginLatitude(latitude: latitudeOrigin)
                        }
                        
                        if let latitudeDestination = (data["latitude_destino"] as AnyObject).doubleValue{
                            self.newRace.setDestinationLatitude(latitude: latitudeDestination)
                        }
                        
                        if let longitudeOrigin = (data["longitude_origem"] as AnyObject).doubleValue{
                            self.newRace.setOriginLongitude(longitude: longitudeOrigin)
                        }
                        
                        if let longitudeDestination = (data["longitude_destino"] as AnyObject).doubleValue{
                            self.newRace.setDestinationLongitude(longitude: longitudeDestination)
                        }
                        
                        if let nameCustomer = data["nome_usuario"] as? String{
                            self.newRace.setName(name: nameCustomer)
                        }
                        
                        if let bags = (data["qtd_malas"] as AnyObject).integerValue{
                            self.newRace.setBags(bags: bags)
                        }
                        
                        if let amount = (data["qtd_passageiros"] as AnyObject).integerValue{
                            self.newRace.setAmount(amount: amount)
                        }
                        
                        if let costRace = (data["valor_corrida"] as AnyObject).doubleValue{
                            self.newRace.setRaceCost(customerRaceCost: costRace)
                        }
                        
                        if let statusRace = data["status"] as? String{
                            if let cod = (data["cod"] as AnyObject).integerValue {
                                self.newRace.setStatusRace(statusRace: [GenericField(id: cod, name: statusRace)])
                            }
                        }
                        createDetailsView()
                        self.viewDetailExist = true
                        if self.newRace.getStatusRace().first?.id == 84 {
                            
                            self.booleanOnOffline = !booleanOnOffline
                            self.buttonOnlineOffiline.isHidden = true
                            startTimer()
                            self.routeCustomer(timer: -1)
                            
                        }else if self.newRace.getStatusRace().first?.id == 85{
                            
                            self.booleanOnOffline = !booleanOnOffline
                            self.buttonOnlineOffiline.isHidden = true
                            startTimer()
                            self.routeDestination(validate: 1)
                        }else if self.newRace.getStatusRace().first?.id == 87 {
                            
                            self.booleanOnOffline = !booleanOnOffline
                            startTimer()
                        }else if self.newRace.getStatusRace().first?.id == 88{
                            
                            if let onlineDriver = (json["motorista_online"] as AnyObject).integerValue {
                                if self.newRace.getStatusRace().first?.id == 88 && onlineDriver == 0{
                                    print("Chegou aqui")
                                    self.stopTimer()
                                    self.booleanOnOffline = false
                                    self.onlineOffline()
                                }
                            }
                        }
                        
                    }
                    
                    
                default:
                    break
                }
                
            case 0://did not find nothing
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9: //catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10: //internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
                
            }
            
        }
        
    }
    
}
