//
//  DetailPerformRaceData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 14/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class DetailPerformRaceData {
    
    private var car                 : String
    private var beginDate           : String
    private var endDate             : String
    private var originAddress       : String
    private var destinationAddress  : String
    private var justificationCancel : String
    private var customerName        : String
    private var licensePlate        : String
    private var statusRace          : String
    private var costRace            : String
    private var idRace              : Int
    private var idCustomer          : Int
    private var distanceKM          : Double
    private var customerEvaluate    : Int
    private var driverEvaluate      : Int
    
    init(car: String, beginDate: String, endDate: String, originAddress: String, destinationAddress: String, justificationCancel: String, customerName: String, licensePlate: String, statusRace: String, costRace: String, idRace: Int, idCustomer: Int, distanceKM: Double, customerEvaluate: Int, driverEvaluate: Int) {
        
        self.car                    = car
        self.beginDate              = beginDate
        self.endDate                = endDate
        self.originAddress          = originAddress
        self.destinationAddress     = destinationAddress
        self.justificationCancel    = justificationCancel
        self.customerName           = customerName
        self.licensePlate           = licensePlate
        self.statusRace             = statusRace
        self.costRace               = costRace
        self.idRace                 = idRace
        self.idCustomer             = idCustomer
        self.distanceKM             = distanceKM
        self.customerEvaluate       = customerEvaluate
        self.driverEvaluate         = driverEvaluate
        
    }
    
    init() {
        
        self.car                    = ""
        self.beginDate              = ""
        self.endDate                = ""
        self.originAddress          = ""
        self.destinationAddress     = ""
        self.justificationCancel    = ""
        self.customerName           = ""
        self.licensePlate           = ""
        self.statusRace             = ""
        self.costRace               = ""
        self.idRace                 = 0
        self.idCustomer             = 0
        self.distanceKM             = 0.0
        self.customerEvaluate       = -1
        self.driverEvaluate         = -1
        
    }
    
//    self.car
    public func getCar() -> String{
        return self.car
    }
    
    public func setCar(car: String){
        self.car = car
    }
    
//    self.beginDate
    public func getBeginDate() -> String{
        return self.beginDate
    }
    
    public func setBeginDate(beginDate: String){
        self.beginDate = beginDate
    }
    
//    self.endDate
    public func getEndDate() -> String{
        return self.endDate
    }
    
    public func setEndDate(endDate: String){
        self.endDate = endDate
    }
    
//    self.originAddress
    public func getOriginAddress() -> String{
        return self.originAddress
    }
    
    public func setOriginAddress(originAddress: String){
        self.originAddress = originAddress
    }
    
//    self.destinationAddress
    public func getDestinationAddress() -> String{
        return self.destinationAddress
    }
    
    public func setDestinationAddress(destinationAddress: String){
        self.destinationAddress = destinationAddress
    }
    
//    self.justificationCancel
    public func getJustification() -> String{
        return self.justificationCancel
    }
    
    public func setJustification(justification: String){
        self.justificationCancel = justification
    }
    
//    self.customerName
    public func getCustomerName() -> String{
        return self.customerName
    }
    
    public func setCustomerName(customerName: String){
        self.customerName = customerName
    }
    
//    self.licensePlate
    public func getLicensePlate() -> String{
        return self.licensePlate
    }
    
    public func setLicensePlate(licensePlate: String){
        self.licensePlate = licensePlate
    }
    
//    self.statusRace
    public func getStatus() -> String{
        return self.statusRace
    }
    
    public func setStatus(status: String){
        self.statusRace = status
    }
    
//    self.costRace
    public func getCost() -> String{
        return self.costRace
    }
    
    public func setCost(cost: String){
        self.costRace = cost
    }
    
//    self.idRace
    public func getIDRace() -> Int{
        return self.idRace
    }
    
    public func setIDRace(idRace: Int){
        self.idRace = idRace
    }
    
//    self.idCustomer
    public func getIDCustomer() -> Int{
        return self.idCustomer
    }
    
    public func setIDCustomer(idCustomer: Int){
        self.idCustomer = idCustomer
    }
    
//    self.distanceKM
    public func getDistance() -> Double{
        return self.distanceKM
    }
    
    public func setDistance(distance: Double){
        self.distanceKM = distance
    }
    
//    self.customerEvaluate
    public func getCustomerEvaluate() -> Int{
        return self.customerEvaluate
    }
    
    public func setCustomerEvaluate(customerEvaluate: Int){
        self.customerEvaluate = customerEvaluate
    }
    
//    driverEvaluate
    public func getDriverEvaluate() -> Int{
        return self.driverEvaluate
    }
    
    public func setDriverEvaluate(driverEvaluate: Int){
        self.driverEvaluate = driverEvaluate
    }
    
}
