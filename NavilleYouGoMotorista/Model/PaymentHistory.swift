//
//  PaymentHistory.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 25/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class PaymentHistory{
    
    private var beginDate:      String
    private var endDate:        String
    private var id:             Int
    private var name:           String
    private var status:         Int
    private var value:          String
    
    init(beginDate: String, endDate: String, status: Int, name: String, id: Int, value: String) {
        
        self.beginDate      = beginDate
        self.endDate        = endDate
        self.status         = status
        self.name           = name
        self.id             = id
        self.value          = value
        
    }
    
    init() {
        
        self.beginDate      = ""
        self.endDate        = ""
        self.status         = -1
        self.name           = ""
        self.id             = 0
        self.value          = ""
        
    }
    
    //    beginDate
    public func getBeginDate() -> String{
        return self.beginDate
    }
    
    public func setBeginDate(date: String){
        self.beginDate = date
    }
    
    //    endDate
    public func getEndDate() -> String{
        return self.endDate
    }
    
    public func setEndDate(date: String){
        self.endDate = date
    }
    
    //    status
    public func getStatus() -> Int{
        return self.status
    }
    
    public func setStatus(status: Int){
        self.status = status
    }
    
    //    id
    public func getID() -> Int{
        return self.id
    }
    
    public func setID(id: Int){
        self.id = id
    }
    
    //    nextQuantity
    public func getName() -> String{
        return self.name
    }
    
    public func setName(name: String){
        self.name = name
    }
    
//    value
    public func getValue() -> String{
        return self.value
    }
    
    public func setValue(value: String){
        self.value = value
    }
}
