//
//  ActionsRegisterThird.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 05/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension RegisterThirdScreenViewController: retornoJsonDelegate {
    
    internal func registerDriver(){
        
        let ws = WebService(
            funcao:"cadastro_motorista",
            campos: recoveryAllData(),
            authUsuario:"Usuário",
            authSenha:"Senha",
            tipo:"POST",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        
        if let status = json["status"] as? Int{
            
            switch status {
                
            case 1:
                
                if self.delegateSMS != nil {
                    
                    self.delegateSMS?.activeSMS(email: self.personalData.getEmail(), password: self.personalData.getPassword())
                    
                }
                
                delayWithSeconds(3, completion: {
                    self.avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 1)
                    self.navigationController?.popToRootViewController(animated: true)
                })
                
                
            case -9:
                avisoToast(json["resultado"] as! String , posicao: 3, altura: 45, tipo: 4)
            case -10:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case 0:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            default:
                break
                
            }
            
        }
        
    }
    
}
