//
//  PersonalData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 02/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class PersonalData{
    
    private var name:            String
    private var email:           String
    private var phone:           String
    private var idFacebook:      String?
    private var rg:              String
    private var cpf:             String
    private var cellPhone:       String
    private var address:         String
    private var city:            String
    private var idState:         Int
    private var neighborhood:    String
    private var zipCode:         String
    private var complement:      String?
    private var residenceNumber: String
    private var idGender:        Int
    private var password:        String
    private var facebookImage:   String?
    
    init(name: String, email: String, phone: String, idFacebook: String?, rg: String, cpf: String, cellPhone: String, address: String, city: String, idState: Int, neighborhood: String, zipCode: String, complement: String?, residenceNumber: String, idGender: Int, password: String, facebookImage: String?) {
        
        self.name =              name
        self.email =             email
        self.phone =             phone
        self.idFacebook =        idFacebook
        self.rg =                rg
        self.cpf =               cpf
        self.cellPhone =         cellPhone
        self.address =           address
        self.city =              city
        self.idState =           idState
        self.neighborhood =      neighborhood
        self.zipCode =           zipCode
        self.complement =        complement
        self.residenceNumber =   residenceNumber
        self.idGender =          idGender
        self.password =          password
        self.facebookImage =     facebookImage
    }
    
    init() {
        self.name =              ""
        self.email =             ""
        self.phone =             ""
        self.idFacebook =        ""
        self.rg =                ""
        self.cpf =               ""
        self.cellPhone =         ""
        self.address =           ""
        self.city =              ""
        self.idState =           0
        self.neighborhood =      ""
        self.zipCode =           ""
        self.complement =        ""
        self.residenceNumber =   ""
        self.idGender =          0
        self.password =          ""
        self.facebookImage =     ""
    }
    
    //name
    public func getDriverName() -> String {
        return self.name
    }
    
    public func setDriverName(name: String){
        self.name = name
    }
    
    //email
    public func getEmail() -> String{
        return self.email
    }
    
    public func setEmail(email: String){
        self.email = email
    }
    
    //phone
    public func getPhone() -> String{
        return phone
    }
    
    public func setPhone(phone: String){
        self.phone = phone
    }
    
    //idFacebook
    public func getIdFacebook() -> String?{
        return self.idFacebook
    }
    
    public func setIdFacebook(idFacebook: String?){
        if let idFacebook = idFacebook {
            self.idFacebook = idFacebook
        }
    }
    
    //rg
    public func getRG() -> String{
        return self.rg
    }
    
    public func setRG(rg: String){
        self.rg = rg
    }
    
    //cpf
    public func getCPF() -> String{
        return self.cpf
    }
    
    public func setCPF(cpf: String){
        self.cpf = cpf
    }
    
    //cellPhone
    public func getcellPhone() -> String{
        return cellPhone
    }
    
    public func setcellPhone(cellPhone: String){
        self.cellPhone = cellPhone
    }
    
    //address
    public func getAddress() -> String{
        return self.address
    }
    
    public func setAddress(address: String){
        self.address = address
    }
    
    //city
    public func getCity() -> String{
        return self.city
    }
    
    public func setCity(city: String){
        self.city = city
    }
    
    //idState
    public func getIdState() -> Int{
        return idState
    }
    
    public func setIdState(idState: Int){
        self.idState = idState
    }
    
    //neighborhood
    public func getNeighborhood() -> String{
        return neighborhood
    }
    public func setNeighborhood(neighborhood: String){
        self.neighborhood = neighborhood
    }
    
    //zipCode
    public func getZipCode() -> String{
        return self.zipCode
    }
    
    public func setZipCode(zipCode: String){
        self.zipCode = zipCode
    }
    
    //complement
    public func getComplement() -> String?{
        return self.complement
    }
    
    public func setComplement(complement: String?){
        if let complement = complement{
            self.complement = complement
        }
    }
    
    ///residenceNumber
    public func getResidenceNumber() -> String{
        return residenceNumber
    }
    
    public func setResidenceNumber(residenceNumber: String){
        self .residenceNumber = residenceNumber
    }
    
    //idGender
    public func getGender() -> Int{
        return self.idGender
    }
    
    public func setGender(idGender: Int){
        self.idGender = idGender
    }
    
    //password
    public func getPassword() -> String{
        return password
    }
    
    public func setPassword(password: String){
        self.password = password
    }
    
    //facebookImage
    public func getFacebookImage() -> String?{
        return self.facebookImage
    }
    
    public func setFacebookImage(facebookImage: String?){
        if let facebookImage = facebookImage{
            self.facebookImage = facebookImage
        }
    }
}
