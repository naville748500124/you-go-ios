//
//  ActionsLogin.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 06/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import AccountKit
import FBSDKLoginKit

extension LoginViewController: retornoJsonDelegate{
    
    func commonLogin(){
        
        self.functionController = 1
        let ws = WebService(
            funcao:"login_usuario?tipo_acesso=2&token=\(token)",
            campos:"",
            authUsuario: self.textFieldEmail.text!,
            authSenha: self.textFieldPassword.text!.sha1(),
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func facebookLogin(){
        
        self.functionController = 2
        let ws = WebService(
            funcao:"login_usuario?facebook_usuario=\(self.facebookID)&tipo_acesso=2&token=\(token)",
            campos:"",
            authUsuario: "",
            authSenha: "",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func activatorSMS(){
        
        self.functionController = 3
        let ws = WebService(
            funcao:"ativar_sms",
            campos:"id_usuario=\(self.userID)",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        if let status = json["status"] as? Int{
            
            switch status {
            case 1://return webservice
                
                switch self.functionController{
                    
                case 1://Common login
                    if let userData: Dictionary = json["usuario"] as? [String: AnyObject]{
                        
                        if let id = userData["id_usuario"] as? String{
                            self.shared.userID = id
                        }
                        
                        if let email = userData["email_usuario"] as? String{
                            self.shared.userEmail = email
                        }
                        
                        if let name = userData["nome_usuario"] as? String{
                            self.shared.userName = name
                        }
                        
                        if let password = userData["senha_usuario"] as? String{
                            self.shared.userPassword = password
                        }
                        
                        if let token = userData["token_acesso"] as? String{
                            self.shared.userToken = token
                        }
                        
                        if let lastRace = userData["ultima_corrida"] as? String{
                            self.shared.lastRace = lastRace
                        }
                        
                    }
                    
                    if let bankData: Dictionary = json["dados_bancarios"] as? [String: AnyObject]{
                        
                        if let id = bankData["fk_banco_motorista"] as? String{
                            self.shared.bankID = id
                        }
                        
                        if let agency = bankData["agencia_motorista"] as? String{
                            self.shared.agencyDriver = agency
                        }
                        
                        if let account = bankData["conta_motorista"] as? String{
                            self.shared.accountDriver = account
                        }
                        
                        if let digit = bankData["digito_motorista"] as? String{
                            self.shared.digitAccount = digit
                        }
                        
                    }
                    
                    var ufCompany           = ""
                    var cityCompany         = ""
                    var neighborhoodCompany = ""
                    var addressCompany      = ""
                    var numberCompany       = ""
                    var zipCodCompany       = ""
                    
                    
                    if let helpData: Dictionary = json["ajuda"] as? [String: AnyObject]{
                        
                        if let phone = helpData["tel_empresa"] as? String{
                            self.shared.phoneCompany = phone
                        }
                        
                        if let email = helpData["email_empresa"] as? String{
                            self.shared.emailCompany = email
                        }
                        
                        if let uf = helpData["uf_empresa"] as? String{
                            ufCompany = uf
                        }
                        
                        if let city = helpData["cidade_empresa"] as? String{
                            cityCompany = city
                        }
                        
                        if let neighborhood = helpData["bairro_empresa"] as? String{
                            neighborhoodCompany = neighborhood
                        }
                        
                        if let address = helpData["rua_empresa"] as? String{
                            addressCompany = address
                        }
                        
                        if let number = helpData["numero_empresa"] as? String{
                            numberCompany = number
                        }
                        
                        if let zipCod = helpData["cep_empresa"] as? String{
                            zipCodCompany = zipCod
                        }
                        
                        
                        shared.addressCompany = "\(addressCompany) - \(numberCompany), \(neighborhoodCompany) - \(cityCompany) - \(ufCompany), \(zipCodCompany) "
                    }
                    
                    UserDefaults.standard.set(self.textFieldEmail.text, forKey: "UserDefaultEmail")
                    UserDefaults.standard.set(self.textFieldPassword.text, forKey: "UserDefaultPassword")
                    
                    UIApplication.shared.registerForRemoteNotifications()
                    performSegue(withIdentifier: SEGUE_TO_HOME, sender: nil)
                    
                case 2://facebook login
                    
                    if status == 1 {
                        
                        if let userData: Dictionary = json["usuario"] as? [String: AnyObject]{
                            
                            if let id = userData["id_usuario"] as? String{
                                self.shared.userID = id
                            }
                            
                            if let email = userData["email_usuario"] as? String{
                                self.shared.userEmail = email
                            }
                            
                            if let name = userData["nome_usuario"] as? String{
                                self.shared.userName = name
                            }
                            
                            if let password = userData["senha_usuario"] as? String{
                                self.shared.userPassword = password
                            }
                            
                            if let token = userData["token_acesso"] as? String{
                                self.shared.userToken = token
                            }
                            
                            if let lastRace = userData["ultima_corrida"] as? String{
                                self.shared.lastRace = lastRace
                            }
                            
                        }
                        
                        if let bankData: Dictionary = json["dados_bancarios"] as? [String: AnyObject]{
                            
                            if let id = bankData["fk_banco_motorista"] as? String{
                                self.shared.bankID = id
                            }
                            
                            if let agency = bankData["agencia_motorista"] as? String{
                                self.shared.agencyDriver = agency
                            }
                            
                            if let account = bankData["conta_motorista"] as? String{
                                self.shared.accountDriver = account
                            }
                            
                            if let digit = bankData["digito_motorista"] as? String{
                                self.shared.digitAccount = digit
                            }
                            
                        }
                        
                        var ufCompany           = ""
                        var cityCompany         = ""
                        var neighborhoodCompany = ""
                        var addressCompany      = ""
                        var numberCompany       = ""
                        var zipCodCompany       = ""
                        
                        
                        if let helpData: Dictionary = json["ajuda"] as? [String: AnyObject]{
                            
                            if let phone = helpData["tel_empresa"] as? String{
                                self.shared.phoneCompany = phone
                            }
                            
                            if let email = helpData["email_empresa"] as? String{
                                self.shared.emailCompany = email
                            }
                            
                            if let uf = helpData["uf_empresa"] as? String{
                                ufCompany = uf
                            }
                            
                            if let city = helpData["cidade_empresa"] as? String{
                                cityCompany = city
                            }
                            
                            if let neighborhood = helpData["bairro_empresa"] as? String{
                                neighborhoodCompany = neighborhood
                            }
                            
                            if let address = helpData["rua_empresa"] as? String{
                                addressCompany = address
                            }
                            
                            if let number = helpData["numero_empresa"] as? String{
                                numberCompany = number
                            }
                            
                            if let zipCod = helpData["cep_empresa"] as? String{
                                zipCodCompany = zipCod
                            }
                            
                            
                            shared.addressCompany = "\(addressCompany) - \(numberCompany), \(neighborhoodCompany) - \(cityCompany) - \(ufCompany), \(zipCodCompany) "
                        }
                        
                        UIApplication.shared.registerForRemoteNotifications()
                        performSegue(withIdentifier: SEGUE_TO_HOME, sender: nil)
                    }
                    
                case 3 :
                    if self.saveIdFunction == 1 {
                        self.commonLogin()
                    }else if self.saveIdFunction == 2{
                        self.facebookLogin()
                    }
                    
                default:
                    break
                    
                }
                
            case 2:
                if let userData = json["usuario"] as? NSDictionary{
                    
                    self.userID = (userData["id_usuario"]! as AnyObject).integerValue
                    let number = AKFPhoneNumber(countryCode: "55", phoneNumber: userData["celular_usuario"] as! String)
                    let viewController = accountKit.viewControllerForPhoneLogin(with: number, state: generateState()) as AKFViewController
                    viewController.enableSendToFacebook = true
                    viewController.enableGetACall = true
                    viewController.delegate = self
                    
                    self.present(viewController as! UIViewController, animated: true, completion: nil)
                    
                }
                
            case 3:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                let facebookManager = FBSDKLoginManager()
                facebookManager.logOut()
            case 4:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                let facebookManager = FBSDKLoginManager()
                facebookManager.logOut()
            case 5:
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                let facebookManager = FBSDKLoginManager()
                facebookManager.logOut()
            case -9://error on catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
                let facebookManager = FBSDKLoginManager()
                facebookManager.logOut()
            case -10: // error internet connection
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case 0://return webservice
                
                switch self.functionController{
                case 1:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                    let facebookManager = FBSDKLoginManager()
                    facebookManager.logOut()
                case 2:
                    performSegue(withIdentifier: SEGUE_TO_REGISTER, sender: nil)
                default:
                    let facebookManager = FBSDKLoginManager()
                    facebookManager.logOut()
                    break
                }
            default:
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                break
            }
            
        }
        
    }
    
}
