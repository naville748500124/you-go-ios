//
//  ActionsVehicleData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 07/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

extension VehicleDataViewController: retornoJsonDelegate{
    
    internal func findModels(){
        
        self.functionController = 1
        let ws = WebService(
            funcao:"busca_modelo?id_montadora=\(self.vehicleData.getIdBrandCar())",
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func updateVehicle(){
        
        self.functionController = 2
        
        let ws = WebService(
            funcao: "editar_cadastro_motorista",
            campos: self.postFields(),
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        
        if let status = json["status"] as? Int{
            switch status {
            case 1://request was a success
                
                switch self.functionController{
                    
                case 1:
                    let models = json["modelos"] as! NSArray
                    
                    self.textFieldVehicleModel.isEnabled = true
                    self.modelsArray = [GenericField]()
                    for i in models {
                        
                        let eachInformation = i as! [String: AnyObject]
                        let id   = eachInformation["id_modelo"]?.integerValue
                        let name = eachInformation["modelo"] as! String
                        
                        self.modelsArray.append(GenericField(id: id!, name: name))
                    }
                    
                    if self.modelsArray.count > 0 {
                        self.textFieldVehicleModel.text = self.modelsArray.first?.name
                        self.vehicleData.setIdModelCar(idModelCar: (modelsArray.first?.id)!)
                    }
                    
                    if self.modelsArray.contains(where: {$0.id == self.vehicleData.getIdModelCar()}){
                        for i in modelsArray{
                            if i.id == self.vehicleData.getIdModelCar(){
                                self.textFieldVehicleModel.text = i.name
                            }
                        }
                    }
                    
                case 2:
                    if let status = json["status"] as? Int{
                        
                        if status == 1 {
                            let alert = UIAlertController(title: "Parabéns", message: (json["resultado"] as! String), preferredStyle: .alert)
                            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alert.addAction(ok)
                            present(alert, animated: true, completion: nil)
                        } else {
                            avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
                        }
                    }
                    
                default:
                    break
                }
                
            case 0://did not found nothing
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://catch
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10://internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
            }
        }
        
    }
    
}
