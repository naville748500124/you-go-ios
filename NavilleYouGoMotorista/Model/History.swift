//
//  History.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 25/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class History{
    
    private var beginDate:      String
    private var endDate:        String
    private var status:         Int
    private var selfQuantity:   Int
    private var nextQuantity:   Int
    
    init(beginDate: String, endDate: String, status: Int, selfQuantity: Int, nextQuantity: Int) {
        
        self.beginDate      = beginDate
        self.endDate        = endDate
        self.status         = status
        self.selfQuantity   = selfQuantity
        self.nextQuantity   = nextQuantity
        
    }
    
    init() {
        
        self.beginDate      = ""
        self.endDate        = ""
        self.status         = -1
        self.selfQuantity   = 0
        self.nextQuantity   = 10
    }
    
//    beginDate
    public func getBeginDate() -> String{
        return self.beginDate
    }
    
    public func setBeginDate(date: String){
        self.beginDate = date
    }
    
//    endDate
    public func getEndDate() -> String{
        return self.endDate
    }
    
    public func setEndDate(date: String){
        self.endDate = date
    }
    
//    status
    public func getStatus() -> Int{
        return self.status
    }
    
    public func setStatus(status: Int){
        self.status = status
    }
    
//    selfQuantity
    public func getSelfQuantity() -> Int{
        return self.selfQuantity
    }
    
    public func setSelfQuantity(quantity: Int){
        self.selfQuantity = quantity
    }
    
//    nextQuantity
    public func getNextQuantity() -> Int{
        return self.nextQuantity
    }
    
    public func setNextQuantity(quantity: Int){
        self.nextQuantity = quantity
    }
    
    internal func getURL() -> String{
        
        var url = "?quantidade_desejada=\(getNextQuantity())"
       
        if getBeginDate() != ""{
            url += "&data_corrida=\(getBeginDate())"
        }
        
        if getEndDate() != ""{
            url += "&data_fim_corrida=\(getEndDate())"
        }
        
        if getStatus() != -1 {
            url += "&pago=\(getStatus())"
        }
        
        if getSelfQuantity() >= 0 {
            url += "&quantidade_atual=\(getSelfQuantity())"
        }
        
        
        
        
        return url
    }
    
}
