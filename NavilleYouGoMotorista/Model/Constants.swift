//
//  Constants.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

//SEGUES
let SEGUE_TO_REGISTER               = "segue_to_register"
let SEGUE_TO_RESET_PASSWORD         = "segue_to_reset_password"
let SEGUE_TO_PRIVACY_POLICY         = "segue_to_privacy_policy"
let SEGUE_TO_HOME                   = "segue_to_home"
let SEGUE_SECOND_SCREEN_REGISTER    = "segue_to_register_second_screen"
let SEGUE_THIRD_SCREEN_REGISTER     = "segue_to_register_third_screen"
let SEGUE_TO_PERFORMED_RACES        = "segue_to_perform_race"
let SEGUE_TO_BANK_DATA              = "segue_to_bank_data"
let SEGUE_TO_EVALUATIONS_RECEIVED   = "segue_to_evaluations_received"
let SEGUE_TO_HELP                   = "segue_to_help"
let SEGUE_TO_PROFILE                = "segue_to_profile"
let SEGUE_TO_DATAIL_PERFORM         = "segue_to_datails"
let SEGUE_TO_PERSONAL_DATA          = "segue_to_personal_data"
let SEGUE_TO_VEHICLE_DATA           = "segue_to_vehicle_data"
let SEGUE_TO_PHOTO_DATA             = "segue_to_photo_data"
let SEGUE_TO_PAYMENT                = "segue_to_payment"
let SEGUE_TO_RACE_COST              = "segue_to_race_cost"
let SEGUE_TO_FAQ                    = "segue_to_faq"
let SEGUE_TO_PASSWORD               = "segue_to_password"
let SEGUE_TO_USED_TERMS             = "segue_to_use_terms"
let SEGUE_TO_WEBVIEW_PAYMENT        = "segue_to_webview_payment"
let SEGUE_TO_MAKE_PAYMENT           = "segue_make_payment"

let STATES      = [GenericField(id: 1, name: "AC"),
                   GenericField(id: 2, name: "AL"),
                   GenericField(id: 3, name: "AP"),
                   GenericField(id: 4, name: "AM"),
                   GenericField(id: 5, name: "BA"),
                   GenericField(id: 6, name: "CE"),
                   GenericField(id: 7, name: "DF"),
                   GenericField(id: 8, name: "ES"),
                   GenericField(id: 9, name: "GO"),
                   GenericField(id: 10, name: "MA"),
                   GenericField(id: 11, name: "MT"),
                   GenericField(id: 12, name: "MS"),
                   GenericField(id: 13, name: "MG"),
                   GenericField(id: 14, name: "PA"),
                   GenericField(id: 15, name: "PB"),
                   GenericField(id: 16, name: "PR"),
                   GenericField(id: 17, name: "PE"),
                   GenericField(id: 18, name: "PI"),
                   GenericField(id: 19, name: "RJ"),
                   GenericField(id: 20, name: "RN"),
                   GenericField(id: 21, name: "RS"),
                   GenericField(id: 22, name: "RO"),
                   GenericField(id: 23, name: "RR"),
                   GenericField(id: 24, name: "SC"),
                   GenericField(id: 25, name: "SP"),
                   GenericField(id: 26, name: "SE"),
                   GenericField(id: 27, name: "TO")]

let GENDER = [GenericField(id: 81, name: "Feminino"), GenericField(id: 82, name: "Masculino")]

let STATUS = [GenericField(id: -1, name: "Todos"), GenericField(id: 0, name: "Pendentes"), GenericField(id: 1, name: "Pagos")]

let ID_ACCEPT_RACE = 84
let IDREFUSE     = 83
