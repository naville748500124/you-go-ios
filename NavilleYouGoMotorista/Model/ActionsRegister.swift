//
//  ActionsRegister.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 02/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

extension RegisterViewController: retornoJsonDelegate{
    
    internal func findZipCode(){
        
        if (self.textFieldZipCode.text?.count)! < 9 {
            avisoToast("O cep precisa ter 8 números.", posicao: 3, altura: 45, tipo: 3)
            
        }else{
            
            let zipCode = self.textFieldZipCode.text!.replacingOccurrences(of: "-", with: "")
            self.functionController = 1
            
            let ws = WebService(
                funcao: "buscar_cep?cep=\(zipCode)",
                campos:"",
                authUsuario:"",
                authSenha:"",
                tipo:"GET",
                ativarIndicator: true,
                token: ""
            );
            
            ws.setUrlServidor(urlServidor: self.shared.serverShared)
            ws.conectar()
            ws.retornoDelegate = self
        }
        
       
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        
        print(json)
        if self.functionController == 1 {
        
            if let status = json["status"] as? Int{
                
                switch status {
                    
                case 1 :
                    if let eachInformation: NSDictionary = json["endereco"] as? NSDictionary{
                        
                        
                        if let neighborhood: String = eachInformation["bairro"] as? String{
                            self.textFieldNeighborhood.text = neighborhood
                        }
                        
                        if let zipCode: String = eachInformation["cep"] as? String{
                            self.textFieldZipCode.text = zipCode
                        }
                        
                        if let city: String = eachInformation["localidade"] as? String{
                            self.textFieldCity.text = city
                        }
                        
                        if let address: String = eachInformation["logradouro"] as? String{
                            self.textFieldAddress.text = address
                        }
                        
                        if let state: String = eachInformation["uf"] as? String{
                            self.textFieldState.text = state
                            
                            if STATES.contains(where: {$0.name == state}){
                                
                                for i in STATES{
                                    
                                    if i.name == state {
                                        self.personalData.setIdState(idState: i.id)
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                        if (textFieldNeighborhood.text?.isEmpty)! && (textFieldCity.text?.isEmpty)! && (textFieldAddress.text?.isEmpty)! {
                            let alert = UIAlertController(title: "Aviso", message: "Não foi possível encontrar endereço para este CEP.", preferredStyle: .alert)
                            let ok    = UIAlertAction(title: "Ok", style: .default , handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                    
                case 0:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                case -9:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
                case -10:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
                default:
                    avisoToast("Ocorreu um erro ao buscar o CEP", posicao: 3, altura: 45, tipo: 3)
                    break
                }
                
            }
            
        }
    }
    
}
