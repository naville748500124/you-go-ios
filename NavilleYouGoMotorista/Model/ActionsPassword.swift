//
//  ActionsPassword.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 27/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension PasswordViewController: retornoJsonDelegate{
    
    internal func requestChangePassword(){
        
        let ws = WebService(
            funcao: "editar_cadastro_motorista",
            campos: "senha_usuario=\(self.textFieldNewPassword.text!.sha1())&tipo=1",
            authUsuario: "",
            authSenha: "",
            tipo:"POST",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
        print(json)
        
        if let status = json["status"] as? Int{
            
            switch status{
                
            case 1://the request was a success
                self.shared.userPassword = (self.textFieldNewPassword.text?.sha1())!
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 1)
            case 0://the request have a problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
            case 6: //it have another app with this login
                avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
                delayWithSeconds(4, completion: {
                    self.dismiss(animated: true, completion: nil)
                })
            case -9://the webservice have a problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            case -10://internet problem
                avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
            default:
                break
                
            }
            
        }
        
    }
    
}
