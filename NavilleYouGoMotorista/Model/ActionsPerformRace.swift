//
//  ActionsPerformRace.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 14/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension PerformRaceViewController: retornoJsonDelegate{
    
    internal func requestDetails(){
        
        let ws = WebService(
            funcao:"listar_corridas",
            campos:"",
            authUsuario: "",
            authSenha: "",
            tipo:"GET",
            ativarIndicator: true,
            token: self.shared.userToken
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
       
         if let status = json["status"] as? Int{
        switch status {
        case 1://return webservice
            if let data = json["corridas"] as? NSArray{
                
                self.history = [DetailPerformRaceData]()
                for i in data {
                    
                    let uniqueHistory = DetailPerformRaceData()
                    if let eachInformation = i as? NSDictionary{
                        
                        if let car = eachInformation["carro"] as? String{
                            uniqueHistory.setCar(car: car)
                        }
                        
                        if let beginDate = eachInformation["data_corrida_inicio"] as? String{
                            uniqueHistory.setBeginDate(beginDate: beginDate)
                        }
                        
                        if let endDate = eachInformation["data_corrida_fim"] as? String{
                            uniqueHistory.setEndDate(endDate: endDate)
                        }
                        
                        if let originAddress = eachInformation["endereco_origem"] as? String{
                            uniqueHistory.setOriginAddress(originAddress: originAddress)
                        }
                        
                        if let destinationAddress = eachInformation["endereco_destino"] as? String{
                            uniqueHistory.setDestinationAddress(destinationAddress: destinationAddress)
                        }
                        
                        
                        if let idRace = (eachInformation["id_corrida"] as AnyObject).integerValue {
                            uniqueHistory.setIDRace(idRace: idRace)
                        }
                        
                        if let idCustomer = (eachInformation["id_usuario"] as AnyObject).integerValue {
                            uniqueHistory.setIDCustomer(idCustomer: idCustomer)
                        }
                        
                        if let distance = (eachInformation["km_corrida"] as AnyObject).doubleValue {
                            uniqueHistory.setDistance(distance: distance)
                        }
                        
                        if let justification = eachInformation["motivo_cancelamento"] as? String{
                            uniqueHistory.setJustification(justification: justification)
                        }
                        
                        if let nameCustomer = eachInformation["nome_usuario"] as? String{
                            uniqueHistory.setCustomerName(customerName: nameCustomer)
                        }
                        
                        if let licensePlate = eachInformation["placa_carro_motorista"] as? String{
                            uniqueHistory.setLicensePlate(licensePlate: licensePlate)
                        }
                        
                        if let statusRace = eachInformation["status_corrida"] as? String{
                            uniqueHistory.setStatus(status: statusRace)
                        }
                        
                        if let costRace = eachInformation["valor_corrida"] as? String{
                            uniqueHistory.setCost(cost: costRace)
                        }
                        
                        if let customerEvaluate = (eachInformation["passageiro_avaliou"] as AnyObject).integerValue{
                            uniqueHistory.setCustomerEvaluate(customerEvaluate: customerEvaluate)
                        }
                        
                        if let driverEvaluate = (eachInformation["motorista_avaliou"] as AnyObject).integerValue{
                            uniqueHistory.setDriverEvaluate(driverEvaluate: driverEvaluate)
                        }
                        
                        self.history.append(uniqueHistory)
                        
                    }
                    
                }
                
            }
            self.performRacesTable.reloadData()
            
        case 0://did not find nothing
            avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
        case 6: //it have another app with this login
            avisoToast("Você se logou em outro aparelho e essa sessão será finalizada", posicao: 3, altura: 45, tipo: 4)
            delayWithSeconds(4, completion: {
                self.dismiss(animated: true, completion: nil)
            })
        case -9://catch
            avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
        case -10://internet problem
            avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
        default:
            break
        }
            
        }
        
    }
    
}
