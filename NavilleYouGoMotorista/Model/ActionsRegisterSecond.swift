//
//  ActionsRegisterSecond.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 05/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

extension RegisterSecondScreenViewController: retornoJsonDelegate {
    
    
    //find pre register in webservice
    internal func preRegistrationDriver(){
        
        self.functionController = 1
        let ws = WebService(
            funcao:"pre_cadastro_motorista",
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    
    //find models of car in webservice
    internal func findModels(){
        
        self.functionController = 2
        let ws = WebService(
            funcao:"busca_modelo?id_montadora=\(self.vehicleData.getIdBrandCar())",
            campos:"",
            authUsuario:"",
            authSenha:"",
            tipo:"GET",
            ativarIndicator: true,
            token: ""
        );
        
        ws.setUrlServidor(urlServidor: self.shared.serverShared)
        ws.conectar()
        ws.retornoDelegate = self
        
    }
    
    func json(json: Dictionary<String, AnyObject>) {
     
            if let status = json["status"] as? Int {
                
                switch status {
                    
                case 1://will recovery cities and car brands to webservice
                    
                    switch self.functionController{
                        
                    case 1 :
                        //recovery cities data from webservice
                        if let cities = json["cidades"] as? NSArray{
                            
                            self.citiesArray = [GenericField]()
                            for i in cities {
                                
                                let eachInformation = i as! [String: AnyObject]
                                let id    = eachInformation["id"]?.integerValue
                                let name  = eachInformation["nome"] as! String
                                
                                self.citiesArray.append(GenericField(id: id!, name: name))
                            }
                            
                            if self.citiesArray.count > 0 {
                                self.textFieldCity.text = citiesArray.first?.name
                                self.vehicleData.setIdActive(idActiveCity: (citiesArray.first?.id)!)
                            }
                            
                        }
                        
                        //recovery brands data from webservice
                        if let brands = json["marcas"] as? NSArray{
                            
                            self.brandsArray = [GenericField]()
                            for i in brands{
                                
                                let eachInformation = i as! [String: AnyObject]
                                let id   = eachInformation["id"]?.integerValue
                                let name = eachInformation["nome"] as! String
                                
                                self.brandsArray.append(GenericField(id: id!, name: name))
                                
                            }
                            
                            if self.brandsArray.count > 0 {
                                self.textFieldVehicleBrand.text = brandsArray.first?.name
                                self.vehicleData.setIdBrandCar(idBrandCar: (brandsArray.first?.id)!)
                                findModels()
                            }
                        }
                        
                    case 2: // will recovery car models to webservice
                        
                        let models = json["modelos"] as! NSArray
                        
                        self.textFieldVehicleModel.isEnabled = true
                        self.modelsArray = [GenericField]()
                        for i in models {
                            
                            let eachInformation = i as! [String: AnyObject]
                            let id   = eachInformation["id_modelo"]?.integerValue
                            let name = eachInformation["modelo"] as! String
                            
                            self.modelsArray.append(GenericField(id: id!, name: name))
                            
                        }
                        
                        if self.modelsArray.count > 0 {
                            
                            self.textFieldVehicleModel.text = modelsArray.first?.name
                            self.vehicleData.setIdModelCar(idModelCar: (modelsArray.first?.id)!)
                            
                        }
                        
                    default:
                        break
                        
                    }
                    
                case -9: //error in webservice
                    avisoToast(json["resultado"] as! String , posicao: 3, altura: 45, tipo: 4)
                    
                case -10:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 4)
                    
                case 0:
                    avisoToast(json["resultado"] as! String, posicao: 3, altura: 45, tipo: 3)
                    
                default:
                    break
                    
                }
                
                
                
            }
            
        
        
    }
    
}
