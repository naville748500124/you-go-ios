//
//  Structs.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 27/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

struct MenuData {
    var imageMenu: UIImage
    var nameMenu:   String
}

struct GenericField {
    var id:     Int
    var name:   String
}

