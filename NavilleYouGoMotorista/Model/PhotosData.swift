//
//  PhotosData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 05/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

class PhotosData {
    
    private var driverPhoto:        UIImage
    private var cnhPhoto:           UIImage
    private var insurancePhoto:     UIImage
    private var documentPhoto:      UIImage
    private var vehicleOnePhoto:    UIImage
    private var vehicleTwoPhoto:    UIImage
    private var vehicleThreePhoto:  UIImage
    private var residencePhoto:     UIImage
    private var rgPhoto:            UIImage
    private var criminalPhoto:      UIImage?
    
    private var driverBase64:         String
    private var cnhBase64:            String
    private var documentBase64:       String
    private var insuranceBase64:      String
    private var vehicleOneBase64:     String
    private var vehicleTwoBase64:     String
    private var vehicleThreeBase64:   String
    private var residenceBase64:      String
    private var rgBase64:             String
    private var criminalBase64:       String?
    
    init(driverPhoto: UIImage, cnhPhoto: UIImage, insurancePhoto: UIImage, documentPhoto: UIImage, vehicleOnePhoto: UIImage, vehicleTwoPhoto: UIImage, vehicleThreePhoto: UIImage, residencePhoto: UIImage,rgPhoto: UIImage, criminalPhoto: UIImage?, driver: String, cnh: String, insurance: String, vehicleOne: String, vehicleTwo: String, vehicleThree: String, residence: String, document: String, rgBase64: String, criminalBase64: String?) {
        
        self.driverPhoto        = driverPhoto
        self.cnhPhoto           = cnhPhoto
        self.insurancePhoto     = insurancePhoto
        self.documentPhoto      = documentPhoto
        self.vehicleOnePhoto    = vehicleOnePhoto
        self.vehicleTwoPhoto    = vehicleTwoPhoto
        self.vehicleThreePhoto  = vehicleThreePhoto
        self.residencePhoto     = residencePhoto
        self.rgPhoto            = rgPhoto
        self.criminalPhoto      = criminalPhoto
        
        self.driverBase64             = driver
        self.cnhBase64                = cnh
        self.insuranceBase64          = insurance
        self.documentBase64           = document
        self.vehicleOneBase64         = vehicleOne
        self.vehicleTwoBase64         = vehicleTwo
        self.vehicleThreeBase64       = vehicleThree
        self.residenceBase64          = residence
        self.rgBase64                 = rgBase64
        self.criminalBase64           = criminalBase64
        
    }
    
    init() {
        
        self.driverPhoto        = UIImage()
        self.cnhPhoto           = UIImage()
        self.insurancePhoto     = UIImage()
        self.documentPhoto      = UIImage()
        self.vehicleOnePhoto    = UIImage()
        self.vehicleTwoPhoto    = UIImage()
        self.vehicleThreePhoto  = UIImage()
        self.residencePhoto     = UIImage()
        self.rgPhoto            = UIImage()
        self.criminalPhoto      = UIImage()
        
        self.driverBase64             = ""
        self.cnhBase64                = ""
        self.insuranceBase64          = ""
        self.documentBase64           = ""
        self.vehicleOneBase64         = ""
        self.vehicleTwoBase64         = ""
        self.vehicleThreeBase64       = ""
        self.residenceBase64          = ""
        self.rgBase64                 = ""
        self.criminalBase64           = ""
    }
    
    //driverPhoto
    public func getDriverPhoto() -> UIImage{
        return self.driverPhoto
    }
    
    public func setDriverPhoto(driverPhoto: UIImage){
        self.driverPhoto = driverPhoto
        self.driverBase64 = convertBase64(photo: getDriverPhoto())
    }
    
    //cnhPhoto
    public func getCNHPhoto() -> UIImage{
        return self.cnhPhoto
    }
    
    public func setCNHPhoto(cnhPhoto: UIImage){
        self.cnhPhoto = cnhPhoto
        self.cnhBase64 = convertBase64(photo: getCNHPhoto())
    }
    
    //insurancePhoto
    public func getInsurancePhoto() -> UIImage{
        return self.insurancePhoto
    }
    
    public func setInsurancePhoto(insurancePhoto: UIImage){
        self.insurancePhoto = insurancePhoto
        self.insuranceBase64 = convertBase64(photo: getInsurancePhoto())
    }
    
    //documentPhoto
    public func getDocumentPhoto() -> UIImage{
        return self.documentPhoto
    }
    
    public func setDocumentPhoto(documentPhoto: UIImage){
        self.documentPhoto = documentPhoto
        self.documentBase64 = convertBase64(photo: getDocumentPhoto())
    }
    
    //vehicleOnePhoto
    public func getVehicleOnePhoto() -> UIImage{
        return self.vehicleOnePhoto
    }
    
    public func setVehicleOnePhoto(vehicleOnePhoto: UIImage){
        self.vehicleOnePhoto = vehicleOnePhoto
        self.vehicleOneBase64 = convertBase64(photo: getVehicleOnePhoto())
    }
    
    //vehicleTwoPhoto
    public func getVehicleTwoPhoto() -> UIImage{
        return self.vehicleTwoPhoto
    }
    
    public func setVehicleTwoPhoto(vehicleTwoPhoto: UIImage){
        self.vehicleTwoPhoto = vehicleTwoPhoto
        self.vehicleTwoBase64 = convertBase64(photo: getVehicleTwoPhoto())
    }
    
    //vehicleThreePhoto
    public func getVehicleThreePhoto() -> UIImage{
        return self.vehicleThreePhoto
    }
    
    public func setVehicleThreePhoto(vehicleThreePhoto: UIImage){
        self.vehicleThreePhoto = vehicleThreePhoto
        self.vehicleThreeBase64 = convertBase64(photo: getVehicleThreePhoto())
    }
    
    //residencePhoto
    public func getResidencePhoto() -> UIImage{
        return self.residencePhoto
    }
    
    public func setResidencePhoto(residencePhoto: UIImage){
        self.residencePhoto = residencePhoto
        self.residenceBase64 = convertBase64(photo: getResidencePhoto())
    }
    
    //rgPhoto
    public func getRGPhoto() -> UIImage{
        return self.rgPhoto
    }
    
    public func setRGPhoto(rgPhoto: UIImage){
        self.rgPhoto = rgPhoto
        self.rgBase64 = convertBase64(photo: getRGPhoto())
    }
    
    //rgPhoto
    public func getCriminalPhoto() -> UIImage?{
        return self.criminalPhoto
    }
    
    public func setCriminalPhoto(criminalPhoto: UIImage?){
        if let criminalPhoto = criminalPhoto{
            self.criminalPhoto = criminalPhoto
            self.criminalBase64 = convertBase64(photo: getCriminalPhoto()!)
        }
    }
    
    
    //this function will convert uiimage in base64
    func convertBase64(photo: UIImage) -> String {
        
        var imageData: NSData
        imageData = photo.qualidadeMinima
        let photoConverted: String = imageData.base64EncodedString(options: .lineLength64Characters).replacingOccurrences(of: "\r\n", with: "")
        return photoConverted
        
    }
    
    //getters of base64
    public func getDriverBase64() -> String{
        return self.driverBase64
    }
    
    public func getCNHBase64() -> String{
        return self.cnhBase64
    }
    
    public func getInsuranceBase64() -> String{
        return self.insuranceBase64
    }
    
    public func getVehicleOneBase64() -> String{
        return self.vehicleOneBase64
    }
    public func getVehicleTwoBase64() -> String{
        return self.vehicleTwoBase64
    }
    public func getVehicleThreeBase64() -> String{
        return self.vehicleThreeBase64
    }
    
    public func getDocumentBase64() -> String{
        return self.documentBase64
    }
    
    public func getResidenceBase64() -> String{
        return self.residenceBase64
    }
    
    public func getRGBase64() -> String{
        return self.rgBase64
    }
    
    public func getCriminalBase64() -> String?{
        return self.criminalBase64
    }
}
