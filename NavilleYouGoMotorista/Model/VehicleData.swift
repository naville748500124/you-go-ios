//
//  VehicleData.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 05/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation

class VehicleData{
    
    private var idActiveCity:    Int
    private var idModelCar:      Int
    private var carColor:        String
    private var carLicensePlate: String
    private var idBrandCar:      Int
    
    
    init(idActiveCity: Int, idModelCar: Int, carColor: String, carLicensePlate: String, idBrandCar: Int) {
        self.idActiveCity       = idActiveCity
        self.idModelCar         = idModelCar
        self.carColor           = carColor
        self.carLicensePlate    = carLicensePlate
        self.idBrandCar         = idBrandCar
    }
    
    init() {
        self.idActiveCity       = 0
        self.idModelCar         = 0
        self.carColor           = ""
        self.carLicensePlate    = ""
        self.idBrandCar         = 0
    }
    
    //idActiveCity
    public func getIdActive() -> Int{
        return self.idActiveCity
    }
    
    public func setIdActive(idActiveCity: Int){
        self.idActiveCity = idActiveCity
    }
    
    //idModelCar
    public func getIdModelCar() -> Int{
        return self.idModelCar
    }
    
    public func setIdModelCar(idModelCar: Int){
        self.idModelCar = idModelCar
    }
    
    //carColor
    public func getCarColor() -> String{
        return self.carColor
    }
    
    public func setCarColor(carColor: String){
        self.carColor = carColor
    }
    
    //carLicensePlate
    public func getCarLicensePlate() -> String{
        return self.carLicensePlate
    }
    
    public func setCarLicensePlate(carLicensePlate: String){
        self.carLicensePlate = carLicensePlate
    }
    
    //idBrandCar
    public func getIdBrandCar() -> Int{
        return self.idBrandCar
    }
    
    public func setIdBrandCar(idBrandCar: Int){
        self.idBrandCar = idBrandCar
    }
}
