//
//  AppDelegate.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

//    //variables with server
//    let serverShared                = "http://devnaville-br2.16mb.com/yougo/controller_webservice/"
//    let serverImageShared           = "http://devnaville-br2.16mb.com/yougo/upload/motoristas/"
//    let serverCustomerImageShared   = "http://devnaville-br2.16mb.com/yougo/upload/passageiros/"
//    let serverDetailCostRace        = "http://devnaville-br2.16mb.com/yougo/controller_webservice/comprovante_corrida?fk_corrida="
//    var faqServer                   =  "http://devnaville-br2.16mb.com/yougo/faq_motorista.php"
//    let termsServer                 = "http://devnaville-br2.16mb.com/yougo/controller_termo/termos_de_uso"
//    let paymentURL                  = "http://devnaville-br2.16mb.com/yougo/controller_pagamento/assinatura?fk_corrida="
    
//    //variables with server
    let serverShared                = "http://31.220.63.171/yougo/controller_webservice/"
    let serverImageShared           = "http://31.220.63.171/yougo/upload/motoristas/"
    let serverCustomerImageShared   = "http://31.220.63.171/yougo/upload/passageiros/"
    let serverDetailCostRace        = "http://31.220.63.171/yougo/controller_webservice/comprovante_corrida?fk_corrida="
    var faqServer                   = "http://31.220.63.171/yougo/controller_adm/faq_motorista"
    let termsServer                 = "http://31.220.63.171/yougo/controller_termo/termos_de_uso_motorista"
    let paymentURL                  = "http://31.220.63.171/yougo/controller_pagamento/assinatura?fk_corrida="
    let privacyPolicy               = "http://31.220.63.171/yougo/controller_termo/politica_de_privacidade"
    
    //session datas
    var userID          = ""
    var userEmail       = ""
    var userName        = ""
    var userPassword    = ""
    var userToken       = ""
    var bankID          = ""
    var agencyDriver    = ""
    var accountDriver   = ""
    var digitAccount    = ""
    var lastRace        = ""
    var tipoConta       = 0
    
    var emailCompany    = "contato@yougomobile.com.br"
    var phoneCompany    = "14996060447"
    var addressCompany  = "Rua Gustavo Maciel, 22-85 - Jardim Narsalla, Bauru - 17012110 - SP"
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       
        
        //IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.shared.enable = true
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        registerForPushNotifications(application: application)
        FirebaseApp.configure()
        
        return true
    }
    
    //facebook
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let handle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        return handle
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

  

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    /******************************/
    //Notificações
    /******************************/
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // ...register device token with our Time Entry API server via REST
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("DidFaildRegistration : Device token for push notifications: FAIL -- ")
        //print(error.localizedDescription)
    }
    

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        // Change this to your preferred presentation option
        completionHandler([.alert, .sound])
        print("sei la eu")
    }
    
    func registerForPushNotifications(application: UIApplication) {
        
        let som = UNMutableNotificationContent()
        som.sound = UNNotificationSound(named: "Pop up.m4r")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            
            
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
            }
            
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        print("%@", userInfo)
        UIApplication.shared.applicationIconBadgeNumber = 1
        
//        print("badgets = \(UIApplication.shared.applicationIconBadgeNumber)")
//
//        if UserDefaults.standard.integer(forKey: "badget") == nil {
//            UserDefaults.standard.set(0, forKey: "badget")
//        }else{
//            UIApplication.shared.applicationIconBadgeNumber = UserDefaults.standard.integer(forKey: "badget")
//        }
//
//
//        UIApplication.shared.applicationIconBadgeNumber = 0
//        UserDefaults.standard.set(UIApplication.shared.applicationIconBadgeNumber, forKey: "badget")
//
//        print("badgets = \(UIApplication.shared.applicationIconBadgeNumber)")
//
    }
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data
        print("chegou aqui no register notification")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    /******************************/
    //Notificações - FIM
    /******************************/

}

