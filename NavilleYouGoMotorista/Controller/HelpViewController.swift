//
//  HelpViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import MessageUI

class HelpViewController: UIViewController, MFMailComposeViewControllerDelegate {

    //Elements of screen
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelCompanyData: UILabel!
    
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.labelVersion.text = "Versão \(version)"
        }
        
        self.labelCompanyData.text = shared.addressCompany
        self.labelCompanyData.textAlignment = .center
        
    }
    
    override var prefersStatusBarHidden: Bool{
        return false
    }

    //buttons actions
    @IBAction func callButton(_ sender: Any) {
       self.makeCall()
    }
    
    @IBAction func sendEmailButton(_ sender: Any) {
        sendEmail()
    }
    
    @IBAction func faqButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_FAQ, sender: nil)
    }
    
    @IBAction func termosServiceTapped(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_USED_TERMS, sender: nil)
    }
    
    @IBAction func privacyPressed(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_PRIVACY_POLICY, sender: nil)
    }
    
    //functions
    func makeCall()  {
        
        let url: NSURL = URL(string: "TEL://\(self.shared.phoneCompany)")! as NSURL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func sendEmail() {
        
        let recipientes = [shared.emailCompany]
        let mailComposer = MFMailComposeViewController()
        
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Ajuda!")
        mailComposer.setToRecipients(recipientes)
        present(mailComposer, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
            
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelado")
            
        case MFMailComposeResult.failed.rawValue:
            print("Falhou")
            
        case MFMailComposeResult.saved.rawValue:
            print("Salvo")
            
        case MFMailComposeResult.sent.rawValue:
            print("Enviado")
            
        default:
            break
        }
        
        controller.dismiss(animated: true, completion: nil)
        
    }

}
