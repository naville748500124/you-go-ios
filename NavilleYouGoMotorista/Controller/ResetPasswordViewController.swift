//
//  ResetPasswordViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var textFieldEmail: TextFieldCustom!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //buttons actions
    @IBAction func recoveredButtonTapped(_ sender: Any) {
        
        if (textFieldEmail.text?.isEmpty)!{
            
            avisoToast("O \"E-mail\", está vazio.", posicao: 3, altura: 45, tipo: 4)
            
        }else{
            
            if (self.textFieldEmail.text?.isValidoEmail())!{
                
                self.resetPassword()
                
            }else{
                avisoToast("O \"E-mail\", não é válido.", posicao: 3, altura: 45, tipo: 4)
            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

}
