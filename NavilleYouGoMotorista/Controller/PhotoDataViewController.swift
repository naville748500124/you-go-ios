//
//  PhotoDataViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class PhotoDataViewController: UIViewController{
    
    
    //Elements of screen
    //Constraints
    @IBOutlet weak var viewHeightPersonalPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightCNHPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightInsurancePhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightVehiclePhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightVehicleDocumentPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightResidencePhoto: NSLayoutConstraint!
    
    //views
    @IBOutlet weak var viewPersonal: UIView!
    @IBOutlet weak var viewCNH: UIView!
    @IBOutlet weak var viewInsurance: UIView!
    @IBOutlet weak var viewVehicle: UIView!
    @IBOutlet weak var viewVehicleDocument: UIView!
    @IBOutlet weak var viewResidence: UIView!
    
    //images
    @IBOutlet weak var personalImage: UIImageView!
    @IBOutlet weak var cnhImage: UIImageView!
    @IBOutlet weak var insuranceImage: UIImageView!
    @IBOutlet weak var vehicleImageOne: UIImageView!
    @IBOutlet weak var vehicleImageTwo: UIImageView!
    @IBOutlet weak var vehicleImageThree: UIImageView!
    @IBOutlet weak var vehicleDocumentImage: UIImageView!
    @IBOutlet weak var residenceImage: UIImageView!
    @IBOutlet weak var rgImage: ImageCustom!
    @IBOutlet weak var criminalImage: ImageCustom!
    
    
    //Variables and constants
    //booleans variables
    var booleanPersonalPhotos           = false
    var booleanCNHPhotos                = false
    var booleanInsurancePhotos          = false
    var booleanVehiclePhotos            = false
    var booleanDocumentVehiclePhotos    = false
    var booleanResidencePhotos          = false
    var imagePicker = UIImagePickerController()
    var imageTag = 0
    var shared = UIApplication.shared.delegate as! AppDelegate
    var photoData   = PhotosData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //configuration delegete of imagePicker
        self.imagePicker.delegate = self
        
        //configurations constrains
        self.viewHeightPersonalPhoto.constant = 0
        self.viewHeightCNHPhoto.constant = 0
        self.viewHeightInsurancePhoto.constant = 0
        self.viewHeightVehiclePhoto.constant = 0
        self.viewHeightVehicleDocumentPhoto.constant = 0
        self.viewHeightResidencePhoto.constant = 0
        
        //customize views photos
        self.viewPersonal.layer.cornerRadius = 16
        self.viewCNH.layer.cornerRadius = 16
        self.viewInsurance.layer.cornerRadius = 16
        self.viewVehicleDocument.layer.cornerRadius = 16
        self.viewResidence.layer.cornerRadius = 16
        self.viewVehicle.layer.cornerRadius = 16
        
        self.viewPersonal.clipsToBounds = true
        self.viewCNH.clipsToBounds = true
        self.viewInsurance.clipsToBounds = true
        self.viewVehicleDocument.clipsToBounds = true
        self.viewResidence.clipsToBounds = true
        self.viewVehicle.clipsToBounds = true
        
        //configurations of uiimages
        self.personalImage.isUserInteractionEnabled         = true
        self.cnhImage.isUserInteractionEnabled              = true
        self.insuranceImage.isUserInteractionEnabled        = true
        self.vehicleImageOne.isUserInteractionEnabled       = true
        self.vehicleImageTwo.isUserInteractionEnabled       = true
        self.vehicleImageThree.isUserInteractionEnabled     = true
        self.vehicleDocumentImage.isUserInteractionEnabled  = true
        self.residenceImage.isUserInteractionEnabled        = true
        self.rgImage.isUserInteractionEnabled               = true
        self.criminalImage.isUserInteractionEnabled         = true
        
        self.personalImage.tag = 1
        self.cnhImage.tag = 2
        self.insuranceImage.tag = 3
        self.vehicleImageOne.tag = 4
        self.vehicleImageTwo.tag = 5
        self.vehicleImageThree.tag = 6
        self.vehicleDocumentImage.tag = 7
        self.residenceImage.tag = 8
        self.rgImage.tag = 9
        self.criminalImage.tag = 10
        
        
        //tap gesture, to open camera
        let tapPersona        = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapCNH            = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapInsurance      = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleOne     = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleTwo     = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleThree   = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapDocument       = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapResidence      = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapRG             = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapCriminal       = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        
        tapPersona.numberOfTapsRequired         = 1
        tapCNH.numberOfTapsRequired             = 1
        tapInsurance.numberOfTapsRequired       = 1
        tapVehicleOne.numberOfTapsRequired      = 1
        tapVehicleTwo.numberOfTapsRequired      = 1
        tapVehicleThree.numberOfTapsRequired    = 1
        tapDocument.numberOfTapsRequired        = 1
        tapResidence.numberOfTapsRequired       = 1
        tapRG.numberOfTapsRequired              = 1
        tapCriminal.numberOfTapsRequired        = 1
        
        self.personalImage.addGestureRecognizer(tapPersona)
        self.cnhImage.addGestureRecognizer(tapCNH)
        self.insuranceImage.addGestureRecognizer(tapInsurance)
        self.vehicleImageOne.addGestureRecognizer(tapVehicleOne)
        self.vehicleImageTwo.addGestureRecognizer(tapVehicleTwo)
        self.vehicleImageThree.addGestureRecognizer(tapVehicleThree)
        self.vehicleDocumentImage.addGestureRecognizer(tapDocument)
        self.residenceImage.addGestureRecognizer(tapResidence)
        self.rgImage.addGestureRecognizer(tapRG)
        self.criminalImage.addGestureRecognizer(tapCriminal)
        
        
        //recovered images
        self.recoveredImages()
        
    }
    
    
    //Button action
    @IBAction func personalPhotoTapped(_ sender: Any) {
        openView(validate: booleanPersonalPhotos, constraint: viewHeightPersonalPhoto, number: 150, time: 0.3)
        self.booleanPersonalPhotos = !booleanPersonalPhotos
    }
    
    @IBAction func cnhPhotoTapped(_ sender: Any) {
        openView(validate: booleanCNHPhotos, constraint: viewHeightCNHPhoto, number: 150, time: 0.3)
        self.booleanCNHPhotos = !booleanCNHPhotos
    }
    
    @IBAction func insurancePhotoTapped(_ sender: Any) {
        openView(validate: booleanInsurancePhotos, constraint: viewHeightInsurancePhoto, number: 150, time: 0.3)
        self.booleanInsurancePhotos = !booleanInsurancePhotos
    }
    
    @IBAction func vehiclePhotoTapped(_ sender: Any) {
        openView(validate: booleanVehiclePhotos, constraint: viewHeightVehiclePhoto, number: 150, time: 0.3)
        self.booleanVehiclePhotos = !booleanVehiclePhotos
    }
    
    @IBAction func vehicleDocumentPhotoTapped(_ sender: Any) {
        openView(validate: booleanDocumentVehiclePhotos, constraint: viewHeightVehicleDocumentPhoto, number: 150, time: 0.3)
        self.booleanDocumentVehiclePhotos = !booleanDocumentVehiclePhotos
    }
    
    @IBAction func residencePhotoTapped(_ sender: Any) {
        openView(validate: booleanResidencePhotos, constraint: viewHeightResidencePhoto, number: 150, time: 0.3)
        self.booleanResidencePhotos = !booleanResidencePhotos
    }
    
    
    @IBAction func updateButtonTapped(_ sender: Any) {
       
            let alert = UIAlertController(title: "Aviso", message: "Caso altere alguma foto, o seu acesso será bloqueado até uma nova aprovação do administrador.\nVocê tem certeza que deseja atualizar as fotos?", preferredStyle: .alert)
            let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
            let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
                
                self.updataPhotos()
            }
            alert.addAction(no)
            alert.addAction(yes)
            self.present(alert, animated: true, completion: nil)
            
        
    }
    
    //actions
    internal func checkImages() -> String{
        
        if self.photoData.getDriverBase64() == "" {
            return "Pessoa"
        }else if self.photoData.getCNHBase64() == ""{
            return "CNH"
        }else if self.photoData.getInsuranceBase64() == ""{
            return "Seguro"
        }else if self.photoData.getVehicleOneBase64() == ""{
            return "Primeira foto do veículo"
        }else if self.photoData.getVehicleTwoBase64() == ""{
            return "Segunda foto do veículo"
        }else if self.photoData.getVehicleThreeBase64() == ""{
            return "Terceira foto do veículo"
        }else if self.photoData.getDocumentBase64() == ""{
            return "Documento do veículo"
        }else if self.photoData.getResidenceBase64() == ""{
            return "Comprovante de residencia"
        }else if self.photoData.getRGBase64() == ""{
            return "RG"
        }else if self.photoData.getCriminalBase64() == ""{
            return "Antecedentes criminais"
        }else{
            return ""
        }
        
    }
    
    func recoveredImages(){
        
        if let idUser: String = self.shared.userID{
            print(self.shared.userID)
            
            let urlDriver       = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/motorista.png")
            let urlCNH          = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/cnh.png")
            let urlInsurance    = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/seguro.png")
            let urlDocument     = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/documento.png")
            let urlVehicleOne   = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/veiculo_1.png")
            let urlVehicleTwo   = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/veiculo_2.png")
            let urlVehicleThree = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/veiculo_3.png")
            let urlResidence    = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/comprovante.png")
            let urlRG           = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/rg.png")
            let urlCriminal     = URL(string: self.shared.serverImageShared + "motorista_" + idUser + "/antecedentes.png")
            
            
            DispatchQueue.global().async {
                
                if urlDriver != nil {
                    
                    let data = try? Data(contentsOf: urlDriver!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.personalImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlCNH != nil {
                    
                    let data = try? Data(contentsOf: urlCNH!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.cnhImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlInsurance != nil {
                    
                    let data = try? Data(contentsOf: urlInsurance!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.insuranceImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlDocument != nil {
                    
                    let data = try? Data(contentsOf: urlDocument!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.vehicleDocumentImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlVehicleOne != nil {
                    
                    let data = try? Data(contentsOf: urlVehicleOne!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.vehicleImageOne.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlVehicleTwo != nil {
                    
                    let data = try? Data(contentsOf: urlVehicleTwo!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.vehicleImageTwo.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlVehicleThree != nil {
                    
                    let data = try? Data(contentsOf: urlVehicleThree!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.vehicleImageThree.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlResidence != nil {
                    
                    let data = try? Data(contentsOf: urlResidence!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.residenceImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlRG != nil {
                    
                    let data = try? Data(contentsOf: urlRG!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.rgImage.image = UIImage(data: data!)
                    }
                    
                }
                
                if urlCriminal != nil {
                    
                    let data = try? Data(contentsOf: urlCriminal!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.criminalImage.image = UIImage(data: data!)
                    }
                    
                }
                
            }
            
        }
        
    }
    
    internal func postField() -> String{
        
        let post = "motorista=\(self.photoData.getDriverBase64())&cnh=\(self.photoData.getCNHBase64())&seguro=\(self.photoData.getInsuranceBase64())&documento=\(self.photoData.getDocumentBase64())&veiculo_1=\(self.photoData.getVehicleOneBase64())&veiculo_2=\(self.photoData.getVehicleTwoBase64())&veiculo_3=\(self.photoData.getVehicleThreeBase64())&comprovante=\(self.photoData.getResidenceBase64())&rg=\(self.photoData.getRGBase64())&antecedentes=\(self.photoData.getCriminalBase64()!)"
        
        return post
    }
   
}


//function to open gallery or open the camera
extension PhotoDataViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func openCameraOrGallery(photo: UITapGestureRecognizer){
        
        let image = photo.view!
        self.imageTag = image.tag
        
        let alert = UIAlertController(title: "Aviso", message: "Escolha de onde deseja obter a imagem.", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Câmera", style: .default) { (action) in
            //MARK: TODO: fazer funcioanr abrir a camera
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType    = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
            
        }
        
        let library = UIAlertAction(title: "Biblioteca", style: .default) { (action) in
            //MARK: TODO: fazer funcioanr a biblioteca
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType    = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
        }
        
        let cancel = UIAlertAction(title: "Cencelar", style: .cancel, handler: nil)
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            chooseUIImageView(tag: self.imageTag, image: image)
        }
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            chooseUIImageView(tag: self.imageTag, image: image)
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    internal func chooseUIImageView(tag: Int, image: UIImage){
        
        switch tag {
        case 1:
            self.personalImage.image = image
            self.photoData.setDriverPhoto(driverPhoto: image)
        case 2:
            self.cnhImage.image = image
            self.photoData.setCNHPhoto(cnhPhoto: image)
        case 3:
            self.insuranceImage.image = image
            self.photoData.setInsurancePhoto(insurancePhoto: image)
        case 4:
            self.vehicleImageOne.image = image
            self.photoData.setVehicleOnePhoto(vehicleOnePhoto: image)
        case 5:
            self.vehicleImageTwo.image = image
            self.photoData.setVehicleTwoPhoto(vehicleTwoPhoto: image)
        case 6:
            self.vehicleImageThree.image = image
            self.photoData.setVehicleThreePhoto(vehicleThreePhoto: image)
        case 7:
            self.vehicleDocumentImage.image = image
            self.photoData.setDocumentPhoto(documentPhoto: image)
        case 8:
            self.residenceImage.image = image
            self.photoData.setResidencePhoto(residencePhoto: image)
        case 9:
            self.rgImage.image = image
            self.photoData.setRGPhoto(rgPhoto: image)
        case 10:
            self.criminalImage.image = image
            self.photoData.setCriminalPhoto(criminalPhoto: image)
        default:
            break
        }
        
    }
    
}

