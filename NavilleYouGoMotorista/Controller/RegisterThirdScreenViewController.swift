//
//  RegisterThirdScreenViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class RegisterThirdScreenViewController: UIViewController{
    
    
    //Elements of screen
    //Constraints
    @IBOutlet weak var viewHeightPersonalPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightCNHPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightInsurancePhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightVehiclePhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightVehicleDocumentPhoto: NSLayoutConstraint!
    @IBOutlet weak var viewHeightResidencePhoto: NSLayoutConstraint!
    
    //views
    @IBOutlet weak var viewPersonal: UIView!
    @IBOutlet weak var viewCNH: UIView!
    @IBOutlet weak var viewInsurance: UIView!
    @IBOutlet weak var viewVehicle: UIView!
    @IBOutlet weak var viewVehicleDocument: UIView!
    @IBOutlet weak var viewResidence: UIView!
    
    //images
    @IBOutlet weak var personalImage: UIImageView!
    @IBOutlet weak var cnhImage: UIImageView!
    @IBOutlet weak var insuranceImage: UIImageView!
    @IBOutlet weak var vehicleImageOne: UIImageView!
    @IBOutlet weak var vehicleImageTwo: UIImageView!
    @IBOutlet weak var vehicleImageThree: UIImageView!
    @IBOutlet weak var vehicleDocumentImage: UIImageView!
    @IBOutlet weak var residenceImage: UIImageView!
    @IBOutlet weak var rgImage: ImageCustom!
    @IBOutlet weak var criminalImage: ImageCustom!
    
    
    //Variables and constants
    //booleans variables
    var booleanPersonalPhotos           = false
    var booleanCNHPhotos                = false
    var booleanInsurancePhotos          = false
    var booleanVehiclePhotos            = false
    var booleanDocumentVehiclePhotos    = false
    var booleanResidencePhotos          = false
    var imagePicker = UIImagePickerController()
    var imageTag = 0
    var shared = UIApplication.shared.delegate as! AppDelegate
    var personalData = PersonalData()
    var vehicleData = VehicleData()
    var photoData   = PhotosData()
    var sendSMSCommonLogin = 0
    var delegateSMS: ActivateSMSDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //recovery facebook image
        if let facebookImage = self.personalData.getFacebookImage(){
            
            let url = URL(string: facebookImage)
            DispatchQueue.global().async {
               
                if url != nil {
                    
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.personalImage.image = UIImage(data: data!)
                        self.photoData.setDriverPhoto(driverPhoto: UIImage(data: data!)!)
                    }
                    
                }else{
                    
                    self.delegateSMS = delegateSMSGlobal
                    
                }
                
            }
            
        }

        //configuration delegete of imagePicker
        self.imagePicker.delegate = self
        
        //configurations constrains
        self.viewHeightPersonalPhoto.constant = 0
        self.viewHeightCNHPhoto.constant = 0
        self.viewHeightInsurancePhoto.constant = 0
        self.viewHeightVehiclePhoto.constant = 0
        self.viewHeightVehicleDocumentPhoto.constant = 0
        self.viewHeightResidencePhoto.constant = 0
        
        //customize views photos
        self.viewPersonal.layer.cornerRadius = 16
        self.viewCNH.layer.cornerRadius = 16
        self.viewInsurance.layer.cornerRadius = 16
        self.viewVehicleDocument.layer.cornerRadius = 16
        self.viewResidence.layer.cornerRadius = 16
        self.viewVehicle.layer.cornerRadius = 16
        
        self.viewPersonal.clipsToBounds = true
        self.viewCNH.clipsToBounds = true
        self.viewInsurance.clipsToBounds = true
        self.viewVehicleDocument.clipsToBounds = true
        self.viewResidence.clipsToBounds = true
        self.viewVehicle.clipsToBounds = true
        
        //configurations of uiimages
        self.personalImage.isUserInteractionEnabled         = true
        self.cnhImage.isUserInteractionEnabled              = true
        self.insuranceImage.isUserInteractionEnabled        = true
        self.vehicleImageOne.isUserInteractionEnabled       = true
        self.vehicleImageTwo.isUserInteractionEnabled       = true
        self.vehicleImageThree.isUserInteractionEnabled     = true
        self.vehicleDocumentImage.isUserInteractionEnabled  = true
        self.residenceImage.isUserInteractionEnabled        = true
        self.rgImage.isUserInteractionEnabled               = true
        self.criminalImage.isUserInteractionEnabled         = true
        
        self.personalImage.tag = 1
        self.cnhImage.tag = 2
        self.insuranceImage.tag = 3
        self.vehicleImageOne.tag = 4
        self.vehicleImageTwo.tag = 5
        self.vehicleImageThree.tag = 6
        self.vehicleDocumentImage.tag = 7
        self.residenceImage.tag = 8
        self.rgImage.tag = 9
        self.criminalImage.tag = 10
        
        
        //tap gesture, to open camera
        let tapPersona        = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapCNH            = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapInsurance      = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleOne     = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleTwo     = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapVehicleThree   = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapDocument       = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapResidence      = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapRG             = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        let tapCriminal       = UITapGestureRecognizer(target: self, action: #selector(openCameraOrGallery(photo:)))
        
        tapPersona.numberOfTapsRequired         = 1
        tapCNH.numberOfTapsRequired             = 1
        tapInsurance.numberOfTapsRequired       = 1
        tapVehicleOne.numberOfTapsRequired      = 1
        tapVehicleTwo.numberOfTapsRequired      = 1
        tapVehicleThree.numberOfTapsRequired    = 1
        tapDocument.numberOfTapsRequired        = 1
        tapResidence.numberOfTapsRequired       = 1
        tapRG.numberOfTapsRequired              = 1
        tapCriminal.numberOfTapsRequired        = 1
        
        self.personalImage.addGestureRecognizer(tapPersona)
        self.cnhImage.addGestureRecognizer(tapCNH)
        self.insuranceImage.addGestureRecognizer(tapInsurance)
        self.vehicleImageOne.addGestureRecognizer(tapVehicleOne)
        self.vehicleImageTwo.addGestureRecognizer(tapVehicleTwo)
        self.vehicleImageThree.addGestureRecognizer(tapVehicleThree)
        self.vehicleDocumentImage.addGestureRecognizer(tapDocument)
        self.residenceImage.addGestureRecognizer(tapResidence)
        self.rgImage.addGestureRecognizer(tapRG)
        self.criminalImage.addGestureRecognizer(tapCriminal)
        
    }
    
    
    //Button action
    @IBAction func personalPhotoTapped(_ sender: Any) {
        openView(validate: booleanPersonalPhotos, constraint: viewHeightPersonalPhoto, number: 150, time: 0.3)
        self.booleanPersonalPhotos = !booleanPersonalPhotos
    }
    
    @IBAction func cnhPhotoTapped(_ sender: Any) {
        openView(validate: booleanCNHPhotos, constraint: viewHeightCNHPhoto, number: 150, time: 0.3)
        self.booleanCNHPhotos = !booleanCNHPhotos
    }
    
    @IBAction func insurancePhotoTapped(_ sender: Any) {
        openView(validate: booleanInsurancePhotos, constraint: viewHeightInsurancePhoto, number: 150, time: 0.3)
        self.booleanInsurancePhotos = !booleanInsurancePhotos
    }
    
    @IBAction func vehiclePhotoTapped(_ sender: Any) {
        openView(validate: booleanVehiclePhotos, constraint: viewHeightVehiclePhoto, number: 150, time: 0.3)
        self.booleanVehiclePhotos = !booleanVehiclePhotos
    }
    
    @IBAction func vehicleDocumentPhotoTapped(_ sender: Any) {
        openView(validate: booleanDocumentVehiclePhotos, constraint: viewHeightVehicleDocumentPhoto, number: 150, time: 0.3)
        self.booleanDocumentVehiclePhotos = !booleanDocumentVehiclePhotos
    }
    
    @IBAction func residencePhotoTapped(_ sender: Any) {
        openView(validate: booleanResidencePhotos, constraint: viewHeightResidencePhoto, number: 150, time: 0.3)
        self.booleanResidencePhotos = !booleanResidencePhotos
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Aviso", message: "Você realmente deseja cancelar o cadastro?", preferredStyle: .alert)
        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func finalizeButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Aviso", message: "Os dados digitados estão corretos?", preferredStyle: .alert)
        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
            if self.checkImages() == ""{
                self.registerDriver()
            }else{
                self.avisoToast("A foto \"\(self.checkImages())\", não foi preenchida", posicao: 3, altura: 45, tipo: 3)
            }
        }
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //actions
    internal func checkImages() -> String{
        
        if self.photoData.getDriverBase64() == "" {
            return "Pessoal"
        }else if self.photoData.getCNHBase64() == ""{
            return "CNH"
        }else if self.photoData.getInsuranceBase64() == ""{
            return "Seguro"
        }else if self.photoData.getVehicleOneBase64() == ""{
            return "Primeira foto do veículo"
        }else if self.photoData.getVehicleTwoBase64() == ""{
            return "Segunda foto do veículo"
        }else if self.photoData.getVehicleThreeBase64() == ""{
            return "Terceira foto do veículo"
        }else if self.photoData.getDocumentBase64() == ""{
            return "Documento do veículo"
        }else if self.photoData.getResidenceBase64() == ""{
            return "Comprovante de residencia"
        }else if self.photoData.getRGBase64() == ""{
            return "RG"
        }else if self.photoData.getCriminalBase64() == ""{
            return "Antecedentes criminais"
        }else{
            return ""
        }
        
    }
    
    public func recoveryAllData() -> String{
        
        let postField = "nome_usuario=\(self.personalData.getDriverName())&email_usuario=\(self.personalData.getEmail())&telefone_usuario=\(self.personalData.getPhone())&senha_usuario=\(self.personalData.getPassword().sha1())&facebook_usuario=\(self.personalData.getIdFacebook()!)&rg_usuario=\(self.personalData.getRG())&cpf_usuario=\(self.personalData.getCPF())&celular_usuario=\(self.personalData.getcellPhone().components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: ""))&logradouro_usuario=\(self.personalData.getAddress())&cidade_usuario=\(self.personalData.getCity())&fk_uf_usuario=\(self.personalData.getIdState())&bairro_usuario=\(self.personalData.getNeighborhood())&cep_usuario=\(self.personalData.getZipCode())&complemento_usuario=\(self.personalData.getComplement()!)&num_residencia_usuario=\(self.personalData.getResidenceNumber())&fk_genero=\(self.personalData.getGender())&fk_cidade_ativa_motorista=\(self.vehicleData.getIdActive())&fk_modelo_carro_motorista=\(self.vehicleData.getIdModelCar())&cor_carro_motorista=\(self.vehicleData.getCarColor())&placa_carro_motorista=\(self.vehicleData.getCarLicensePlate())&motorista=\(self.photoData.getDriverBase64())&cnh=\(self.photoData.getCNHBase64())&seguro=\(self.photoData.getInsuranceBase64())&documento=\(self.photoData.getDocumentBase64())&veiculo_1=\(self.photoData.getVehicleOneBase64())&veiculo_2=\(self.photoData.getVehicleTwoBase64())&veiculo_3=\(self.photoData.getVehicleThreeBase64())&comprovante=\(self.photoData.getResidenceBase64())&rg=\(self.photoData.getRGBase64())&antecedentes=\(self.photoData.getCriminalBase64()!)"
        
        return postField
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}


//function to open gallery or open the camera
extension RegisterThirdScreenViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func openCameraOrGallery(photo: UITapGestureRecognizer){
        
        let image = photo.view!
        self.imageTag = image.tag
        
        let alert = UIAlertController(title: "Aviso", message: "Escolha de onde deseja obter a imagem.", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Câmera", style: .default) { (action) in
            //MARK: TODO: fazer funcioanr abrir a camera
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType    = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
            
        }
        
        let library = UIAlertAction(title: "Biblioteca", style: .default) { (action) in
            //MARK: TODO: fazer funcioanr a biblioteca
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType    = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
                
            }
        }
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            chooseUIImageView(tag: self.imageTag, image: image)
        }
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            chooseUIImageView(tag: self.imageTag, image: image)
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    internal func chooseUIImageView(tag: Int, image: UIImage){
      
        switch tag {
        case 1:
            self.personalImage.image = image
            self.photoData.setDriverPhoto(driverPhoto: image)
        case 2:
            self.cnhImage.image = image
            self.photoData.setCNHPhoto(cnhPhoto: image)
        case 3:
            self.insuranceImage.image = image
            self.photoData.setInsurancePhoto(insurancePhoto: image)
        case 4:
            self.vehicleImageOne.image = image
            self.photoData.setVehicleOnePhoto(vehicleOnePhoto: image)
        case 5:
            self.vehicleImageTwo.image = image
            self.photoData.setVehicleTwoPhoto(vehicleTwoPhoto: image)
        case 6:
            self.vehicleImageThree.image = image
            self.photoData.setVehicleThreePhoto(vehicleThreePhoto: image)
        case 7:
            self.vehicleDocumentImage.image = image
            self.photoData.setDocumentPhoto(documentPhoto: image)
        case 8:
            self.residenceImage.image = image
            self.photoData.setResidencePhoto(residencePhoto: image)
        case 9:
            self.rgImage.image = image
            self.photoData.setRGPhoto(rgPhoto: image)
        case 10:
            self.criminalImage.image = image
            self.photoData.setCriminalPhoto(criminalPhoto: image)
        default:
            break
        }
        
    }
    
}
