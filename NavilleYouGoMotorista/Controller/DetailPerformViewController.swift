//
//  DetailPerformViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 27/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import Cosmos

class DetailPerformViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var labelIDRace: UILabel!
    @IBOutlet weak var labelBeginDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var labelVehicle: UILabel!
    @IBOutlet weak var labelLicensePlate: UILabel!
    @IBOutlet weak var labelCustomerName: UILabel!
    @IBOutlet weak var labelBeginPlace: UILabel!
    @IBOutlet weak var labelEndPlace: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    
    @IBOutlet weak var viewDetails: UIView!
    
    //variables and constants
    var uniqueDetailHistory = DetailPerformRaceData()
    var viewEvaluate = UIView()
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        viewDetails.layer.cornerRadius = 16
        viewDetails.layer.shadowColor = UIColor.black.cgColor
        viewDetails.layer.shadowOffset = CGSize(width: 1, height: 1)
        viewDetails.layer.shadowOpacity = 1.0
        viewDetails.layer.shadowRadius = 1
        setDataInView()
        
    }
    
    internal func setDataInView(){
        
        self.labelIDRace.text           = String(describing: uniqueDetailHistory.getIDRace())
        self.labelBeginDate.text        = uniqueDetailHistory.getBeginDate()
        self.labelEndDate.text          = uniqueDetailHistory.getEndDate()
        labelVehicle.text               = uniqueDetailHistory.getCar()
        labelLicensePlate.text          = uniqueDetailHistory.getLicensePlate()
        labelCustomerName.text          = uniqueDetailHistory.getCustomerName()
        labelBeginPlace.text            = uniqueDetailHistory.getOriginAddress()
        labelEndPlace.text              = uniqueDetailHistory.getDestinationAddress()
        labelDistance.text              = String(describing: uniqueDetailHistory.getDistance()) + " KM"
        labelValue.text                 = uniqueDetailHistory.getCost()
        labelStatus.text                = uniqueDetailHistory.getStatus()
        
    }
    
    @IBAction func buttonCostTapped(_ sender: Any) {
        
        performSegue(withIdentifier: SEGUE_TO_RACE_COST, sender: uniqueDetailHistory.getIDRace())
        
    }
    
    @IBAction func buttonEvaluateTapped(_ sender: Any) {
        
        if self.uniqueDetailHistory.getDriverEvaluate() == 1 {
            
            let alert = UIAlertController(title: "Aviso", message: "Você já avaliou está corrida.", preferredStyle: .alert)
            let ok    = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }else if self.uniqueDetailHistory.getDriverEvaluate() == 0{
            self.createViewEvaluations(raceID: uniqueDetailHistory.getIDRace(), passengerID: self.uniqueDetailHistory.getIDCustomer())
        }else{
            
            let alert = UIAlertController(title: "Aviso", message: "Esta corrida não pode mais ser avaliada.", preferredStyle: .alert)
            let ok    = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TO_RACE_COST{
            
            let destination = segue.destination as! DetailCostRaceViewController
            destination.idRace = sender as! Int
            
        }
        
    }
    
    @objc internal func closeEvaluation(){
        viewEvaluate.removeFromSuperview()
    }
    
}

extension DetailPerformViewController{
    
    func createViewEvaluations(raceID: Int, passengerID: Int){
        
        //buttonEvaluate
        let buttonEvaluate = CustomButton()
        
        let viewEvaluations = UIView()
        viewEvaluations.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewEvaluations.layer.cornerRadius = 16
        viewEvaluations.clipsToBounds = true
        viewEvaluations.layer.borderWidth = 1
        viewEvaluations.layer.borderColor = UIColor.black.cgColor
        viewEvaluations.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints a viewEvaluations
        let horizontalViewEvaluations  = NSLayoutConstraint(item: viewEvaluations, attribute: .centerX , relatedBy: .equal, toItem: self.view, attribute: .centerX , multiplier: 1, constant: 0)
        let verticalViewEvaluations    = NSLayoutConstraint(item: viewEvaluations, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
        let widthViewEvaluations      = NSLayoutConstraint(item: viewEvaluations, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightViewEvaluations     = NSLayoutConstraint(item: viewEvaluations, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.height - 32)
        self.view.addSubview(viewEvaluations)
        self.view.addConstraints([horizontalViewEvaluations, verticalViewEvaluations, widthViewEvaluations, heightViewEvaluations])
        
        let labelEvaluations = UILabel()
        labelEvaluations.text = "Avalie o passageiro"
        labelEvaluations.textAlignment = .center
        labelEvaluations.backgroundColor = UIColor.white
        labelEvaluations.font = UIFont.boldSystemFont(ofSize: 16)
        labelEvaluations.clipsToBounds = true
        labelEvaluations.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints a labelEvaluations
        let horizontalLabelEvaluations  = NSLayoutConstraint(item: labelEvaluations, attribute: .centerX , relatedBy: .equal, toItem: viewEvaluations, attribute: .centerX , multiplier: 1, constant: 0)
        let verticalLabelEvaluations    = NSLayoutConstraint(item: labelEvaluations, attribute: .top, relatedBy: .equal, toItem: viewEvaluations, attribute: .top, multiplier: 1, constant: 0)
        let widthLabelEvaluations      = NSLayoutConstraint(item: labelEvaluations, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightLabelEvaluations     = NSLayoutConstraint(item: labelEvaluations, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        viewEvaluations.addSubview(labelEvaluations)
        viewEvaluations.addConstraints([horizontalLabelEvaluations, verticalLabelEvaluations, widthLabelEvaluations, heightLabelEvaluations])
        
        
        //customer image
        let customerImage = UIImageView()
        customerImage.layer.borderWidth = 1
        customerImage.layer.borderColor = UIColor.white.cgColor
        customerImage.contentMode = .scaleAspectFill
        customerImage.clipsToBounds = true
        customerImage.layer.cornerRadius = 60
        customerImage.translatesAutoresizingMaskIntoConstraints = false
        
        if let passengerImage: Int = passengerID{
            
            let url = URL(string: self.shared.serverCustomerImageShared + "passageiro_\(passengerImage)/passageiro.png")
            print(url!)
            DispatchQueue.global().async {
                
                if url != nil {
                    
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        customerImage.image = UIImage(data: data!)
                    }
                    
                }
                
            }
            
        }
        
        //add constraints image
        let constraintOne = NSLayoutConstraint(item: customerImage, attribute: .top, relatedBy: .equal, toItem: labelEvaluations, attribute: .bottom, multiplier: 1, constant: 16)
        let constraintTwo = NSLayoutConstraint(item: customerImage, attribute: .centerX, relatedBy: .equal, toItem: viewEvaluations, attribute: .centerX, multiplier: 1, constant: 0)
        let constraintThree = NSLayoutConstraint(item: customerImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 120)
        let constraintFour  = NSLayoutConstraint(item: customerImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 120)
        
        viewEvaluations.addSubview(customerImage)
        viewEvaluations.addConstraints([constraintOne, constraintTwo, constraintThree, constraintFour])
        
        
        //labelPunctuality
        let labelPunctuality = UILabel()
        labelPunctuality.text = "Pontualidade"
        labelPunctuality.textAlignment = .left
        labelPunctuality.textColor = UIColor.white
        labelPunctuality.font = UIFont.boldSystemFont(ofSize: 14)
        labelPunctuality.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints labelPunctuality
        let horizontalLabelPunctuality  = NSLayoutConstraint(item: labelPunctuality, attribute: .top , relatedBy: .equal, toItem: customerImage, attribute: .bottom , multiplier: 1, constant: 16)
        let verticalLabelPunctuality    = NSLayoutConstraint(item: labelPunctuality, attribute: .leading, relatedBy: .equal, toItem: viewEvaluations, attribute: .leading, multiplier: 1, constant: 8)
        let widthLabelPunctuality      = NSLayoutConstraint(item: labelPunctuality, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightLabelPunctuality    = NSLayoutConstraint(item: labelPunctuality, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
        viewEvaluations.addSubview(labelPunctuality)
        viewEvaluations.addConstraints([horizontalLabelPunctuality, verticalLabelPunctuality, widthLabelPunctuality, heightLabelPunctuality])
        
        //Cosmos Punctuality
        let punctuality = CosmosView()
        
        //Cosmos Sympathy
        let sympathy = CosmosView()
        
        DispatchQueue.main.async {
            
            
            punctuality.settings.updateOnTouch = true
            punctuality.settings.starSize = 35
            punctuality.settings.updateOnTouch = true
            punctuality.settings.filledColor = #colorLiteral(red: 0.999070704, green: 0.8535897136, blue: 0.004626365378, alpha: 1)
            buttonEvaluate.punctuality = 3
            punctuality.didFinishTouchingCosmos = {rating in
                buttonEvaluate.punctuality = rating
            }
            punctuality.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints cosmos Punctuality
            let horizontalPunctuality  = NSLayoutConstraint(item: punctuality, attribute: .leading , relatedBy: .equal, toItem: viewEvaluations, attribute: .leading , multiplier: 1, constant: 8)
            let verticalPunctuality    = NSLayoutConstraint(item: punctuality, attribute: .top, relatedBy: .equal, toItem: labelPunctuality, attribute: .bottom, multiplier: 1, constant: 16)
            let widthPunctuality     = NSLayoutConstraint(item: punctuality, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewEvaluations.frame.size.width)
            let heightPunctuality    = NSLayoutConstraint(item: punctuality, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            viewEvaluations.addSubview(punctuality)
            viewEvaluations.addConstraints([horizontalPunctuality, verticalPunctuality, widthPunctuality, heightPunctuality])
            
            
            //labelPunctuality
            let labelSympathy = UILabel()
            labelSympathy.text = "Simpatia"
            labelSympathy.textAlignment = .left
            labelSympathy.textColor = UIColor.white
            labelSympathy.font = UIFont.boldSystemFont(ofSize: 14)
            labelSympathy.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints labelPunctuality
            let horizontalLabelSympathy  = NSLayoutConstraint(item: labelSympathy, attribute: .top , relatedBy: .equal, toItem: punctuality, attribute: .bottom , multiplier: 1, constant: 16)
            let verticalLabelSympathy    = NSLayoutConstraint(item: labelSympathy, attribute: .leading, relatedBy: .equal, toItem: punctuality, attribute: .leading, multiplier: 1, constant: 0)
            let widthLabelSympathy       = NSLayoutConstraint(item: labelSympathy, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
            let heightLabelSympathy      = NSLayoutConstraint(item: labelSympathy, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            viewEvaluations.addSubview(labelSympathy)
            viewEvaluations.addConstraints([horizontalLabelSympathy, verticalLabelSympathy, widthLabelSympathy, heightLabelSympathy])
            
            
            
            sympathy.settings.starSize = 35
            sympathy.settings.updateOnTouch = true
            sympathy.settings.filledColor = #colorLiteral(red: 0.999070704, green: 0.8535897136, blue: 0.004626365378, alpha: 1)
            buttonEvaluate.sympathy = 3
            sympathy.didFinishTouchingCosmos = {rating in
                buttonEvaluate.sympathy = rating
                
            }
            
            
            sympathy.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints cosmos Sympathy
            let horizontalSympathy  = NSLayoutConstraint(item: sympathy, attribute: .leading , relatedBy: .equal, toItem: viewEvaluations, attribute: .leading , multiplier: 1, constant: 8)
            let verticalSympathy    = NSLayoutConstraint(item: sympathy, attribute: .top, relatedBy: .equal, toItem: labelSympathy, attribute: .bottom, multiplier: 1, constant: 16)
            let widthSympathy    = NSLayoutConstraint(item: sympathy, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewEvaluations.frame.size.width)
            let heightSympathy    = NSLayoutConstraint(item: sympathy, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            viewEvaluations.addSubview(sympathy)
            viewEvaluations.addConstraints([horizontalSympathy, verticalSympathy, widthSympathy, heightSympathy])
            
        }
        
        //create buttom close and button Evaluate
        //buttonClose
        let buttonClose = UIButton()
        buttonClose.setTitle("Fechar", for: .normal)
        buttonClose.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        buttonClose.translatesAutoresizingMaskIntoConstraints = false
        buttonClose.addTarget(self, action: #selector(closeEvaluation), for: .touchUpInside)
        
        //add constraints a buttonClose
        let horizontalButtonClose  = NSLayoutConstraint(item: buttonClose, attribute: .bottom , relatedBy: .equal, toItem: viewEvaluations, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalButtonClose    = NSLayoutConstraint(item: buttonClose, attribute: .leading, relatedBy: .equal, toItem: viewEvaluations, attribute: .leading, multiplier: 1, constant: 0)
        let widthButtonClose      = NSLayoutConstraint(item: buttonClose, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (self.view.frame.size.width - 32) / 2)
        let heightButtonClose     = NSLayoutConstraint(item: buttonClose, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewEvaluations.addSubview(buttonClose)
        viewEvaluations.addConstraints([horizontalButtonClose, verticalButtonClose, widthButtonClose, heightButtonClose])
        
        
        buttonEvaluate.idRace      = raceID
        buttonEvaluate.setTitle("Avaliar", for: .normal)
        buttonEvaluate.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        buttonEvaluate.translatesAutoresizingMaskIntoConstraints = false
        buttonEvaluate.addTarget(self, action: #selector(self.evaluationCustomer(sender:)), for: .touchUpInside)
        
        //add constraints a buttonEvaluate
        let horizontalButtonEvaluate  = NSLayoutConstraint(item: buttonEvaluate, attribute: .bottom , relatedBy: .equal, toItem: viewEvaluations, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalButtonEvaluate    = NSLayoutConstraint(item: buttonEvaluate, attribute: .trailing, relatedBy: .equal, toItem: viewEvaluations, attribute: .trailing, multiplier: 1, constant: 0)
        let widthButtonEvaluate      = NSLayoutConstraint(item: buttonEvaluate, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (self.view.frame.size.width - 32) / 2)
        let heightButtonEvaluate     = NSLayoutConstraint(item: buttonEvaluate, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewEvaluations.addSubview(buttonEvaluate)
        viewEvaluations.addConstraints([horizontalButtonEvaluate, verticalButtonEvaluate, widthButtonEvaluate, heightButtonEvaluate])
        
        self.viewEvaluate = viewEvaluations
        
    }
    
}
