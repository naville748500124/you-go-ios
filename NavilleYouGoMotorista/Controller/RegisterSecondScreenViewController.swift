//
//  RegisterSecondScreenViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class RegisterSecondScreenViewController: UIViewController {

    
    //Elements of screen
    @IBOutlet weak var textFieldCity: TextFieldCustom!
    @IBOutlet weak var textFieldVehicleBrand: TextFieldCustom!
    @IBOutlet weak var textFieldVehicleModel: TextFieldCustom!
    @IBOutlet weak var textFieldVehicleLicensePlate: TextFieldCustom!
    @IBOutlet weak var textFieldVehicleColor: TextFieldCustom!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var citiesArray = [GenericField]()
    var brandsArray = [GenericField]()
    var modelsArray = [GenericField]()
    var personalData = PersonalData()
    var vehicleData = VehicleData()
    
    var pickerCities = UIPickerView()
    var pickerBrands = UIPickerView()
    var pickerModels = UIPickerView()
    
    //variable used to controller webservice function
    var functionController = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Recovery data from webservice
        self.preRegistrationDriver()
        
        
        //transform textfield in pickerview
        self.pickerCities.delegate      = self
        self.pickerCities.dataSource    = self
        self.pickerBrands.delegate      = self
        self.pickerBrands.dataSource    = self
        self.pickerModels.delegate      = self
        self.pickerModels.dataSource    = self
        self.pickerCities.tag = 1
        self.pickerBrands.tag = 2
        self.pickerModels.tag = 3
        self.textFieldCity.inputView         = self.pickerCities
        self.textFieldVehicleBrand.inputView = self.pickerBrands
        self.textFieldVehicleModel.inputView = self.pickerModels
        
        self.textFieldVehicleModel.isEnabled = false
    }


    //Button actions
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Aviso", message: "Você realmente deseja cancelar o cadastro?", preferredStyle: .alert)
        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if self.validateFields() == "" {
            
            if (self.textFieldVehicleLicensePlate.text?.count)! < 8{
                    avisoToast("A placa precisa ter 3 letras e 4 números.", posicao: 3, altura: 45, tipo: 3)
            }else{
                self.recoverFields()
            }
        }else{
            
           avisoToast("O campo: \"\(self.validateFields())\", está vazio.", posicao: 3, altura: 45, tipo: 3)
            
        }
        
    }
    
    //actions
    func validateFields() -> String{
        
        if (self.textFieldCity.text?.isEmpty)! {
            return "Cidade de trabalho"
        }else if (self.textFieldVehicleBrand.text?.isEmpty)!{
            return "Marca do veículo"
        }else if (self.textFieldVehicleModel.text?.isEmpty)!{
            return "Modelo do veículo"
        }else if (self.textFieldVehicleLicensePlate.text?.isEmpty)!{
            return "Placa do veículo"
        }else if (self.textFieldVehicleColor.text?.isEmpty)!{
            return "Cor do veículo"
        }else{
            return ""
        }
        
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_THIRD_SCREEN_REGISTER {
            
            let destination = segue.destination as! RegisterThirdScreenViewController
            destination.personalData = self.personalData
            destination.vehicleData = self.vehicleData
            
        }
    }
    
    internal func recoverFields(){
        
        self.vehicleData.setCarColor(carColor: self.textFieldVehicleColor.text!)
        self.vehicleData.setCarLicensePlate(carLicensePlate: self.textFieldVehicleLicensePlate.text!)
        performSegue(withIdentifier: SEGUE_THIRD_SCREEN_REGISTER, sender: nil)
    }
    
}

extension RegisterSecondScreenViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return self.citiesArray.count
        }else if pickerView.tag == 2 {
            return self.brandsArray.count
        }else if pickerView.tag == 3 {
            return self.modelsArray.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            return self.citiesArray[row].name
        }else if pickerView.tag == 2 {
            return self.brandsArray[row].name
        }else if pickerView.tag == 3 {
            return self.modelsArray[row].name
        }else{
            return ""
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            
            self.textFieldCity.text = citiesArray[row].name
            self.vehicleData.setIdActive(idActiveCity: citiesArray[row].id)
            
        }else if pickerView.tag == 2 {
            
            self.textFieldVehicleBrand.text = brandsArray[row].name
            self.vehicleData.setIdBrandCar(idBrandCar: brandsArray[row].id)
            self.findModels()
            
        }else if pickerView.tag == 3{
            
            self.textFieldVehicleModel.text = modelsArray[row].name
            self.vehicleData.setIdModelCar(idModelCar: modelsArray[row].id)
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}
