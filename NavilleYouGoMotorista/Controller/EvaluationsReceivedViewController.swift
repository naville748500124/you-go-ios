//
//  EvaluationsReceivedViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import Cosmos

class EvaluationsReceivedViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var punctuality: CosmosView!
    @IBOutlet weak var sympathy: CosmosView!
    @IBOutlet weak var car: CosmosView!
    @IBOutlet weak var service: CosmosView!
    @IBOutlet weak var driverPhoto: ImageCustom!
    
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var evaluations = Evaluations()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //recovery driver image
        if let driverImage: String = self.shared.userID as? String{
            
            let url = URL(string: self.shared.serverImageShared + "motorista_" + driverImage + "/motorista.png")
            print(url!)
            print(self.shared.userID)
            DispatchQueue.global().async {
                
                if url != nil {
                    
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        self.driverPhoto.image = UIImage(data: data!)
                    }
                    
                }
                
            }
            
        }
        
        //request evaluations from webservice
        self.requestEvaluations()
    }
    
    internal func updateEvaluations(){
        
        print("printando a pontualidade =\(self.evaluations.getService())")
        
        self.punctuality.rating = self.evaluations.getPunctuality()
        self.sympathy.rating    = self.evaluations.getSympathy()
        self.car.rating         = self.evaluations.getCar()
        self.service.rating     = self.evaluations.getService()
        
    }

}
