//
//  FaqViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 02/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import WebKit

class FaqViewController: UIViewController {
    
    //Elements of screen
    @IBOutlet weak var webView: WKWebView!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ActivityIndicator.start()
        
        //configuring webview
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        self.webView.allowsLinkPreview = true
        self.webView.allowsBackForwardNavigationGestures = true
        
        //pass a link to webview
        let request = URLRequest(url: URL(string: self.shared.faqServer)!)
        self.webView.load(request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    
    func voltar() {
        self.dismiss(animated: true, completion: nil)
    }
}

//webKit essencials
extension FaqViewController: WKUIDelegate, WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.stop()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        var action: WKNavigationActionPolicy?
        
        defer {
            decisionHandler(action ?? .allow)
        }
        
        guard let url = navigationAction.request.url else { return }
        
        if String(describing: url) == "megamil.net.webview-avancado://voltar" {
            self.voltar()
        }
        
        if navigationAction.navigationType == .linkActivated, url.absoluteString.hasPrefix("megamil.net.webview-avancado") {
            action = .cancel
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
}

