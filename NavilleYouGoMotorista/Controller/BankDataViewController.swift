//
//  BankDataViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class BankDataViewController: UIViewController {

    
    //Elements of screen
    @IBOutlet weak var textFieldBank: TextFieldCustom!
    @IBOutlet weak var textFieldAgency: TextFieldCustom!
    @IBOutlet weak var textFieldAccount: TextFieldCustom!
    @IBOutlet weak var textFieldDigit: TextFieldCustom!
    @IBOutlet weak var registarButton: ButtonCustom!
    
    @IBOutlet weak var switchContaCorrente: UISwitch!
    @IBOutlet weak var switchPoupanca: UISwitch!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var bankArray = [GenericField]()
    var pickerBank = UIPickerView()
    var functionController = 0
    var idBank = 0
    var validateBankData = 0
    var tipoConta = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //configuration textfield to pickerView
        pickerBank.dataSource = self
        pickerBank.delegate   = self
        textFieldBank.inputView = pickerBank
        
        if self.shared.bankID.isEmpty {
            self.registarButton.setTitle("Cadastrar", for: .normal)
        }else{
            self.registarButton.setTitle("Atualizar", for: .normal)
        }
        
        if self.validateBankData == 2 {
            
            if self.shared.bankID.isEmpty {
                avisoToast("Cadastre os dados bancários para poder ficar online.", posicao: 2, altura: 62, tipo: 3)
            }else{
                avisoToast("Atualize os dados bancários para poder ficar online.", posicao: 2, altura: 62, tipo: 3)
            }
            
        }
        
        self.findBanks()
        self.recoveredData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.bool(forKey: "cc") {
            switchContaCorrente.setOn(true, animated: false)
            switchPoupanca.setOn(false, animated: false)
        }
        
        if UserDefaults.standard.bool(forKey: "cp") {
            switchPoupanca.setOn(true, animated: false)
            switchContaCorrente.setOn(false, animated: false)
        }
    }
    
    @IBAction func switchCorrenteChangedValue(_ sender: UISwitch) {
        
        if switchContaCorrente.isOn {
            switchContaCorrente.setOn(true, animated: true)
            switchPoupanca.setOn(false, animated: true)
            tipoConta = 1
            UserDefaults.standard.set(true, forKey: "cc")
            UserDefaults.standard.set(false, forKey: "cp")
            
        } else {
            switchContaCorrente.setOn(false, animated: true)
            switchPoupanca.setOn(true, animated: true)
            tipoConta = 2
            UserDefaults.standard.set(true, forKey: "cp")
            UserDefaults.standard.set(false, forKey: "cc")
        }
        
    }
    
    @IBAction func switchPoupancaChangedValue(_ sender: Any) {
        
        if switchPoupanca.isOn {
            switchPoupanca.setOn(true, animated: true)
            switchContaCorrente.setOn(false, animated: true)
            tipoConta = 2
            
        } else {
            switchPoupanca.setOn(false, animated: true)
            switchContaCorrente.setOn(true, animated: true)
            tipoConta = 1
        }
        print("Parametros TC: ", tipoConta)
    }
    
    //buttons actions
    @IBAction func registarUpdataButtonTapped(_ sender: Any) {
        
        if !((self.textFieldBank.text?.isEmpty)!){
            
            if !((self.textFieldAgency.text?.isEmpty)!){
                
                if !((self.textFieldAccount.text?.isEmpty)!){
                    
                    if !((self.textFieldDigit.text?.isEmpty)!){
                    
                        let alert = UIAlertController(title: "Aviso", message: "Os dados bancários estão corretos?", preferredStyle: .alert)
                        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
                        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
                            
                            self.registerBank(tipoConta: self.tipoConta)
                        }
                        alert.addAction(no)
                        alert.addAction(yes)
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        avisoToast("O campo \"Digito\", está vazio.", posicao: 3, altura: 45, tipo: 3)
                    }
                    
                }else{
                    avisoToast("O campo \"Conta\", está vazio.", posicao: 3, altura: 45, tipo: 3)
                }
            
            }else{
                avisoToast("O campo \"Agência\", está vazio.", posicao: 3, altura: 45, tipo: 3)
            }
            
        }
        
    }
    
    
    //actions
    func recoveredData(){
        
        if let id = self.shared.bankID as? String{
            
            if let agency = self.shared.agencyDriver as? String{
                
                if let account = self.shared.accountDriver as? String{
                    
                    if let digit = self.shared.digitAccount as? String{
                        
                        self.textFieldAgency.text = agency
                        self.textFieldAccount.text = account
                        self.textFieldDigit.text = digit
                        
                    }
                    
                }
                
            }
            
        }
        
    }

}

extension BankDataViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bankArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.bankArray[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textFieldBank.text = self.bankArray[row].name
        self.idBank = self.bankArray[row].id
    }
    
}
