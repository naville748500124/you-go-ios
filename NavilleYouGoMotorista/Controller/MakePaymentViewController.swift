//
//  MakePaymentViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import WebKit

class MakePaymentViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var webView: WKWebView!
    var idRace = 0
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("printando o id da corrida na modal de pagamento = \(self.idRace)")
        
        ActivityIndicator.start()
        
        //configuring view
        self.viewMain.layer.cornerRadius = 16
        self.viewMain.clipsToBounds = true
        self.viewMain.layer.borderWidth = 2
        self.viewMain.layer.borderColor = UIColor.white.cgColor
        
        //configuring webview
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        self.webView.allowsLinkPreview = true
        self.webView.allowsBackForwardNavigationGestures = true
        
        //pass a link to webview
        let request = URLRequest(url: URL(string: "\(self.shared.paymentURL)\(self.idRace)")!)
        self.webView.load(request)
    }
    
    func voltar(){
        self.dismiss(animated: true, completion: nil)
    }
    
}

//webKit essencials
extension MakePaymentViewController: WKUIDelegate, WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.stop()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        var action: WKNavigationActionPolicy?
        
        defer {
            decisionHandler(action ?? .allow)
        }
        
        guard let url = navigationAction.request.url else { return }
        
        if String(describing: url) == "megamil.net.webview-avancado://voltar" {
            self.voltar()
        }
        
        if navigationAction.navigationType == .linkActivated, url.absoluteString.hasPrefix("megamil.net.webview-avancado") {
            action = .cancel
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
}


