//
//  PasswordViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 27/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class PasswordViewController: UIViewController {
    
    //Elements of screen
    @IBOutlet weak var textFieldCurrentPassword: TextFieldCustom!
    @IBOutlet weak var textFieldNewPassword: TextFieldCustom!
    @IBOutlet weak var textFieldConfirmNewPassword: TextFieldCustom!
    
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //buttons actions
    @IBAction func updateButtonTapped(_ sender: Any) {
        //MARK: TODO: make minimum number of caracteres
        if validateEmptyFields() == "" {
            
            if validateNewPassword() == ""{
                
                if validatePassword() == "" {
                    
                    if (self.textFieldNewPassword.text?.count)! < 8 {
                         avisoToast("Sua senha precisa ter no minimo 8 caracteres.", posicao: 3, altura: 45, tipo: 3)
                    }else{
                        
                        let alert = UIAlertController(title: "Aviso", message: "Você confirma a alteração da senha?", preferredStyle: .alert)
                        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
                        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
                            self.requestChangePassword()
                        }
                        alert.addAction(no)
                        alert.addAction(yes)
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }else{
                    avisoToast(validatePassword(), posicao: 3, altura: 45, tipo: 3)
                }
                
            }else{
                avisoToast(validateEmptyFields(), posicao: 3, altura: 45, tipo: 3)
            }
            
        }else{
            avisoToast("O campo \"\(validateEmptyFields())\", está vazio.", posicao: 3, altura: 45, tipo: 3)
        }
        
    }
    
    //functions
    internal func validateEmptyFields() -> String{
        
        if (self.textFieldCurrentPassword.text?.isEmpty)!{
            return "Senha atual"
        }else if (self.textFieldNewPassword.text?.isEmpty)!{
            return "Nova senha"
        }else if (self.textFieldConfirmNewPassword.text?.isEmpty)!{
            return "Confirmar senha"
        }else{
            return ""
        }
        
    }
    
    internal func validatePassword() -> String{
        if self.textFieldCurrentPassword.text!.sha1() != self.shared.userPassword{
            print("senha atual = \(self.shared.userPassword)")
            print("a nova senha = \(self.textFieldCurrentPassword.text!.sha1())")
            return "A senha atual não confere."
        }else{
            return ""
        }
    }
    
    internal func validateNewPassword() -> String{
        if self.textFieldNewPassword.text! != self.textFieldConfirmNewPassword.text!{
            return "A nova senha e a confirmação precisar ser iguais."
        }else{
            return ""
        }
    }
    
}
