//
//  PerformRaceViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import SDWebImage

class PerformRaceViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var performRacesTable: UITableView!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var history = [DetailPerformRaceData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.performRacesTable.delegate = self
        self.performRacesTable.dataSource = self
        self.requestDetails()
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TO_DATAIL_PERFORM {
            let destination = segue.destination as! DetailPerformViewController
            destination.uniqueDetailHistory = history[sender as! Int]
            
        }
        
    }
    
}

//configuration of table
extension PerformRaceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.history.count < 1 {
            let label = UILabel()
            label.text = "Nenhum histórico de corrida encontrado."
            label.adjustsFontSizeToFitWidth = true
            label.textAlignment = .center
            self.performRacesTable.backgroundView = label
        }else{
            self.performRacesTable.backgroundView = nil
        }
        
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.performRacesTable.dequeueReusableCell(withIdentifier: "cell") as! PerformRaceTableViewCell
        
        cell.viewContent.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        cell.viewContent.layer.cornerRadius = 16
        cell.viewContent.layer.shadowColor = UIColor.black.cgColor
        cell.viewContent.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.viewContent.layer.shadowOpacity = 1.0
        cell.viewContent.layer.shadowRadius = 1
        
        var date = history[indexPath.row].getBeginDate().split(separator: " ")
        cell.labelDate.text         = String(describing: date[0])
        cell.labelOrigin.text       = history[indexPath.row].getOriginAddress()
        cell.labelDestination.text  = history[indexPath.row].getDestinationAddress()
        cell.labelName.adjustsFontSizeToFitWidth = true
        cell.labelName.text         = history[indexPath.row].getCustomerName()
        cell.labelPrice.text        = history[indexPath.row].getCost()
        cell.imageCustomer.contentMode = .scaleAspectFill
        
        if let customerImage: Int = history[indexPath.row].getIDCustomer(){
            
            let url: NSURL? = NSURL(string: self.shared.serverCustomerImageShared + "passageiro_\(customerImage)/passageiro.png")
            if let urlImage = url{
                cell.imageCustomer.sd_setImage(with: urlImage as URL!)
            }
        }
        
//        if let customerImage: Int = history[indexPath.row].getIDCustomer(){
//            
//            let url = URL(string: self.shared.serverCustomerImageShared + "passageiro_\(customerImage)/passageiro.png")
//            print(url!)
//            DispatchQueue.global().async {
//                
//                if url != nil {
//                    
//                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//                    DispatchQueue.main.async {
//                        cell.imageCustomer.image = UIImage(data: data!)
//                    }
//                    
//                }
//                
//            }
//            
//        }
    
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SEGUE_TO_DATAIL_PERFORM, sender: indexPath.row)
    }
    
}
