//
//  DetailCostRaceViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 15/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import WebKit

class DetailCostRaceViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var webView: WKWebView!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var idRace = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ActivityIndicator.start()
        
        //configuring webView
        self.webView.navigationDelegate = self
        self.webView.uiDelegate         = self
        self.webView.allowsLinkPreview  = true
        self.webView.allowsBackForwardNavigationGestures = true
        
        //send the link to webView
        let request = URLRequest(url: URL(string: self.shared.serverDetailCostRace + "\(idRace)&usuario=2")!)
        self.webView.load(request)
    }

}

extension DetailCostRaceViewController: WKUIDelegate, WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.stop()
    }
    
}
