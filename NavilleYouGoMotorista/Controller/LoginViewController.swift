//
//  LoginViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import AccountKit
import FirebaseCore
import FirebaseInstanceID
import CoreLocation

var delegateSMSGlobal: ActivateSMSDelegate? = nil

class LoginViewController: UIViewController, ActivateSMSDelegate, CLLocationManagerDelegate{


//    class func ActiveSMS(email: String, password: String){
//
//        let login = LoginViewController.self()
//        login.textFieldEmail.text    = email
//        login.textFieldPassword.text = password.sha1()
//        login.commonLogin()
//
//    }
    
    func activeSMS(email: String, password: String) {
        
        self.textFieldEmail.text = email
        self.textFieldPassword.text = password
        self.commonLogin()
          
    }
    
    //Elements of the screen
    @IBOutlet weak var textFieldEmail: TextFieldCustom!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var btnShowHidePassword: UIButton!
    
    //variables and constants
    var functionController = 0
    var shared = UIApplication.shared.delegate as! AppDelegate
    var facebookEmail       = ""
    var facebookFirstName   = ""
    var facebookLastName    = ""
    var facebookID          = ""
    var facebookImage       = ""
    var userID              = 0
    var saveIdFunction      = 0
    var token = "" //Notificações
    
    var isShowingPassword = false
    
    //variable to take map permission
    var locationManager = CLLocationManager()
    
    //properties of accountKit
    let accountKit = AKFAccountKit(responseType: AKFResponseType.authorizationCode)
    var pendingLoginViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //facebook configuration
        if FBSDKAccessToken.current() != nil {
            self.recoveryDataFacebook()
        }else{
            //MARK: TODO: create userDefaults to save and revocery data on login
            if let email = UserDefaults.standard.value(forKey: "UserDefaultEmail") as? String{
                if let password = UserDefaults.standard.value(forKey: "UserDefaultPassword") as? String{
                    self.textFieldEmail.text    = email
                    self.textFieldPassword.text = password
                    self.commonLogin()
                }
            }
            
        }
        
        //accountKit configuration
        pendingLoginViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: generateState())
        
        if let token_ = InstanceID.instanceID().token() {
            token = token_
            print("InstanceID token: \(token)")
        }
        
        if (CLLocationManager.locationServicesEnabled()){
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.allowsBackgroundLocationUpdates = true
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func showHidePasswordPressed(_ sender: UIButton) {
        isShowingPassword = !isShowingPassword
        
        if isShowingPassword {
            btnShowHidePassword.setImage(#imageLiteral(resourceName: "visibility"), for: .normal)
            textFieldPassword.isSecureTextEntry = false
        } else {
            btnShowHidePassword.setImage(#imageLiteral(resourceName: "visibility_off"), for: .normal)
            textFieldPassword.isSecureTextEntry = true
        }
        
    }
    
    //Buttons action
    @IBAction func logInButtonTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        if validateFields() == "" {
            
            if (self.textFieldEmail.text?.isValidoEmail())!{
                
                self.commonLogin()
                
            }else{
                avisoToast("O endereço de \"E-MAIL\" digitado não é válido.", posicao: 3, altura: 45, tipo: 4)
            }
            
        }else{
            avisoToast("O endereço de \"\(validateFields())\", está vazio.", posicao: 3, altura: 45, tipo: 4)
        }
        
    }
    
    @IBAction func logInFacebookButtonTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            
            if error != nil {
                print("has no permission")
            }else{
                self.recoveryDataFacebook()
            }
            
        }
        
    }
    
    @IBAction func registarButtonTapped(_ sender: Any) {
        
        delegateSMSGlobal = self
        self.functionController = 0
        performSegue(withIdentifier: SEGUE_TO_REGISTER, sender: nil)
    }
    
    @IBAction func resetPasswordButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_RESET_PASSWORD, sender: nil)
    }
    
    @IBAction func privacyPolicyButtonTapped(_ sender: Any) {
        ActivityIndicator.start()
        performSegue(withIdentifier: SEGUE_TO_PRIVACY_POLICY, sender: nil)
        
    }
    
    //Actions
    internal func validateFields() -> String{
        
        if (self.textFieldEmail.text?.isEmpty)!{
            return "E-mail"
        }else if (self.textFieldPassword.text?.isEmpty)!{
            return "Senha"
        }else{
            return ""
        }
        
    }
    
    internal func recoveryDataFacebook(){
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, email, first_name, last_name, picture.type(large)"]).start { (connection, result, error) in
            
            if error != nil {
            }else{
                
                let resultDictinary = result as! NSDictionary
                print(result)
                if let email = resultDictinary["email"] as? String{
                    self.facebookEmail = email
                }
                
                if let first = resultDictinary["first_name"] as? String{
                    self.facebookFirstName = first
                }
                
                if let last = resultDictinary["last_name"] as? String{
                    self.facebookLastName = last
                }
                
                if let id = resultDictinary["id"] as? String{
                    self.facebookID = id
                }
                
                if let imageData = resultDictinary["picture"] as? NSDictionary{
                    if let data = imageData["data"] as? NSDictionary{
                        
                        self.facebookImage = data["url"] as! String
                        
                    }
                }
                
                self.facebookLogin()
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TO_REGISTER && self.functionController == 2 {
            
            let destination = segue.destination as! RegisterViewController
            destination.facebookEmail = self.facebookEmail
            destination.facebookImage = self.facebookImage
            destination.facebookID    = self.facebookID
            destination.facebookName  = self.facebookFirstName + " " + self.facebookLastName
            destination.validateButtonLogin = self.functionController
            
        }else if segue.identifier == SEGUE_TO_REGISTER && self.functionController != 2 {
            
            let destination = segue.destination as! RegisterViewController
            destination.facebookEmail = ""
            destination.facebookImage = ""
            destination.facebookID    = ""
            destination.facebookName  = ""
            destination.validateButtonLogin = 0
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

extension LoginViewController: FBSDKLoginButtonDelegate {
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
        
        if (result.declinedPermissions != nil) || result.isCancelled {
            print("CANCEL FACEBOOK")
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool { return true }
    
}

//AccountKit
extension LoginViewController: AKFViewControllerDelegate{
    
    func generateState() -> String{
        
        let uuid = NSUUID().uuidString
        return uuid
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        
       print("0")
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        
        print("1")
        self.dismiss(animated: true, completion: nil)
        self.saveIdFunction = self.functionController
        self.activatorSMS()
        
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        self.dismiss(animated: true, completion: nil)
        print("2")
        avisoToast("Ocorreu um erro, tente novamente em instantes.", posicao: 3, altura: 45, tipo: 4)
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        print("3")
        avisoToast("Você cancelou a verificação.", posicao: 3, altura: 45, tipo: 4)
        
    }
    
}

