//
//  RegisterViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift
import CryptoSwift

//MARK: TODO: colocar as mascaras
//MARK: TODO: deixar senha com 8 caracteres

class RegisterViewController: UIViewController {

    
    //Elements of screen
    @IBOutlet weak var textFieldName: TextFieldCustom!
    @IBOutlet weak var textFieldGender: TextFieldCustom!
    @IBOutlet weak var textFieldRG: JMMaskTextField!
    @IBOutlet weak var textFieldCPF: TextFieldCustom!
    @IBOutlet weak var textFieldEmail: TextFieldCustom!
    @IBOutlet weak var textFieldConfirmEmail: TextFieldCustom!
    @IBOutlet weak var textFieldPassword: TextFieldCustom!
    @IBOutlet weak var textFieldConfirmPassword: TextFieldCustom!
    @IBOutlet weak var textFieldPhone: TextFieldCustom!
    @IBOutlet weak var textFieldCellPhone: TextFieldCustom!
    @IBOutlet weak var textFieldZipCode: TextFieldCustom!
    @IBOutlet weak var textFieldAddress: TextFieldCustom!
    @IBOutlet weak var textFieldNeighborhood: TextFieldCustom!
    @IBOutlet weak var textFieldCity: TextFieldCustom!
    @IBOutlet weak var textFieldState: TextFieldCustom!
    @IBOutlet weak var textFieldComplement: TextFieldCustom!
    @IBOutlet weak var textFieldNumber: TextFieldCustom!
    @IBOutlet weak var switchUseTerms: UISwitch!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate  //This variable is to share the server path
    var functionController = 0
    var pickerGender = UIPickerView()
    var pickerState  = UIPickerView()
    var personalData = PersonalData()
    
    var facebookName  = ""
    var facebookEmail = ""
    var facebookImage = ""
    var facebookID    = ""
    var validateButtonLogin = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //recovered facebookData
        if self.validateButtonLogin == 2 {
            self.textFieldName.text = self.facebookName
            self.textFieldEmail.text = self.facebookEmail
            self.textFieldConfirmEmail.text = self.facebookEmail
            self.personalData.setIdFacebook(idFacebook: self.facebookID)
            self.personalData.setFacebookImage(facebookImage: self.facebookImage)
        }else{
            self.personalData.setFacebookImage(facebookImage: "")
            self.textFieldName.text = ""
            self.textFieldEmail.text = ""
            self.textFieldConfirmEmail.text = nil
            
        }
        
        //configuration textfilds
        self.textFieldZipCode.delegate = self
        self.textFieldZipCode.tag = 1
       
        
        //setting initial value on pickerview
        if GENDER.count > 0 {
            self.textFieldGender.text = GENDER.first?.name
            self.personalData.setGender(idGender: (GENDER.first?.id)!)
        }
        
        if STATES.count > 0 {
            self.textFieldState.text = STATES.first?.name
            self.personalData.setIdState(idState: (STATES.first?.id)!)
        }
        
        
        //configuration of pickers view
        self.pickerState.delegate = self
        self.pickerState.dataSource = self
        self.pickerGender.delegate = self
        self.pickerGender.dataSource = self
        self.pickerState.tag = 2
        self.pickerGender.tag = 1
        self.textFieldState.inputView = self.pickerState
        self.textFieldGender.inputView = self.pickerGender
        
    }
    
    //Button actions
    @IBAction func cancelButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Aviso", message: "Você realmente deseja cancelar o cadastro?", preferredStyle: .alert)
        let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
        let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if self.validateFields() == "" {
            
            if self.textFieldEmail.text == self.textFieldConfirmEmail.text {
                
                if self.textFieldPassword.text == textFieldConfirmPassword.text {
                    
                    if (self.textFieldCPF.text?.isValidoCPF)! {
                        
                        if (self.textFieldEmail.text?.isValidoEmail())! {
                            
                            if self.switchUseTerms.isOn{
                                
                                if (self.textFieldPassword.text?.count)! < 8 {
                                    avisoToast("Sua senha precisa ter no minimo 8 caracteres.", posicao: 3, altura: 45, tipo: 3)
                                }else{
                                    self.recoveryFieldsData()
                                }
                            }else{
                                avisoToast("Você precisa aceitar os termos de uso para avançar.", posicao: 3, altura: 45, tipo: 3)
                            }
                           
                        }else{
                            avisoToast("O endereço de \"E-MAIL\" digitado não é válido.", posicao: 3, altura: 45, tipo: 3)
                        }
                        
                    }else{
                        avisoToast("O \"CPF\" digitado não é válido.", posicao: 3, altura: 45, tipo: 3)
                    }
                    
                }else{
                    avisoToast("As \"SENHAS\" digitadas precisam ser iguais.", posicao: 3, altura: 45, tipo: 3)
                }
                
            }else{
                avisoToast("Os \"E-MAILS\" digitados precisam ser iguais.", posicao: 3, altura: 45, tipo: 3)
            }
            
        }else{
            avisoToast("O campo: \"\(self.validateFields())\", está vazio.", posicao: 3, altura: 45, tipo: 3)
        }
        
    }
    
    @IBAction func useTermsButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_USED_TERMS, sender: nil)
    }
    
    
    //actions
    func validateFields() -> String{
        
        if textFieldName.text == "" {
            return "Nome"
        }else if textFieldGender.text == "" {
            return "Sexo"
        }else if textFieldRG.text == "" {
            return "RG"
        }else if textFieldCPF.text == "" {
            return "CPF"
        }else if textFieldEmail.text == "" {
            return "E-mail"
        }else if textFieldPassword.text == "" {
            return "Senha"
        }else if textFieldPhone.text == "" {
            return "Telefone"
        }else if textFieldCellPhone.text == "" {
            return "Celular"
        }else if textFieldZipCode.text == "" {
            return "CEP"
        }else if textFieldAddress.text == "" {
            return "Logradouro"
        }else if textFieldNeighborhood.text == "" {
            return "Bairro"
        }else if textFieldCity.text == "" {
            return "Cidade"
        }else if textFieldState.text == "" {
            return "UF"
        }else if textFieldNumber.text == "" {
            return "Número"
        }else if textFieldConfirmEmail.text == "" {
            return "Confirmar e-mail"
        }else if textFieldConfirmPassword.text == "" {
            return "Confirmar senha"
        }
        return ""
    }
    
    
    func recoveryFieldsData(){
        
        self.personalData.setDriverName(name: self.textFieldName.text!)
        self.personalData.setRG(rg: self.textFieldRG.text!)
        self.personalData.setCPF(cpf: self.textFieldCPF.text!)
        self.personalData.setEmail(email: self.textFieldEmail.text!)
        self.personalData.setPassword(password: (self.textFieldPassword.text!))
        self.personalData.setPhone(phone: self.textFieldPhone.text!)
        self.personalData.setcellPhone(cellPhone: self.textFieldCellPhone.text!)
        self.personalData.setZipCode(zipCode: self.textFieldZipCode.text!)
        self.personalData.setAddress(address: self.textFieldAddress.text!)
        self.personalData.setResidenceNumber(residenceNumber: self.textFieldNumber.text!)
        self.personalData.setComplement(complement: self.textFieldComplement.text)
        self.personalData.setNeighborhood(neighborhood: self.textFieldNeighborhood.text!)
        self.personalData.setCity(city: self.textFieldCity.text!)
        
        performSegue(withIdentifier: SEGUE_SECOND_SCREEN_REGISTER, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_SECOND_SCREEN_REGISTER {
            
            let destination = segue.destination as! RegisterSecondScreenViewController
            destination.personalData = self.personalData
            
        }
        
    }
    
    @IBAction func rgChangeValue(_ sender: Any) {
        
        let mask  = JMStringMask(mask: "00.000.000-0")
        let mask2 = JMStringMask(mask: "00.000.000-A")
        var text = self.textFieldRG.text?.replacingOccurrences(of: ".", with: "")
        text = text?.replacingOccurrences(of: "-", with: "")
        
        if (text?.count)! < 8  {
            self.textFieldRG.keyboardType = UIKeyboardType.numberPad
            self.textFieldRG.reloadInputViews()
        }else {
            textFieldRG.keyboardType = .default
            self.textFieldRG.reloadInputViews()
        }
        
        if (text?.isEmpty)! || ((text?.contains("X"))! || (text?.contains("x"))!){
            
            if (text?.count)! < 10 {
                let maskedText = mask2.mask(string: text)
                self.textFieldRG.text = maskedText
            }else{
                text?.removeLast()
                let maskedText = mask2.mask(string: text)
                self.textFieldRG.text = maskedText
            }
        }else{
            if (text?.count)! < 10 {
                text = text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                
                let maskedText = mask.mask(string: text)
                self.textFieldRG.text = maskedText
            }else{
                text?.removeLast()
                let maskedText = mask.mask(string: text)
                self.textFieldRG.text = maskedText
            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

//textfield delegates and functions
extension RegisterViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            self.view.endEditing(true)
            self.findZipCode()
        }
    }
    
    
}



//configuration of pickers
extension RegisterViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if pickerView.tag == 1 {
            return 1
        }else if pickerView.tag == 2{
            return 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return GENDER.count
        }else if pickerView.tag == 2{
            return STATES.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            return GENDER[row].name
        }else if pickerView.tag == 2{
            return STATES[row].name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            self.textFieldGender.text = GENDER[row].name
            self.personalData.setGender(idGender: GENDER[row].id)
        }else if pickerView.tag == 2 {
            self.textFieldState.text = STATES[row].name
            self.personalData.setIdState(idState: STATES[row].id)
        }
        
    }
    
}
