//
//  UseTermsViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import WebKit

class UseTermsViewController: UIViewController {

    //Elements of screen
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var webView: WKWebView!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ActivityIndicator.start()
        
        //configuring view
        self.viewMain.layer.cornerRadius = 16
        self.viewMain.clipsToBounds = true
        self.viewMain.layer.borderWidth = 2
        self.viewMain.layer.borderColor = UIColor.white.cgColor
        
        //configuring webview
        self.webView.navigationDelegate = self
        self.webView.uiDelegate = self
        self.webView.allowsLinkPreview = true
        self.webView.allowsBackForwardNavigationGestures = true
        
        //pass a link to webview
        let request = URLRequest(url: URL(string: self.shared.termsServer)!)
        self.webView.load(request)
        
    }

    
    @IBAction func buttonCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//webKit essencials
extension UseTermsViewController: WKUIDelegate, WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ActivityIndicator.stop()
    }
    
}
