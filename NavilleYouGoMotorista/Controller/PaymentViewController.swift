//
//  PaymentViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    //Elements of screen
    @IBOutlet weak var paymentTable: UITableView!
    @IBOutlet weak var heightSearchConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldStatus: UITextField!
    @IBOutlet weak var textFieldFromDate: UITextField!
    @IBOutlet weak var textFieldToDate: UITextField!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var booleanSearch = false
    var datePicker = UIDatePicker()
    var pickerStatus = UIPickerView()
    var newSearch  = History()
    var arraySearch = [PaymentHistory]()
    var booleanRequestData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //configuring table
        self.paymentTable.delegate = self
        self.paymentTable.dataSource = self
        
        //configuring searchView
        self.heightSearchConstraint.constant = 0
        
        //configuring TextFields
        self.pickerStatus.delegate = self
        self.pickerStatus.dataSource = self
        self.textFieldStatus.inputView = pickerStatus
        self.textFieldFromDate.tag = 0
        self.textFieldToDate.tag   = 1
        self.datePicker.datePickerMode = .date
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        minDateComponent.day = 01
        minDateComponent.month = 01
        minDateComponent.year = 2018
        
        let minDate = calendar.date(from: minDateComponent)
        
        var maxDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        maxDateComponent.day = 31
        maxDateComponent.month = 12
        maxDateComponent.year = 2100
        
        let maxDate = calendar.date(from: maxDateComponent)
        
        datePicker.minimumDate = minDate! as Date
        datePicker.maximumDate =  maxDate! as Date
        
        if STATUS.count > 0 {
            self.textFieldStatus.text = STATUS.first?.name
            self.newSearch.setStatus(status: (STATUS.first?.id)!)
        }
        
        requestHistory()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! DetailCostRaceViewController
        destination.idRace = sender as! Int
    }
    
    //buttons actions
    @IBAction func searchTapped(_ sender: Any) {
        
        if self.booleanSearch {
            openView(validate: self.booleanSearch, constraint: self.heightSearchConstraint, number: 0, time: 0.4)
        }else{
            openView(validate: self.booleanSearch, constraint: self.heightSearchConstraint, number: 120, time: 0.4)
        }
        self.booleanSearch = !booleanSearch
        
    }
    
    @IBAction func findButtonTapped(_ sender: Any) {
        self.arraySearch = [PaymentHistory]()
        if (self.textFieldFromDate.text?.isEmpty)! && (self.textFieldToDate.text?.isEmpty)!{
            
            self.newSearch.setBeginDate(date: "")
            self.newSearch.setEndDate(date: "")
            self.newSearch.setSelfQuantity(quantity: 0)
            
            self.requestHistory()
            
        }else{
            
            if !(self.textFieldFromDate.text?.isEmpty)! && !(self.textFieldToDate.text?.isEmpty)!{
                
                if self.validateDates(dataInicial: self.textFieldFromDate.text!, dataFinal: self.textFieldToDate.text!){
                    
                    
                    self.newSearch.setSelfQuantity(quantity: 0)
                    
                    self.requestHistory()
                    
                }else{
                    avisoToast("A data inicial não pode ser maior que a data final.", posicao: 3, altura: 45, tipo: 3)
                }
                
            }else{
                
                self.newSearch.setSelfQuantity(quantity: 0)
                self.arraySearch = [PaymentHistory]()
                self.requestHistory()
                
            }
            
            
        }
        
    }
    
    
    //textfields actions
    @IBAction func textfieldFromToBegin(_ sender: UITextField) {
        
        if sender.tag == 0 {
            
            sender.inputView = datePicker
            datePicker.tag = 0
            datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
            
        }else if sender.tag == 1 {
            
            sender.inputView = datePicker
            datePicker.tag = 1
            datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        }
        
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterText = DateFormatter()
        dateFormatterText.dateFormat = "dd-MM-yyyy"
        
        if sender.tag == 0 {
            
            textFieldFromDate.text = dateFormatterText.string(from: sender.date)
            
        }else if sender.tag == 1 {
            
            textFieldToDate.text = dateFormatterText.string(from: sender.date)
            
        }
        
        
        
        if let date = dateFormatterGet.date(from: dateFormatterGet.string(from: sender.date)){
            
            if sender.tag == 0 {
                self.newSearch.setBeginDate(date: dateFormatterPrint.string(from: date))
            }else if sender.tag == 1{
                self.newSearch.setEndDate(date: dateFormatterPrint.string(from: date))
            }
            
        }else{
            print("deu ruim ")
        }
        
    }
    
    // MARK: Validate dates
    func validateDates(dataInicial: String, dataFinal: String) -> Bool {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        let initialDate = formatter.date(from: dataInicial)
        let endDate = formatter.date(from: dataFinal)
        
        if endDate?.compare(initialDate!) == .orderedDescending || endDate?.compare(initialDate!) == .orderedSame {
            return true
        } else {
            return false
        }
        
    }
    
}

extension PaymentViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return STATUS.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return STATUS[row].name
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.textFieldStatus.text = STATUS[row].name
        self.newSearch.setStatus(status: STATUS[row].id)
    }
    
    
}

extension PaymentViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arraySearch.count < 1 {
            let label = UILabel()
            label.text = "Não foram encontrados dados"
            label.textAlignment = .center
            self.paymentTable.backgroundView = label
        }else{
            self.paymentTable.backgroundView = nil
        }
        
        return arraySearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.paymentTable.dequeueReusableCell(withIdentifier: "cell") as! PaymentTableViewCell
        
        cell.raceView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        cell.raceView.layer.cornerRadius = 16
        cell.raceView.layer.shadowColor = UIColor.black.cgColor
        cell.raceView.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.raceView.layer.shadowOpacity = 1.0
        cell.raceView.layer.shadowRadius = 1
        cell.raceDate.adjustsFontSizeToFitWidth = true
        
        cell.raceId.text = String(describing: self.arraySearch[indexPath.row].getID())
        cell.raceDate.text = self.arraySearch[indexPath.row].getBeginDate()
        cell.raceValue.text = self.arraySearch[indexPath.row].getValue()
        cell.raceCustomerName.text = self.arraySearch[indexPath.row].getName()
        
        if self.arraySearch[indexPath.row].getStatus() == 1 {
            cell.raceStatus.textColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
            cell.raceStatus.text = "Pagamento já efetuado"
        }else{
            cell.raceStatus.textColor = #colorLiteral(red: 0.9620884127, green: 0.1928599775, blue: 0.1957365274, alpha: 1)
            cell.raceStatus.text = "Pagamento pendente"
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: SEGUE_TO_WEBVIEW_PAYMENT, sender: self.arraySearch[indexPath.row].getID())
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        if indexPath.row == self.arraySearch.count - 1 && self.arraySearch.count >= self.newSearch.getNextQuantity(){
//
//
//            requestHistory()
//            print("arrived here with indexPath = \(indexPath.row) e o arraySearch =\(arraySearch.count - 1)")
//        }
        
        if indexPath.row == self.arraySearch.count - 1 && self.arraySearch.count >= self.newSearch.getNextQuantity() {
            
            if self.booleanRequestData == false{
                requestHistory()
                 print("arrived here with indexPath = \(indexPath.row) e o arraySearch =\(arraySearch.count - 1)")
            }
            
        }
        
    }
    
}
