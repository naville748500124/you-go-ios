//
//  PersonalDataViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import JMMaskTextField_Swift
import CryptoSwift

//MARK: TODO: colocar as mascaras
//MARK: TODO: deixar senha com 8 caracteres

class PersonalDataViewController: UIViewController {
    
    
    //Elements of screen
    @IBOutlet weak var textFieldName: TextFieldCustom!
    @IBOutlet weak var textFieldGender: TextFieldCustom!
    @IBOutlet weak var textFieldRG: JMMaskTextField!
    @IBOutlet weak var textFieldCPF: TextFieldCustom!
    @IBOutlet weak var textFieldEmail: TextFieldCustom!
    @IBOutlet weak var textFieldConfirmEmail: TextFieldCustom!
    @IBOutlet weak var textFieldPhone: TextFieldCustom!
    @IBOutlet weak var textFieldCellPhone: TextFieldCustom!
    @IBOutlet weak var textFieldZipCode: TextFieldCustom!
    @IBOutlet weak var textFieldAddress: TextFieldCustom!
    @IBOutlet weak var textFieldNeighborhood: TextFieldCustom!
    @IBOutlet weak var textFieldCity: TextFieldCustom!
    @IBOutlet weak var textFieldState: TextFieldCustom!
    @IBOutlet weak var textFieldComplement: TextFieldCustom!
    @IBOutlet weak var textFieldNumber: TextFieldCustom!
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate  //This variable is to share the server path
    var functionController = 0
    var pickerGender = UIPickerView()
    var pickerState  = UIPickerView()
    var personalData = PersonalData()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //configuration textfilds
        self.textFieldZipCode.delegate = self
        self.textFieldZipCode.tag = 1
        
        
        //configuration of pickers view
        self.pickerState.delegate = self
        self.pickerState.dataSource = self
        self.pickerGender.delegate = self
        self.pickerGender.dataSource = self
        self.pickerState.tag = 2
        self.pickerGender.tag = 1
        self.textFieldState.inputView = self.pickerState
        self.textFieldGender.inputView = self.pickerGender
        
        //recovered webService data
        self.recoveredPersonalData()
        
    }
    
    //Button actions
    @IBAction func updateButtonTapped(_ sender: Any) {
        
        if self.validateFields() == "" {
            
            if self.textFieldEmail.text == self.textFieldConfirmEmail.text {
                
                if (self.textFieldCPF.text?.isValidoCPF)! {
                        
                        if (self.textFieldEmail.text?.isValidoEmail())! {
                            
                            let alert = UIAlertController(title: "Aviso", message: "Você confirma a alteração dos dados pessoais?", preferredStyle: .alert)
                            let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
                            let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
                                
                                self.recoveryFieldsData()
                                self.updateDriver()
                            }
                            alert.addAction(no)
                            alert.addAction(yes)
                            self.present(alert, animated: true, completion: nil)
                            
                        }else{
                            avisoToast("O endereço de \"E-MAIL\" digitado não é válido.", posicao: 3, altura: 45, tipo: 4)
                        }
                        
                    }else{
                        avisoToast("O \"CPF\" digitado não é válido.", posicao: 3, altura: 45, tipo: 4)
                    }
                
            }else{
                avisoToast("Os \"E-MAILS\" digitados precisam ser iguais.", posicao: 3, altura: 45, tipo: 4)
            }
            
        }else{
            avisoToast("O campo: \"\(self.validateFields())\", está vazio.", posicao: 3, altura: 45, tipo: 4)
        }
        
    }
    
    
    
    //actions
    func validateFields() -> String{
        
        if textFieldName.text == "" {
            return "Nome"
        }else if textFieldGender.text == "" {
            return "Sexo"
        }else if textFieldRG.text == "" {
            return "RG"
        }else if textFieldCPF.text == "" {
            return "CPF"
        }else if textFieldEmail.text == "" {
            return "E-mail"
        }else if textFieldPhone.text == "" {
            return "Telefone"
        }else if textFieldCellPhone.text == "" {
            return "Celular"
        }else if textFieldZipCode.text == "" {
            return "CEP"
        }else if textFieldAddress.text == "" {
            return "Logradouro"
        }else if textFieldNeighborhood.text == "" {
            return "Bairro"
        }else if textFieldCity.text == "" {
            return "Cidade"
        }else if textFieldState.text == "" {
            return "UF"
        }else if textFieldNumber.text == "" {
            return "Número"
        }else if textFieldConfirmEmail.text == "" {
            return "Confirmar e-mail"
        }
        
        return ""
    }
    
    
    func recoveryFieldsData(){
        
        self.personalData.setDriverName(name: self.textFieldName.text!)
        self.personalData.setRG(rg: self.textFieldRG.text!)
        self.personalData.setCPF(cpf: self.textFieldCPF.text!)
        self.personalData.setEmail(email: self.textFieldEmail.text!)
        self.personalData.setPhone(phone: self.textFieldPhone.text!)
        self.personalData.setcellPhone(cellPhone: self.textFieldCellPhone.text!)
        self.personalData.setZipCode(zipCode: self.textFieldZipCode.text!)
        self.personalData.setAddress(address: self.textFieldAddress.text!)
        self.personalData.setResidenceNumber(residenceNumber: self.textFieldNumber.text!)
        self.personalData.setComplement(complement: self.textFieldComplement.text)
        self.personalData.setNeighborhood(neighborhood: self.textFieldNeighborhood.text!)
        self.personalData.setCity(city: self.textFieldCity.text!)
        
    }
    
    func recoveredPersonalData(){
        
        if !self.personalData.getDriverName().isEmpty {
            
        //}else{
            
            let maskCPF         = JMStringMask(mask: "000.000.000-00")
            let maskZipCode     = JMStringMask(mask: "00000-000")
            let maskPhone       = JMStringMask(mask: "(00) 0000-0000")
            let maskCellPhone   = JMStringMask(mask: "(00) 0 0000-0000")
            let maskRG1         = JMStringMask(mask: "00.000.000-0")
            let maskRG2         = JMStringMask(mask: "00.000.000-A")
            
            if self.personalData.getRG().contains("x"){
                self.textFieldRG.text = maskRG2.mask(string: self.personalData.getRG())
            }else{
                self.textFieldRG.text = maskRG1.mask(string: self.personalData.getRG())
            }
            
            self.textFieldName.text         = self.personalData.getDriverName()
            self.textFieldCPF.text          = maskCPF.mask(string: self.personalData.getCPF())
            self.textFieldEmail.text        = self.personalData.getEmail()
            self.textFieldConfirmEmail.text = self.personalData.getEmail()
            self.textFieldPhone.text        = maskPhone.mask(string: self.personalData.getPhone())
            self.textFieldCellPhone.text    = maskCellPhone.mask(string: self.personalData.getcellPhone())
            self.textFieldZipCode.text      = maskZipCode.mask(string: self.personalData.getZipCode())
            self.textFieldAddress.text      = self.personalData.getAddress()
            self.textFieldNumber.text       = self.personalData.getResidenceNumber()
            self.textFieldComplement.text   = self.personalData.getComplement()
            self.textFieldNeighborhood.text = self.personalData.getNeighborhood()
            self.textFieldCity.text         = self.personalData.getCity()
            
            if GENDER.contains(where: {$0.id == self.personalData.getGender()}){
                for i in GENDER{
                    if i.id == self.personalData.getGender(){
                        self.textFieldGender.text = i.name
                    }
                }
            }
            
            if STATES.contains(where: {$0.id == self.personalData.getIdState()}){
                for i in STATES{
                    if i.id == self.personalData.getIdState(){
                        self.textFieldState.text = i.name
                    }
                }
            }
        }
    }
    
    func postFields() -> String {
        
        let post = "tipo=1&nome_usuario=\(self.personalData.getDriverName())&email_usuario=\(self.personalData.getEmail())&telefone_usuario=\(self.personalData.getPhone())&rg_usuario=\(self.personalData.getRG())&cpf_usuario=\(self.personalData.getCPF())&celular_usuario=\(self.personalData.getcellPhone())&logradouro_usuario=\(self.personalData.getAddress())&cidade_usuario=\(self.personalData.getCity())&fk_uf_usuario=\(self.personalData.getIdState())&bairro_usuario=\(self.personalData.getNeighborhood())&cep_usuario=\(self.personalData.getZipCode())&complemento_usuario=\(self.personalData.getComplement()!)&num_residencia_usuario=\(self.personalData.getResidenceNumber())&fk_genero=\(self.personalData.getGender())"
        
        return post
    }
    
    
    @IBAction func rgChangeValue(_ sender: Any) {
        
        let mask  = JMStringMask(mask: "00.000.000-0")
        let mask2 = JMStringMask(mask: "00.000.000-A")
        var text = self.textFieldRG.text?.replacingOccurrences(of: ".", with: "")
        text = text?.replacingOccurrences(of: "-", with: "")
        
        if (text?.count)! < 8  {
            self.textFieldRG.keyboardType = UIKeyboardType.numberPad
            self.textFieldRG.reloadInputViews()
        }else {
            textFieldRG.keyboardType = .default
            self.textFieldRG.reloadInputViews()
        }
        
        if (text?.isEmpty)! || ((text?.contains("X"))! || (text?.contains("x"))!){
            
            if (text?.count)! < 10 {
                let maskedText = mask2.mask(string: text)
                self.textFieldRG.text = maskedText
            }else{
                text?.removeLast()
                let maskedText = mask2.mask(string: text)
                self.textFieldRG.text = maskedText
            }
        }else{
            if (text?.count)! < 10 {
                text = text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                
                let maskedText = mask.mask(string: text)
                self.textFieldRG.text = maskedText
            }else{
                text?.removeLast()
                let maskedText = mask.mask(string: text)
                self.textFieldRG.text = maskedText
            }
            
        }
        
    }
    
}

//textfield delegates and functions
extension PersonalDataViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 {
            self.findZipCode()
        }
    }
    
}



//configuration of pickers
extension PersonalDataViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        if pickerView.tag == 1 {
            return 1
        }else if pickerView.tag == 2{
            return 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 1 {
            return GENDER.count
        }else if pickerView.tag == 2{
            return STATES.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 1 {
            return GENDER[row].name
        }else if pickerView.tag == 2{
            return STATES[row].name
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            self.textFieldGender.text = GENDER[row].name
            self.personalData.setGender(idGender: GENDER[row].id)
        }else if pickerView.tag == 2 {
            self.textFieldState.text = STATES[row].name
            self.personalData.setIdState(idState: STATES[row].id)
        }
        
    }
    
}

