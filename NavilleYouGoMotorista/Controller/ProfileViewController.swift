//
//  ProfileViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    //objects
    var personalData = PersonalData()
    var vehicleData  = VehicleData()
    var photoData    = PhotosData()
    var cities       = [GenericField]()
    var brands       = [GenericField]()
    
    //variables and constants
    var shared = UIApplication.shared.delegate as! AppDelegate
    var validateBankData = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.recoveryAllData()
        
        if self.validateBankData == 2 {
            performSegue(withIdentifier: SEGUE_TO_BANK_DATA, sender: nil)
        }
        
    }

    //buttons actions
    @IBAction func personalDataButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_PERSONAL_DATA, sender: nil)
    }
    @IBAction func passwordButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_PASSWORD, sender: nil)
    }
    
    @IBAction func vehicleDataButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_VEHICLE_DATA, sender: nil)
    }
    
    @IBAction func photosDataButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_PHOTO_DATA, sender: nil)
    }
    
    @IBAction func bankDataButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_BANK_DATA, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == SEGUE_TO_PERSONAL_DATA {
            
            let destination = segue.destination as! PersonalDataViewController
            destination.personalData = self.personalData
            
        }else if segue.identifier == SEGUE_TO_VEHICLE_DATA {
            
            let destination = segue.destination as! VehicleDataViewController
            destination.vehicleData = self.vehicleData
            destination.brandsArray = self.brands
            destination.citiesArray = self.cities
            
        }else if segue.identifier == SEGUE_TO_BANK_DATA && self.validateBankData == 2 {
            
            let destination = segue.destination as! BankDataViewController
            destination.validateBankData = self.validateBankData
            
        }
        
    }
    
}
