//
//  HomeViewController.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 22/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FBSDKLoginKit
import AudioToolbox

class HomeViewController: UIViewController {
    
    //Elements of screen
    //Map
    @IBOutlet weak var map: MKMapView!
    
    //menu
    @IBOutlet weak var leadingConstraintMenu: NSLayoutConstraint!
    @IBOutlet weak var tableMenu: UITableView!
    @IBOutlet weak var viewBlackout: UIView!
    @IBOutlet weak var imageMenu: ImageCustom!
    @IBOutlet weak var buttonOnlineOffiline: ButtonCustom!
    @IBOutlet weak var labelDriverName: UILabel!
    
    //Variables and constants
    var booleanMenu = true
    var dataMenu = [MenuData]()
    var booleanOnOffline = true
    var shared = UIApplication.shared.delegate as! AppDelegate
    var functionController = 0
    var validateBankData = 0
    var newRace = Race()
    var acceptOrRefuseRace = 0
    let buttonGo = UIButton()
    let buttonAccept = UIButton()
    let buttonCancel = UIButton()
    var booleanProgressRace = false
    var justifield = ""
    var viewDetailExist = false
    
    //variables from cllocation
    internal var locationManager: CLLocationManager!
    internal var currentLocation: CLLocation?
    var userLatitude  = ""
    var userLongitude = ""
    var timer: DispatchSourceTimer?
    var timerButton = Timer()
    var seconds = 0
    
    //viewDetails variables
    var booleanViewDetails = false
    var heightConstraintViewDetails: NSLayoutConstraint?
    let buttonMinimize = UIButton()
    let cancelButton = UIButton()
    
    var viewRace = UIView()
    var viewEvaluation = UIView()
    var viewDetail = UIView()
    
    //variables viewJustifield
    var viewJustifield = UIView()
    var heightConstraintViewJustifield: NSLayoutConstraint?
    var booleanJustifield = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(self.shared.lastRace, forKey: "UserIDRace")
        
        //verify if have permission to use maps
        if CLLocationManager.authorizationStatus().rawValue == 0 || CLLocationManager.authorizationStatus().rawValue == 2 {
            
            let alert   = UIAlertController(title: "Aviso", message: "Para visualizar os locais de risco, você precisa permitir o acesso sua localização em Ajustes -> YouGo Cliente", preferredStyle: .alert)
            let fechar  = UIAlertAction(title: "Fechar", style: .destructive , handler: { (acao) in
                self.dismiss(animated: true, completion: nil)
            })
            let ir      = UIAlertAction(title: "Ir", style: .default, handler: { (acao) in
                
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    self.dismiss(animated: true, completion: nil)
                    UIApplication.shared.open(appSettings, options: [:], completionHandler: nil)
                }
            })
            alert.addAction(ir)
            alert.addAction(fechar)
            self.present(alert, animated: true, completion: nil)
            
        }else{
        
            //print idRacce on userDefault
            if let idRaceSaved = UserDefaults.standard.string(forKey: "UserIDRace"){
                if idRaceSaved == "0" || idRaceSaved == "" {
                    print("\n\ndo not have race\n\n")
                }else{
                    print("\n\nit have a race\n\n")
                    self.followRace(idRace: Int(idRaceSaved)!)
                }
            }
        }
        
        //logo on navigation
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let logo = UIImage(named: "camada9Copiar.png")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        //configuration button online/offline
        if booleanOnOffline{
            self.buttonOnlineOffiline.setTitle("Status: Offline", for: .normal)
            self.buttonOnlineOffiline.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }else{
            self.buttonOnlineOffiline.setTitle("Status: Online", for: .normal)
            self.buttonOnlineOffiline.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        }
        
        //configuration map
        self.map.delegate = self
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        //asking permission to use userlocation
        if CLLocationManager.locationServicesEnabled(){
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.allowsBackgroundLocationUpdates = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //tap to close menu
        let tapCloseMenu = UITapGestureRecognizer(target: self, action: #selector(closeMenu))
        tapCloseMenu.numberOfTapsRequired = 1
        
        //configuration of menu
        self.labelDriverName.text = self.shared.userName
        self.viewBlackout.addGestureRecognizer(tapCloseMenu)
        self.leadingConstraintMenu.constant = -240
        self.tableMenu.delegate = self
        self.tableMenu.dataSource = self
        self.tableMenu.isScrollEnabled = false
        self.viewBlackout.isHidden = true
        self.imageMenu.isUserInteractionEnabled = true
        dataMenu = [MenuData(imageMenu: #imageLiteral(resourceName: "ic_perm_identity"), nameMenu: "Editar Perfil"), MenuData(imageMenu: #imageLiteral(resourceName: "ic_directions_car") ,nameMenu: "Corridas realizadas"), MenuData(imageMenu: #imageLiteral(resourceName: "ic_attach_money"), nameMenu: "Pagamentos"), MenuData(imageMenu: #imageLiteral(resourceName: "ic_star_half"), nameMenu: "Avaliações recebidas"), MenuData(imageMenu: #imageLiteral(resourceName: "ic_live_help"), nameMenu: "Ajuda")]
        
        if shared.userID != "" {
            
            guard let url = URL(string: self.shared.serverImageShared + "motorista_" + shared.userID + "/motorista.png") else { return }
            print("IMAGE URL: ", url)
            print("USER ID: ", shared.userID)
            
            DispatchQueue.main.async {
                self.imageMenu.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    @IBAction func myLocationPressed(_ sender: UIButton) {
        guard let location = locationManager.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(location, 500, 500)
        map.setRegion(region, animated: true)
    }
    
    //Buttons action
    @IBAction func menuButton(_ sender: Any) {
        
        self.closeMenu()
    }
    
    @IBAction func logOutButton(_ sender: Any) {
        
        if self.newRace.getIDRace() != 0 {
            let alert = UIAlertController(title: "Aviso", message: "Você não pode ficar offline com uma corrida em andamento.", preferredStyle: .alert)
            let ok    = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }else{
            
            self.closeMenu()
            let alert = UIAlertController(title: "Aviso", message: "Você realmente deseja sair?", preferredStyle: .actionSheet)
            let no    = UIAlertAction(title: "Não", style: .cancel, handler: nil)
            let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in
                
                self.stopTimer()
                self.shared.userID          = ""
                self.shared.userName        = ""
                self.shared.userEmail       = ""
                self.shared.userPassword    = ""
                self.shared.bankID          = ""
                self.shared.agencyDriver    = ""
                self.shared.accountDriver   = ""
                self.shared.digitAccount    = ""
                
                UserDefaults.standard.set(nil, forKey: "UserDefaultEmail")
                UserDefaults.standard.set(nil, forKey: "UserDefaultPassword")
                UIApplication.shared.unregisterForRemoteNotifications()
                
                let facebookManager = FBSDKLoginManager()
                facebookManager.logOut()
                self.dismiss(animated: true, completion: nil)
                
            }
            alert.addAction(yes)
            alert.addAction(no)
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
    }
    
    @IBAction func onlineOfflineButton(_ sender: Any) {
        
        if !self.shared.bankID.isEmpty || !self.shared.agencyDriver.isEmpty || !self.shared.accountDriver.isEmpty || !self.shared.digitAccount.isEmpty{
            
            if self.booleanOnOffline {
                onlineOffline()
                
            } else {
                let alert = UIAlertController(title: "Aviso", message: "Você não receberá corridas enquanto estiver Offline.", preferredStyle: .alert)
                let no    = UIAlertAction(title: "Voltar", style: .default, handler: nil)
                let yes   = UIAlertAction(title: "OK", style: .destructive) { (action) in
                    self.onlineOffline()
                }
                alert.addAction(no)
                alert.addAction(yes)
                self.present(alert, animated: true, completion: nil)
            }
            
            
        } else {
            self.validateBankData = 2
            self.performSegue(withIdentifier: SEGUE_TO_PROFILE, sender: nil)
        }
        
    }
    
    
    //actions
    
    
    @objc func closeViewJustifield(sender: UIButton){
        
        self.viewJustifield.removeFromSuperview()
        for subview in self.self.viewJustifield.subviews{
            if subview.isDescendant(of: viewJustifield){
                subview.removeFromSuperview()
            }
        }
        
        if sender.tag == 1 {
            if (self.justifield.isEmpty){
                
                let alert = UIAlertController(title: "Aviso", message: "Sua corrida não pode ser cancelada, porque você não deu justificativa", preferredStyle: .alert)
                let ok    = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.visibleJustifield()
                    self.createViewJustifield()
                })
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                
            }else{
                self.visibleJustifield()
                self.cancelOneRace(justifield: self.justifield)
                //self.requestCancelRace()
            }
        }else{
            self.visibleJustifield()
        }
    }
    
    
    @objc func closeMenu(){
        openView(validate: booleanMenu, constraint: leadingConstraintMenu, number: -240, time: 0.3)
        if booleanMenu {
            self.viewBlackout.isHidden = false
        }else{
            self.viewBlackout.isHidden = true
        }
        self.booleanMenu = !booleanMenu
    }
    
    func vibratePhone(){
        AudioServicesPlaySystemSound(1304)
    }
    
    @objc func cancelRace(){
        
        self.visibleDetailView()
        
        if let validate: [GenericField] = self.newRace.getStatusRace() as? [GenericField] {

            if validate.first?.id == 85 {

                let alert = UIAlertController(title: "Aviso", message: "Você não pode cancelar uma corrida que já foi inicializada, só pode finalizar a corrida.", preferredStyle: .alert)
                let ok    = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)

            }else{

                let alert1 = UIAlertController(title: "Aviso", message: "Você realmente deseja cancelar está corrida?", preferredStyle: .alert)
                let no    = UIAlertAction(title: "Não", style: .default, handler: nil)
                let yes   = UIAlertAction(title: "Sim", style: .destructive) { (action) in

                    self.createViewJustifield()
   
                }

                alert1.addAction(no)
                alert1.addAction(yes)
                self.present(alert1, animated: true, completion: nil)

            }

        }
        
    }
    
    @objc func callCustomer(){
        self.makeCall()
    }
    
    @objc func chatCustomer(){
        self.sendMessageChat()
    }
    
    @objc func beginRace(){
        
        if self.newRace.getStatusRace().first?.id == 85 {
            
            let alert = UIAlertController(title: "Aviso", message: "O que você deseja fazer?", preferredStyle: .actionSheet)
            let close = UIAlertAction(title: "Fechar", style: .cancel , handler: nil)
            
            let route = UIAlertAction(title: "Abrir GPS", style: .default , handler: { (action) in
                self.chooseNavigation(originOrDestination: false)
            })
            
            let finalize = UIAlertAction(title: "Finalizar corrida", style: .destructive , handler: { (action) in
                self.finishRace()
            })
            
            alert.addAction(finalize)
            alert.addAction(route)
            alert.addAction(close)
            
            self.present(alert, animated: true, completion: nil)
            
        }else if newRace.getStatusRace().first?.id == 84{
            
            let alert = UIAlertController(title: "Aviso", message: "O passageiro já esta no veículo?", preferredStyle: .alert)
            let yes   = UIAlertAction(title: "Sim", style: .default) { (action) in
                self.startNewRace()
            }
            let no    = UIAlertAction(title: "Não", style: .default) { (action) in
                self.chooseNavigation(originOrDestination: true)
            }
            
            alert.addAction(yes)
            alert.addAction(no)
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    internal func chooseNavigation(originOrDestination: Bool){
        
        //openGoogleMaps()
        //wazeMaps()
        
        let alert = UIAlertController(title: "Tipo de navegação", message: "Escolha um tipo de navegação abaixo.", preferredStyle: .actionSheet )
        let waze = UIAlertAction(title: "Waze", style: .default) { (action) in
            
            if originOrDestination {
                self.openWazeMaps(latitude: self.newRace.getOriginLatitude(), longitude: self.newRace.getOriginLongitude())
            }else{
                self.openWazeMaps(latitude: self.newRace.getDestinationLatitude(), longitude: self.newRace.getDestinationLongitude())
            }
            
        }
        
        let googleMaps = UIAlertAction(title: "Google Maps", style: .default) { (action) in
            
            if originOrDestination{
                self.openGoogleMaps(latitude: self.newRace.getOriginLatitude(), longitude: self.newRace.getOriginLongitude())
            }else{
                self.openGoogleMaps(latitude: self.newRace.getDestinationLatitude(), longitude: self.newRace.getDestinationLongitude())
            }
            
        }
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel , handler: nil)
        alert.addAction(waze)
        alert.addAction(googleMaps)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openWazeMaps(latitude: Double, longitude: Double)
    {
        let openUrl = URL(string: "waze://?ll=\(latitude),\(longitude)&navigate=yes")!
        UIApplication.shared.open(openUrl , options:[:]) { (success) in
            if !success
            {
                
            }
        }
    }
    
    func openGoogleMaps(latitude:Double, longitude: Double) {
        //Working in Swift new versions.
        
        let openUrl = URL(string: "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")!
        UIApplication.shared.open(openUrl , options:[:]) { (success) in
            if !success
            {
                
            }
        }
    }
    
    func makeCall()  {
        
        let url: NSURL = URL(string: "TEL://\(self.newRace.getPhone())")! as NSURL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func sendMessageChat(){
        
        let urlWhats = "whatsapp://send?phone=+55\(self.newRace.getPhone())&abid=12354&text=Olá \(self.newRace.getName()), tudo bem?"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                } else {
                    print("Install Whatsapp")
                }
            }
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func reffuseRace(){
        
        if self.newRace.getIDRace() != 0 {
            self.acceptOrRefuseRace = IDREFUSE
            self.acceptOneRace()
        }
        
    }
    
    @objc func acceptRace(){
        
        self.acceptOrRefuseRace = ID_ACCEPT_RACE
        self.acceptOneRace()
    }
    
    @objc func closeEvaluation(){
        self.buttonOnlineOffiline.isHidden = false
        self.startTimer()
        viewEvaluation.removeFromSuperview()
        for subview in self.viewEvaluation.subviews {
            if subview.isDescendant(of: viewEvaluation){
                subview.removeFromSuperview()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier ==  SEGUE_TO_PROFILE{
            let destination = segue.destination as! ProfileViewController
            destination.validateBankData = self.validateBankData
        }
        
        if segue.identifier == SEGUE_TO_MAKE_PAYMENT{
            let destination = segue.destination as! MakePaymentViewController
            destination.idRace = sender as! Int
        }
        
    }
    
}



//configuration of menu with table
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableMenu.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
        cell.label.text = dataMenu[indexPath.row].nameMenu
        cell.imageMenu.image  = dataMenu[indexPath.row].imageMenu
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        closeMenu()
        switch indexPath.row {
        case 0:
            self.validateBankData = 1
            performSegue(withIdentifier: SEGUE_TO_PROFILE, sender: nil)
        case 1:
            performSegue(withIdentifier: SEGUE_TO_PERFORMED_RACES, sender: nil)
        case 2:
            performSegue(withIdentifier: SEGUE_TO_PAYMENT, sender: nil)
        case 3:
            performSegue(withIdentifier: SEGUE_TO_EVALUATIONS_RECEIVED, sender: nil)
        case 4:
            performSegue(withIdentifier: SEGUE_TO_HELP, sender: nil)
        default:
            break
        }
        
    }
    
}

//configuring Userlocation
extension HomeViewController: CLLocationManagerDelegate, MKMapViewDelegate{
    
    func mapZoomUserLocation(){
        
        let viewRegion = MKCoordinateRegionMakeWithDistance((currentLocation?.coordinate)!, 500, 500)
        self.map.setRegion(viewRegion, animated: true)
        
        self.userLatitude  = String(describing: currentLocation!.coordinate.latitude)
        self.userLongitude = String(describing: currentLocation!.coordinate.longitude)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if currentLocation == nil {
            
            if let userLocation = locations.last{
                if currentLocation != userLocation{
                    let viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 500, 500)
                    self.map.setRegion(viewRegion, animated: true)
                    
                    self.userLatitude  = String(describing: userLocation.coordinate.latitude)
                    self.userLongitude = String(describing: userLocation.coordinate.longitude)
                    currentLocation = userLocation
                    
                }
                
            }
            
        }else{
            
            if let userLocation = locations.last{
                if currentLocation != userLocation{
                    
                    
                    self.userLatitude  = String(describing: userLocation.coordinate.latitude)
                    self.userLongitude = String(describing: userLocation.coordinate.longitude)
                    currentLocation = userLocation
                    
                }
                
            }
            
        }
        
    }
    
    
    
    func startTimer() {
        let queue = DispatchQueue(label: "com.domain.app.timer")  // you can also use `DispatchQueue.main`, if you want
        timer = DispatchSource.makeTimerSource(queue: queue)
        timer!.schedule(deadline: .now(), repeating: .seconds(4))
        timer!.setEventHandler { [weak self] in
            
            self?.sendMyLocation()
        }
        timer!.resume()
    }
    
    func stopTimer() {
        timer?.cancel()
        timer = nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.black
        renderer.lineWidth = 4.0
        
        return renderer
        
    }
    
    func routeCustomer(timer: Int) {
        
        let sourceLocation = CLLocationCoordinate2D(latitude: Double(self.userLatitude)!, longitude: Double(self.userLongitude)!)
        let destinationLocation = CLLocationCoordinate2D(latitude: self.newRace.getOriginLatitude(), longitude: self.newRace.getOriginLongitude())
        
        let sourcePin = CustomPim(pinTitle: "Você", pinSubtitle: "", location: sourceLocation)
        let destinationPin = CustomPim(pinTitle: "Cliente", pinSubtitle: "", location: destinationLocation)
        self.map.addAnnotation(sourcePin)
        self.map.addAnnotation(destinationPin)
        
        let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
        
        let directionsRequest = MKDirectionsRequest()
        directionsRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionsRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionsRequest.transportType = .automobile
        
        let directions =  MKDirections(request: directionsRequest)
        directions.calculate { (response, error) in
            
            guard let directionResponse = response else{
                if let error = error{
                    print("we have error getting directions =\(error.localizedDescription)")
                }
                
                return
            }
            
            let route = directionResponse.routes[0]
            self.map.add(route.polyline, level: .aboveRoads)
            
            if let first = self.map.overlays.first {
                
                if timer == -1{
                    if let first = self.map.overlays.first {
                        let rect = self.map.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                        self.map.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 100.0, left: 40.0, bottom: 110.0, right: 40.0), animated: true)
                        
                        self.visibleDetailView()
                    }
                }else{
                    let rect = self.map.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                    self.map.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 150.0, left: 130.0, bottom: 130.0, right: 130.0), animated: true)
                    self.createView(timer: timer)
                }
                
            }
            
            
            
        }
        
    }
    
    func routeDestination(validate: Int) {
        
        let sourceLocation = CLLocationCoordinate2D(latitude: self.newRace.getOriginLatitude(), longitude: self.newRace.getOriginLongitude())
        let destinationLocation = CLLocationCoordinate2D(latitude: self.newRace.getDestinationLatitude(), longitude: self.newRace.getDestinationLongitude())
        
        let sourcePin = CustomPim(pinTitle: "Você", pinSubtitle: "", location: sourceLocation)
        let destinationPin = CustomPim(pinTitle: "Destino", pinSubtitle: "", location: destinationLocation)
        self.map.addAnnotation(sourcePin)
        self.map.addAnnotation(destinationPin)
        
        let sourcePlaceMark = MKPlacemark(coordinate: sourceLocation)
        let destinationPlaceMark = MKPlacemark(coordinate: destinationLocation)
        
        let directionsRequest = MKDirectionsRequest()
        directionsRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionsRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionsRequest.transportType = .automobile
        
        let directions =  MKDirections(request: directionsRequest)
        directions.calculate { (response, error) in
            
            guard let directionResponse = response else{
                if let error = error{
                    print("we have error getting directions =\(error.localizedDescription)")
                }
                
                return
            }
            
            let route = directionResponse.routes[0]
            self.map.add(route.polyline, level: .aboveRoads)
            
            if let first = self.map.overlays.first {
                let rect = self.map.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                self.map.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0), animated: true)
                
            }
            
            if validate == 1 {
                self.buttonGo.setTitle("Finalizar corrida", for: .normal)
                self.buttonGo.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
                self.visibleDetailView()
            }
        }
        
    }
    
}

class CustomPim: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    
    init(pinTitle: String, pinSubtitle: String, location: CLLocationCoordinate2D) {
        
        self.title = pinTitle
        self.subtitle = pinSubtitle
        self.coordinate = location
        
    }
    
}
