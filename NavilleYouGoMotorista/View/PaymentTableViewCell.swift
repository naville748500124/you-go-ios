//
//  PaymentTableViewCell.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 23/04/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class PaymentTableViewCell: UITableViewCell {
    
    //Elements of screen
    @IBOutlet weak var raceView: UIView!
    @IBOutlet weak var raceDate: UILabel!
    @IBOutlet weak var raceValue: UILabel!
    @IBOutlet weak var raceCustomerName: UILabel!
    @IBOutlet weak var raceId: UILabel!
    @IBOutlet weak var raceStatus: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
