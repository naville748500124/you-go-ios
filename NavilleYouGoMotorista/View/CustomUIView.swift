//
//  CustomUIView.swift
//  YouGoMotorista
//
//  Created by Junior Silva on 17/09/18.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomUIView: UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}
