//
//  custonsViews.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 12/03/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Cosmos

//create views
extension HomeViewController{
    
    @objc func updateTime(){
        
        self.seconds -= 1
        
        if seconds <= 0 {
            self.reffuseRace()
        }else{
            self.buttonCancel.setTitle("\(seconds)  Recusar", for: .normal)
        }
        
    }
    
    func createView(timer: Int){
        
        
        self.seconds = timer
        timerButton = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
        self.vibratePhone()
        self.buttonOnlineOffiline.isHidden = true
        
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "background.png")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        viewRace.frame = self.map.frame
        viewRace.insertSubview(backgroundImage, at: 0)
        
        //assume you work in UIViewcontroller
        self.map.addSubview(viewRace)
        let maskLayer = CALayer()
        maskLayer.frame = viewRace.bounds
        let circleLayer = CAShapeLayer()
        
        
        //assume the circle's radius is 150
        circleLayer.frame = CGRect(x:0 , y:0,width: viewRace.frame.size.width,height: viewRace.frame.size.height)
        let finalPath = UIBezierPath(roundedRect: CGRect(x:0 , y:0,width: viewRace.frame.size.width,height: viewRace.frame.size.height), cornerRadius: 0)
        let circlePath = UIBezierPath(ovalIn: CGRect(x:viewRace.center.x - 150, y:viewRace.center.y - 150, width: 300, height: 300))
        finalPath.append(circlePath.reversing())
        finalPath.usesEvenOddFillRule = true
        circleLayer.path = finalPath.cgPath
        circleLayer.borderColor = UIColor.white.withAlphaComponent(1).cgColor
        circleLayer.borderWidth = 1
        maskLayer.addSublayer(circleLayer)
        viewRace.layer.mask = maskLayer
        
        
        //create an accept button
        buttonAccept.translatesAutoresizingMaskIntoConstraints = false
        buttonAccept.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        buttonAccept.setTitleColor(.white, for: .normal)
        buttonAccept.setTitle("Aceitar", for: .normal)
        buttonAccept.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        
        //add constraints an accept button
        let horizontalbuttonAccept  = NSLayoutConstraint(item: buttonAccept, attribute: .bottom , relatedBy: .equal, toItem: viewRace, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalbuttonAccept    = NSLayoutConstraint(item: buttonAccept, attribute: .leading, relatedBy: .equal, toItem: viewRace, attribute: .leading, multiplier: 1, constant: 0)
        let widthbuttonAccept       = NSLayoutConstraint(item: buttonAccept, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width / 2)
        let heightbuttonAccept      = NSLayoutConstraint(item: buttonAccept, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewRace.addSubview(buttonAccept)
        viewRace.addConstraints([horizontalbuttonAccept, verticalbuttonAccept, widthbuttonAccept, heightbuttonAccept])
        
        //create a cancel button
        buttonCancel.translatesAutoresizingMaskIntoConstraints = false
        buttonCancel.backgroundColor = UIColor.red
        buttonCancel.setTitle("Recusar", for: .normal)
        buttonCancel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        
        //add constraints on cancel button
        let horizontalbuttonCancel  = NSLayoutConstraint(item: buttonCancel, attribute: .bottom , relatedBy: .equal, toItem: viewRace, attribute: .bottom, multiplier: 1, constant: 0)
        let verticalbuttonCancel    = NSLayoutConstraint(item: buttonCancel, attribute: .trailing , relatedBy: .equal, toItem: viewRace, attribute: .trailing , multiplier: 1, constant: 0)
        let widthbuttonCancel       = NSLayoutConstraint(item: buttonCancel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width / 2)
        let heightbuttonCancel      = NSLayoutConstraint(item: buttonCancel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewRace.addSubview(buttonCancel)
        viewRace.addConstraints([horizontalbuttonCancel, verticalbuttonCancel, widthbuttonCancel, heightbuttonCancel])
        
        //actions of buttonCancel and buttonAccept
        buttonCancel.addTarget(self, action: #selector(reffuseRace), for: .touchUpInside)
        buttonAccept.addTarget(self, action: #selector(acceptRace), for: .touchUpInside)
        
        //create a label with numbers of passagers and bags
        let labelBagsAndPassagers = UILabel()
        labelBagsAndPassagers.text = "Quantidade de malas: \(self.newRace.getBags())\nQuantidade de passageiros: \(self.newRace.getAmount())"
        labelBagsAndPassagers.textColor = UIColor.white
        labelBagsAndPassagers.textAlignment = .center
        labelBagsAndPassagers.numberOfLines = 0
        labelBagsAndPassagers.adjustsFontSizeToFitWidth = true
        labelBagsAndPassagers.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints of label with numbers of passagers and bags
        let horizontallabelBags  = NSLayoutConstraint(item: labelBagsAndPassagers, attribute: .bottom , relatedBy: .equal, toItem: buttonAccept, attribute: .top, multiplier: 1, constant: 0)
        let verticallabelBags    = NSLayoutConstraint(item: labelBagsAndPassagers, attribute: .leading , relatedBy: .equal, toItem: buttonAccept, attribute: .leading , multiplier: 1, constant: 0)
        let widthlabelBags       = NSLayoutConstraint(item: labelBagsAndPassagers, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width)
        let heightlabelBags      = NSLayoutConstraint(item: labelBagsAndPassagers, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewRace.addSubview(labelBagsAndPassagers)
        viewRace.addConstraints([horizontallabelBags, verticallabelBags, widthlabelBags, heightlabelBags])
        
        //variable distance em km
        let coordinateOne = CLLocation(latitude: Double(userLatitude)!, longitude: Double(userLongitude)!)
        let coordinateTwo = CLLocation(latitude: self.newRace.getOriginLatitude(), longitude: self.newRace.getOriginLongitude())
        let distance      = (coordinateOne.distance(from: coordinateTwo) / 1000).rounded()
        
        
        //create a label with destination of customer
        let labelDestination = UILabel()
        labelDestination.text = "\(self.newRace.getOriginAddress())\nDistancia: \(distance)KM"
        labelDestination.textColor = UIColor.white
        labelDestination.textAlignment = .center
        labelDestination.numberOfLines = 0
        labelDestination.adjustsFontSizeToFitWidth = true
        labelDestination.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints of label destination of customer
        let horizontallabelDestination       = NSLayoutConstraint(item: labelDestination, attribute: .top , relatedBy: .equal, toItem: viewRace, attribute: .top, multiplier: 1, constant: 0)
        let verticallabellabelDestination    = NSLayoutConstraint(item: labelDestination, attribute: .leading , relatedBy: .equal, toItem: viewRace, attribute: .leading , multiplier: 1, constant: 0)
        let widthlabellabelDestination       = NSLayoutConstraint(item: labelDestination, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width)
        let heightlabellabelDestination      = NSLayoutConstraint(item: labelDestination, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80)
        viewRace.addSubview(labelDestination)
        viewRace.addConstraints([horizontallabelDestination, verticallabellabelDestination, widthlabellabelDestination, heightlabellabelDestination])
        
        
    }
    
    
    
    func createDetailsView(){
        
        
        //creating and configuration viewDetails
        let viewDetail = UIView()
        viewDetail.backgroundColor = UIColor.black
        viewDetail.translatesAutoresizingMaskIntoConstraints = false
        
        let hDestination       = NSLayoutConstraint(item: viewDetail, attribute: .bottom , relatedBy: .equal, toItem: map, attribute: .bottom, multiplier: 1, constant: 0)
        let vDestination       = NSLayoutConstraint(item: viewDetail, attribute: .trailing , relatedBy: .equal, toItem: map, attribute: .trailing , multiplier: 1, constant: 0)
        let wDestination       = NSLayoutConstraint(item: viewDetail, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width)
        let h2Destination      = NSLayoutConstraint(item: viewDetail, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        self.heightConstraintViewDetails = h2Destination
        map.addSubview(viewDetail)
        map.addConstraints([hDestination, vDestination ,wDestination, h2Destination])
        
        //configuration buttonMinimize
        buttonMinimize.translatesAutoresizingMaskIntoConstraints = false
        buttonMinimize.addTarget(self, action: #selector(visibleDetailView), for: .touchUpInside)
        
        let horizontalButtonArrow = NSLayoutConstraint(item: buttonMinimize, attribute: .top , relatedBy: .equal, toItem: viewDetail, attribute: .top, multiplier: 1, constant: 0)
        let verticalButtonArrow   = NSLayoutConstraint(item: buttonMinimize, attribute: .trailing , relatedBy: .equal, toItem: viewDetail, attribute: .trailing, multiplier: 1, constant: -5)
        let widthButtonArrow      = NSLayoutConstraint(item: buttonMinimize, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        let heightButtonArrow     = NSLayoutConstraint(item: buttonMinimize, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        viewDetail.addSubview(buttonMinimize)
        viewDetail.addConstraints([horizontalButtonArrow, verticalButtonArrow ,widthButtonArrow, heightButtonArrow])
        
        //creating and configuration label name
        let labelCustomerName = UILabel()
        labelCustomerName.text = "Nome: " + self.newRace.getName()
        labelCustomerName.textColor = UIColor.white
        labelCustomerName.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalLabelName = NSLayoutConstraint(item: labelCustomerName, attribute: .top , relatedBy: .equal, toItem: viewDetail, attribute: .top, multiplier: 1, constant: 0)
        let verticalLabelName   = NSLayoutConstraint(item: labelCustomerName, attribute: .leading , relatedBy: .equal, toItem: viewDetail, attribute: .leading, multiplier: 1, constant: 10)
        let widthLabelName     = NSLayoutConstraint(item: labelCustomerName, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
        let heightLabelName     = NSLayoutConstraint(item: labelCustomerName, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        viewDetail.addSubview(labelCustomerName)
        viewDetail.addConstraints([horizontalLabelName, verticalLabelName ,widthLabelName, heightLabelName])
        
        //creating and configuration image customer
        let imageCustomer = UIImageView()
        
        imageCustomer.layer.cornerRadius = 30
        imageCustomer.clipsToBounds = true
        imageCustomer.layer.borderColor = UIColor.white.cgColor
        imageCustomer.layer.borderWidth = 2
        imageCustomer.contentMode = .scaleAspectFill
        imageCustomer.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalImageCustomer = NSLayoutConstraint(item: imageCustomer, attribute: .top , relatedBy: .equal, toItem: labelCustomerName, attribute: .bottom , multiplier: 1, constant: 5)
        let verticalImageCustomer   = NSLayoutConstraint(item: imageCustomer, attribute: .leading , relatedBy: .equal, toItem: viewDetail, attribute: .leading, multiplier: 1, constant: 10)
        let widthImageCustomer      = NSLayoutConstraint(item: imageCustomer, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        let heightImageCustomer     = NSLayoutConstraint(item: imageCustomer, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        
        viewDetail.addSubview(imageCustomer)
        viewDetail.addConstraints([horizontalImageCustomer, verticalImageCustomer ,widthImageCustomer, heightImageCustomer])
        
        if let customerImage: Int = self.newRace.getIDCustomer(){
            
            let url = URL(string: self.shared.serverCustomerImageShared + "passageiro_\(customerImage)/passageiro.png")
            print(url!)
            DispatchQueue.global().async {
                
                if url != nil {
                    
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        imageCustomer.image = UIImage(data: data!)
                    }
                    
                }
                
            }
            
        }
        
        //creating and configuration button go, chat, call, cancel
        //button Go
        buttonGo.setTitle("Iniciar Corrida", for: .normal)
        buttonGo.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        buttonGo.tintColor = UIColor.white
        buttonGo.translatesAutoresizingMaskIntoConstraints = false
        buttonGo.clipsToBounds = true
        buttonGo.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        buttonGo.addTarget(self, action: #selector(beginRace), for: .touchUpInside)
        
        let horizontalButtonGo = NSLayoutConstraint(item: buttonGo, attribute: .top , relatedBy: .equal, toItem: imageCustomer, attribute: .bottom , multiplier: 1, constant: 80)
        let verticalButtonGo   = NSLayoutConstraint(item: buttonGo, attribute: .leading , relatedBy: .equal, toItem: viewDetail, attribute: .leading, multiplier: 1, constant: 0)
        let widthButtonGo      = NSLayoutConstraint(item: buttonGo, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width)
        let heightButtonGo     = NSLayoutConstraint(item: buttonGo, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
        
        viewDetail.addSubview(buttonGo)
        viewDetail.addConstraints([horizontalButtonGo, verticalButtonGo ,widthButtonGo, heightButtonGo])
        
        //button chat
        let buttonChat = UIButton()
        buttonChat.setTitle("Chat", for: .normal)
        buttonChat.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        buttonChat.tintColor = UIColor.white
        buttonChat.translatesAutoresizingMaskIntoConstraints = false
        buttonChat.clipsToBounds = true
        buttonChat.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        buttonChat.addTarget(self, action: #selector(chatCustomer), for: .touchUpInside)
        
        let horizontalButtonChat = NSLayoutConstraint(item: buttonChat, attribute: .top , relatedBy: .equal, toItem: imageCustomer, attribute: .bottom , multiplier: 1, constant: 35)
        let verticalButtonChat   = NSLayoutConstraint(item: buttonChat, attribute: .leading , relatedBy: .equal, toItem: viewDetail, attribute: .leading, multiplier: 1, constant: 0)
        let widthButtonChat      = NSLayoutConstraint(item: buttonChat, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width / 3)
        let heightButtonChat     = NSLayoutConstraint(item: buttonChat, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        
        viewDetail.addSubview(buttonChat)
        viewDetail.addConstraints([horizontalButtonChat, verticalButtonChat ,widthButtonChat, heightButtonChat])
        
        //button call
        let buttonCall = UIButton()
        buttonCall.setTitle("Ligar", for: .normal)
        buttonCall.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        buttonCall.tintColor = UIColor.white
        buttonCall.translatesAutoresizingMaskIntoConstraints = false
        buttonCall.clipsToBounds = true
        buttonCall.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        buttonCall.addTarget(self, action: #selector(callCustomer), for: .touchUpInside)
        
        
        let horizontalButtonCall = NSLayoutConstraint(item: buttonCall, attribute: .top , relatedBy: .equal, toItem: imageCustomer, attribute: .bottom , multiplier: 1, constant: 35)
        let verticalButtonCall   = NSLayoutConstraint(item: buttonCall, attribute: .leading , relatedBy: .equal, toItem: buttonChat, attribute: .trailing, multiplier: 1, constant: 0)
        let widthButtonCall      = NSLayoutConstraint(item: buttonCall, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width / 3)
        let heightButtonCall     = NSLayoutConstraint(item: buttonCall, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        
        viewDetail.addSubview(buttonCall)
        viewDetail.addConstraints([horizontalButtonCall, verticalButtonCall ,widthButtonCall, heightButtonCall])
        
        //button cancel
        cancelButton.setTitle("Cancelar", for: .normal)
        cancelButton.setTitleColor(.white, for: .normal)
        cancelButton.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        cancelButton.addTarget(self, action: #selector(cancelRace), for: .touchUpInside)
        
        let horizontalCancelButton  = NSLayoutConstraint(item: cancelButton , attribute: .top , relatedBy: .equal, toItem: imageCustomer, attribute: .bottom , multiplier: 1, constant: 35)
        let verticalCancelButton    = NSLayoutConstraint(item: cancelButton, attribute: .trailing , relatedBy: .equal, toItem: viewDetail, attribute: .trailing, multiplier: 1, constant: 0)
        let widthCancelButton       = NSLayoutConstraint(item: cancelButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width / 3)
        let heightCancelButton      = NSLayoutConstraint(item: cancelButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewDetail.addSubview(cancelButton)
        viewDetail.addConstraints([horizontalCancelButton, verticalCancelButton, widthCancelButton, heightCancelButton])
        
        
        //create a label address
        let labelAdress = UILabel()
        labelAdress.text = "De: \(self.newRace.getOriginAddress())\nPara: \(self.newRace.getDestinationAddress())"
        labelAdress.textColor = UIColor.white
        labelAdress.textAlignment = .left
        labelAdress.numberOfLines = 0
        labelAdress.adjustsFontSizeToFitWidth = true
        labelAdress.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints of label address
        let horizontalLabelAdress  = NSLayoutConstraint(item: labelAdress, attribute: .top , relatedBy: .equal, toItem: labelCustomerName, attribute: .bottom, multiplier: 1, constant: 8)
        let verticalLabelAdress    = NSLayoutConstraint(item: labelAdress, attribute: .leading , relatedBy: .equal, toItem: imageCustomer, attribute: .trailing , multiplier: 1, constant: 12)
        //let trailingLabelAdress     = NSLayoutConstraint(item: labelAdress, attribute: .trailing , relatedBy: .equal, toItem: viewDetail, attribute: .trailing, multiplier: 1, constant: 8)
        let widthLabelAdress       = NSLayoutConstraint(item: labelAdress, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width - 100)
        let heightLabelAdress      = NSLayoutConstraint(item: labelAdress, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 65)
        viewDetail.addSubview(labelAdress)
        viewDetail.addConstraints([horizontalLabelAdress, verticalLabelAdress, widthLabelAdress, heightLabelAdress])
        
        self.viewDetail = viewDetail
        
    }
    
    func createViewEvaluations(raceID: Int, passengerID: Int){
        
        self.buttonOnlineOffiline.isHidden = true
        self.performSegue(withIdentifier: SEGUE_TO_MAKE_PAYMENT, sender: raceID)
        
        
        //buttonEvaluate
        let buttonEvaluate = CustomButton()
        
        let viewEvaluations = UIView()
        viewEvaluations.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        viewEvaluations.layer.cornerRadius = 16
        viewEvaluations.clipsToBounds = true
        viewEvaluations.layer.borderWidth = 1
        viewEvaluations.layer.borderColor = UIColor.black.cgColor
        viewEvaluations.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints a viewEvaluations
        let horizontalViewEvaluations  = NSLayoutConstraint(item: viewEvaluations, attribute: .centerX , relatedBy: .equal, toItem: self.map, attribute: .centerX , multiplier: 1, constant: 0)
        let verticalViewEvaluations    = NSLayoutConstraint(item: viewEvaluations, attribute: .centerY, relatedBy: .equal, toItem: self.map, attribute: .centerY, multiplier: 1, constant: 0)
        let widthViewEvaluations      = NSLayoutConstraint(item: viewEvaluations, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightViewEvaluations     = NSLayoutConstraint(item: viewEvaluations, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.height - 32)
        self.map.addSubview(viewEvaluations)
        self.map.addConstraints([horizontalViewEvaluations, verticalViewEvaluations, widthViewEvaluations, heightViewEvaluations])
        
        
        
        let labelEvaluations = UILabel()
        labelEvaluations.text = "Avalie o passageiro"
        labelEvaluations.textAlignment = .center
        labelEvaluations.backgroundColor = UIColor.white
        labelEvaluations.font = UIFont.boldSystemFont(ofSize: 16)
        labelEvaluations.clipsToBounds = true
        labelEvaluations.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints a labelEvaluations
        let horizontalLabelEvaluations  = NSLayoutConstraint(item: labelEvaluations, attribute: .centerX , relatedBy: .equal, toItem: viewEvaluations, attribute: .centerX , multiplier: 1, constant: 0)
        let verticalLabelEvaluations    = NSLayoutConstraint(item: labelEvaluations, attribute: .top, relatedBy: .equal, toItem: viewEvaluations, attribute: .top, multiplier: 1, constant: 0)
        let widthLabelEvaluations      = NSLayoutConstraint(item: labelEvaluations, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightLabelEvaluations     = NSLayoutConstraint(item: labelEvaluations, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        viewEvaluations.addSubview(labelEvaluations)
        viewEvaluations.addConstraints([horizontalLabelEvaluations, verticalLabelEvaluations, widthLabelEvaluations, heightLabelEvaluations])
        
        
        //customer image
        let customerImage = UIImageView()
        customerImage.layer.borderWidth = 1
        customerImage.layer.borderColor = UIColor.white.cgColor
        customerImage.contentMode = .scaleAspectFill
        customerImage.clipsToBounds = true
        customerImage.layer.cornerRadius = 60
        customerImage.translatesAutoresizingMaskIntoConstraints = false
        
        if let passengerImage: Int = passengerID{
            
            let url = URL(string: self.shared.serverCustomerImageShared + "passageiro_\(passengerImage)/passageiro.png")
            print(url!)
            DispatchQueue.global().async {
                
                if url != nil {
                    
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        customerImage.image = UIImage(data: data!)
                    }
                    
                }
                
            }
            
        }
        
        //add constraints image
        let constraintOne = NSLayoutConstraint(item: customerImage, attribute: .top, relatedBy: .equal, toItem: labelEvaluations, attribute: .bottom, multiplier: 1, constant: 16)
        let constraintTwo = NSLayoutConstraint(item: customerImage, attribute: .centerX, relatedBy: .equal, toItem: viewEvaluations, attribute: .centerX, multiplier: 1, constant: 0)
        let constraintThree = NSLayoutConstraint(item: customerImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 120)
        let constraintFour  = NSLayoutConstraint(item: customerImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 120)
        
        viewEvaluations.addSubview(customerImage)
        viewEvaluations.addConstraints([constraintOne, constraintTwo, constraintThree, constraintFour])
        
        
        //labelRaceValue
        let labelValue = UILabel()
        let value = ""
        labelValue.text = "O valor da corrida foi: \(value.formatCurrency(value: self.newRace.getRaceCost()))"
        labelValue.textAlignment = .left
        labelValue.font = UIFont.boldSystemFont(ofSize: 18)
        labelValue.textColor = UIColor.white
        labelValue.adjustsFontSizeToFitWidth = true
        labelValue.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints labelPunctuality
        let horizontalLabelValue  = NSLayoutConstraint(item: labelValue, attribute: .top , relatedBy: .equal, toItem: customerImage, attribute: .bottom , multiplier: 1, constant: 16)
        let verticalLabelValue    = NSLayoutConstraint(item: labelValue, attribute: .leading, relatedBy: .equal, toItem: viewEvaluations, attribute: .leading, multiplier: 1, constant: 8)
        let widthLabelValue     = NSLayoutConstraint(item: labelValue, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightLabelValue   = NSLayoutConstraint(item: labelValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
        viewEvaluations.addSubview(labelValue)
        viewEvaluations.addConstraints([horizontalLabelValue, verticalLabelValue, widthLabelValue, heightLabelValue])
        
        //labelPunctuality
        let labelPunctuality = UILabel()
        labelPunctuality.text = "Pontualidade"
        labelPunctuality.textAlignment = .left
        labelPunctuality.textColor = UIColor.white
        labelPunctuality.font = UIFont.boldSystemFont(ofSize: 14)
        labelPunctuality.translatesAutoresizingMaskIntoConstraints = false
        
        //add constraints labelPunctuality
        let horizontalLabelPunctuality  = NSLayoutConstraint(item: labelPunctuality, attribute: .top , relatedBy: .equal, toItem: labelValue, attribute: .bottom , multiplier: 1, constant: 12)
        let verticalLabelPunctuality    = NSLayoutConstraint(item: labelPunctuality, attribute: .leading, relatedBy: .equal, toItem: viewEvaluations, attribute: .leading, multiplier: 1, constant: 8)
        let widthLabelPunctuality      = NSLayoutConstraint(item: labelPunctuality, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
        let heightLabelPunctuality    = NSLayoutConstraint(item: labelPunctuality, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
        viewEvaluations.addSubview(labelPunctuality)
        viewEvaluations.addConstraints([horizontalLabelPunctuality, verticalLabelPunctuality, widthLabelPunctuality, heightLabelPunctuality])
        
        //Cosmos Punctuality
        let punctuality = CosmosView()
        
        //Cosmos Sympathy
        let sympathy = CosmosView()
        
        DispatchQueue.main.async {
            
            
            punctuality.settings.updateOnTouch = true
            punctuality.settings.starSize = 35
            punctuality.settings.updateOnTouch = true
            punctuality.settings.filledColor = #colorLiteral(red: 0.999070704, green: 0.8535897136, blue: 0.004626365378, alpha: 1)
            buttonEvaluate.punctuality = 3
            punctuality.didFinishTouchingCosmos = {rating in
                buttonEvaluate.punctuality = rating
            }
            punctuality.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints cosmos Punctuality
            let horizontalPunctuality  = NSLayoutConstraint(item: punctuality, attribute: .leading , relatedBy: .equal, toItem: viewEvaluations, attribute: .leading , multiplier: 1, constant: 8)
            let verticalPunctuality    = NSLayoutConstraint(item: punctuality, attribute: .top, relatedBy: .equal, toItem: labelPunctuality, attribute: .bottom, multiplier: 1, constant: 16)
            let widthPunctuality     = NSLayoutConstraint(item: punctuality, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewEvaluations.frame.size.width)
            let heightPunctuality    = NSLayoutConstraint(item: punctuality, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            viewEvaluations.addSubview(punctuality)
            viewEvaluations.addConstraints([horizontalPunctuality, verticalPunctuality, widthPunctuality, heightPunctuality])
            
            
            //labelPunctuality
            let labelSympathy = UILabel()
            labelSympathy.text = "Simpatia"
            labelSympathy.textAlignment = .left
            labelSympathy.textColor = UIColor.white
            labelSympathy.font = UIFont.boldSystemFont(ofSize: 14)
            labelSympathy.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints labelPunctuality
            let horizontalLabelSympathy  = NSLayoutConstraint(item: labelSympathy, attribute: .top , relatedBy: .equal, toItem: punctuality, attribute: .bottom , multiplier: 1, constant: 16)
            let verticalLabelSympathy    = NSLayoutConstraint(item: labelSympathy, attribute: .leading, relatedBy: .equal, toItem: punctuality, attribute: .leading, multiplier: 1, constant: 0)
            let widthLabelSympathy       = NSLayoutConstraint(item: labelSympathy, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: self.view.frame.size.width - 32)
            let heightLabelSympathy      = NSLayoutConstraint(item: labelSympathy, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20)
            viewEvaluations.addSubview(labelSympathy)
            viewEvaluations.addConstraints([horizontalLabelSympathy, verticalLabelSympathy, widthLabelSympathy, heightLabelSympathy])
            
            
            
            sympathy.settings.starSize = 35
            sympathy.settings.updateOnTouch = true
            sympathy.settings.filledColor = #colorLiteral(red: 0.999070704, green: 0.8535897136, blue: 0.004626365378, alpha: 1)
            buttonEvaluate.sympathy = 3
            sympathy.didFinishTouchingCosmos = {rating in
                buttonEvaluate.sympathy = rating
                
            }
            
            
            sympathy.translatesAutoresizingMaskIntoConstraints = false
            
            //add constraints cosmos Sympathy
            let horizontalSympathy  = NSLayoutConstraint(item: sympathy, attribute: .leading , relatedBy: .equal, toItem: viewEvaluations, attribute: .leading , multiplier: 1, constant: 8)
            let verticalSympathy    = NSLayoutConstraint(item: sympathy, attribute: .top, relatedBy: .equal, toItem: labelSympathy, attribute: .bottom, multiplier: 1, constant: 16)
            let widthSympathy    = NSLayoutConstraint(item: sympathy, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewEvaluations.frame.size.width)
            let heightSympathy    = NSLayoutConstraint(item: sympathy, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            viewEvaluations.addSubview(sympathy)
            viewEvaluations.addConstraints([horizontalSympathy, verticalSympathy, widthSympathy, heightSympathy])
            
        }
        
        //create buttom close and button Evaluate
        //buttonClose
        let buttonClose = UIButton()
        buttonClose.setTitle("Fechar", for: .normal)
        buttonClose.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        buttonClose.translatesAutoresizingMaskIntoConstraints = false
        buttonClose.addTarget(self, action: #selector(closeEvaluation), for: .touchUpInside)
        
        //add constraints a buttonClose
        let horizontalButtonClose  = NSLayoutConstraint(item: buttonClose, attribute: .bottom , relatedBy: .equal, toItem: viewEvaluations, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalButtonClose    = NSLayoutConstraint(item: buttonClose, attribute: .leading, relatedBy: .equal, toItem: viewEvaluations, attribute: .leading, multiplier: 1, constant: 0)
        let widthButtonClose      = NSLayoutConstraint(item: buttonClose, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (self.view.frame.size.width - 32) / 2)
        let heightButtonClose     = NSLayoutConstraint(item: buttonClose, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewEvaluations.addSubview(buttonClose)
        viewEvaluations.addConstraints([horizontalButtonClose, verticalButtonClose, widthButtonClose, heightButtonClose])
        
        
        buttonEvaluate.idRace      = raceID
        buttonEvaluate.setTitle("Avaliar", for: .normal)
        buttonEvaluate.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        buttonEvaluate.translatesAutoresizingMaskIntoConstraints = false
        buttonEvaluate.addTarget(self, action: #selector(self.evaluationCustomer(sender:)), for: .touchUpInside)
        
        //add constraints a buttonEvaluate
        let horizontalButtonEvaluate  = NSLayoutConstraint(item: buttonEvaluate, attribute: .bottom , relatedBy: .equal, toItem: viewEvaluations, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalButtonEvaluate    = NSLayoutConstraint(item: buttonEvaluate, attribute: .trailing, relatedBy: .equal, toItem: viewEvaluations, attribute: .trailing, multiplier: 1, constant: 0)
        let widthButtonEvaluate      = NSLayoutConstraint(item: buttonEvaluate, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (self.view.frame.size.width - 32) / 2)
        let heightButtonEvaluate     = NSLayoutConstraint(item: buttonEvaluate, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        viewEvaluations.addSubview(buttonEvaluate)
        viewEvaluations.addConstraints([horizontalButtonEvaluate, verticalButtonEvaluate, widthButtonEvaluate, heightButtonEvaluate])
        
        
        
        self.viewEvaluation = viewEvaluations
       
    }
    
    @objc func visibleDetailView(){
        
        if self.booleanViewDetails == false{
            openView(validate: booleanViewDetails, constraint: heightConstraintViewDetails!, number: 240, time: 0.7)
            self.buttonMinimize.setBackgroundImage(#imageLiteral(resourceName: "ic_keyboard_arrow_down_white"), for: .normal)
        }else{
            self.buttonMinimize.setBackgroundImage(#imageLiteral(resourceName: "ic_keyboard_arrow_up_white"), for: .normal)
            openView(validate: booleanViewDetails, constraint: heightConstraintViewDetails!, number: 40, time: 0.7)
            
        }
        self.booleanViewDetails = !booleanViewDetails
    }
    
}


extension HomeViewController: UITextViewDelegate{
    
    internal func createViewJustifield(){
        
        //create view master
        //MARK: TODO: fazer a view preta ocupar a tela inteira
        let viewMaster = UIView()
        viewMaster.translatesAutoresizingMaskIntoConstraints = false
        viewMaster.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.33)
        
        let hViewMaster       = NSLayoutConstraint(item: viewMaster, attribute: .top , relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: (((self.navigationController?.navigationBar.frame.size.height)! + 40) * -1))
        let vViewMaster       = NSLayoutConstraint(item: viewMaster, attribute: .leading , relatedBy: .equal, toItem: view, attribute: .leading , multiplier: 1, constant: 0)
        let wViewMaster       = NSLayoutConstraint(item: viewMaster, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width )
        let h2ViewMaster      = NSLayoutConstraint(item: viewMaster, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.height + ((self.navigationController?.navigationBar.frame.size.height)! + 40))
        
        self.view.addSubview(viewMaster)
        
        self.view.addConstraints([hViewMaster , vViewMaster  ,wViewMaster , h2ViewMaster])
        
        
        
        
        //create view
        let viewJustifield = UIView(frame: CGRect(x: 16, y: 100, width: self.view.frame.size.width - 32, height: 0))
        viewJustifield.translatesAutoresizingMaskIntoConstraints = false
        viewJustifield.clipsToBounds = true
        viewJustifield.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1)
        viewJustifield.layer.cornerRadius = 20
        viewJustifield.layer.borderColor = UIColor.black.cgColor
        viewJustifield.layer.borderWidth = 1
        
        
        //add constraints
        let hJustifield       = NSLayoutConstraint(item: viewJustifield, attribute: .centerX , relatedBy: .equal, toItem: viewMaster, attribute: .centerX, multiplier: 1, constant: 0)
        let vJustifield        = NSLayoutConstraint(item: viewJustifield, attribute: .centerY , relatedBy: .equal, toItem: viewMaster, attribute: .centerY , multiplier: 1, constant: 0)
        let wJustifield        = NSLayoutConstraint(item: viewJustifield, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.size.width - 32)
        let h2Justifield       = NSLayoutConstraint(item: viewJustifield, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        viewMaster.addSubview(viewJustifield)
        viewMaster.addConstraints([hJustifield , vJustifield  ,wJustifield , h2Justifield])
        
        self.heightConstraintViewJustifield = h2Justifield
        
        
        
        //create textview
        let textViewJustifield = UITextView()
        textViewJustifield.text = "Digite aqui sua justificativa para o cancelamento."
        textViewJustifield.layer.cornerRadius = 16
        textViewJustifield.layer.borderWidth = 1
        textViewJustifield.layer.borderColor = UIColor.black.cgColor
        textViewJustifield.translatesAutoresizingMaskIntoConstraints = false
        
        textViewJustifield.delegate = self
        
        //add constraints
        let hTextView       = NSLayoutConstraint(item: textViewJustifield, attribute: .top , relatedBy: .equal, toItem: viewJustifield, attribute: .top, multiplier: 1, constant: 16)
        let vTextView        = NSLayoutConstraint(item: textViewJustifield, attribute: .leading , relatedBy: .equal, toItem: viewJustifield, attribute: .leading , multiplier: 1, constant: 16)
        let wTextView       = NSLayoutConstraint(item: textViewJustifield, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewJustifield.frame.size.width - 32)
        let h2TextView      = NSLayoutConstraint(item: textViewJustifield, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 200)
        
        viewJustifield.addSubview(textViewJustifield)
        viewJustifield.addConstraints([hTextView , vTextView  ,wTextView , h2TextView])
        
        //button cancel
        let buttonCancel = UIButton()
        buttonCancel.tag = 1
        buttonCancel.setTitle("Cancelar", for: .normal)
        buttonCancel.backgroundColor =  #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        buttonCancel.tintColor = UIColor.white
        buttonCancel.translatesAutoresizingMaskIntoConstraints = false
        buttonCancel.clipsToBounds = true
        buttonCancel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        buttonCancel.addTarget(self, action: #selector(closeViewJustifield(sender:)), for: .touchUpInside)
        
        let horizontalButtonCancel = NSLayoutConstraint(item: buttonCancel, attribute: .bottom , relatedBy: .equal, toItem: viewJustifield, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalButtonCancel   = NSLayoutConstraint(item: buttonCancel, attribute: .trailing , relatedBy: .equal, toItem: viewJustifield, attribute: .trailing, multiplier: 1, constant: 0)
        let widthButtonCancel      = NSLayoutConstraint(item: buttonCancel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewJustifield.frame.size.width / 2)
        let heightButtonCancel     = NSLayoutConstraint(item: buttonCancel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        
        viewJustifield.addSubview(buttonCancel)
        viewJustifield.addConstraints([horizontalButtonCancel, verticalButtonCancel,widthButtonCancel, heightButtonCancel])
        
        //button give up
        let buttonGiveUp = UIButton()
        buttonGiveUp.tag = 0
        buttonGiveUp.setTitle("Voltar", for: .normal)
        buttonGiveUp.backgroundColor = #colorLiteral(red: 0, green: 0.5603182912, blue: 0, alpha: 1)
        buttonGiveUp.tintColor = UIColor.white
        buttonGiveUp.translatesAutoresizingMaskIntoConstraints = false
        buttonGiveUp.clipsToBounds = true
        buttonGiveUp.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        buttonGiveUp.addTarget(self, action: #selector(closeViewJustifield(sender:)), for: .touchUpInside)
        
        
        let horizontalGiveUp = NSLayoutConstraint(item: buttonGiveUp, attribute: .bottom , relatedBy: .equal, toItem: viewJustifield, attribute: .bottom , multiplier: 1, constant: 0)
        let verticalGiveUp   = NSLayoutConstraint(item: buttonGiveUp, attribute: .leading , relatedBy: .equal, toItem: viewJustifield, attribute: .leading, multiplier: 1, constant: 0)
        let widthGiveUp     = NSLayoutConstraint(item: buttonGiveUp, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: viewJustifield.frame.size.width / 2)
        let heightGiveUp     = NSLayoutConstraint(item: buttonGiveUp, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        
        viewJustifield.addSubview(buttonGiveUp)
        viewJustifield.addConstraints([horizontalGiveUp, verticalGiveUp, widthGiveUp, heightGiveUp])
        
        //self.requestCancelRace()
        self.viewJustifield = viewMaster
        self.navigationController?.navigationBar.layer.zPosition = -1
        self.viewJustifield.layer.zPosition = 0
        self.visibleJustifield()
    }
    
    internal func visibleJustifield(){
        
        if self.booleanJustifield == false{
            openView(validate: booleanJustifield , constraint: heightConstraintViewJustifield!, number: 300, time: 0)
            
        }else{
            
            openView(validate: booleanViewDetails, constraint: heightConstraintViewJustifield!, number: 0, time: 0)
            
        }
        self.booleanJustifield = !booleanJustifield
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = "Digite aqui sua justificativa para o cancelamento."
            self.justifield = ""
        }else{
            self.justifield = textView.text!
        }
    }
    
}
