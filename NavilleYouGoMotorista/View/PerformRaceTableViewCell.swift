//
//  PerformRaceTableViewCell.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 27/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import UIKit

class PerformRaceTableViewCell: UITableViewCell {

    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelOrigin: UILabel!
    @IBOutlet weak var labelDestination: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageCustomer: ImageCustom!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
