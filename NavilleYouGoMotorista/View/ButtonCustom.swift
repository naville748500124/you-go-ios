//
//  ButtonCustom.swift
//  Aerocalc
//
//  Created by Marcos Barbosa on 07/12/2017.
//  Copyright © 2017 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class ButtonCustom: UIButton {
   
    
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = #colorLiteral (red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0){
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0){
    didSet{
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize = CGSize(width: 1, height: 1){
        didSet{
            self.layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat = 0 {
        didSet{
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float = 0 {
        didSet{
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable
    var clip: Bool = false{
        didSet{
            self.layer.masksToBounds = clip
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.layer.shadowColor = UIColor.clear.cgColor
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.layer.shadowColor = self.shadowColor.cgColor
    }
    
}

class CustomButton:UIButton {
    var punctuality: Double?
    var sympathy:    Double?
    var idRace:      Int?
}
