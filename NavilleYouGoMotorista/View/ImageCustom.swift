//
//  ImageCustom.swift
//  NavilleYouGoMotorista
//
//  Created by Marcos Barbosa on 26/02/2018.
//  Copyright © 2018 Marcos Barbosa. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class ImageCustom: UIImageView {
    
    
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = #colorLiteral (red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0){
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0){
        didSet{
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize = CGSize(width: 1, height: 1){
        didSet{
            self.layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat = 0 {
        didSet{
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float = 0 {
        didSet{
            self.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable
    var clip: Bool = false{
        didSet{
            self.layer.masksToBounds = clip
        }
    }
    
}
